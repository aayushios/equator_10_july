(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-checkout-address-checkout-module"],{

/***/ "./src/app/address-checkout/address-checkout.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/address-checkout/address-checkout.module.ts ***!
  \*************************************************************/
/*! exports provided: AddressCheckoutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressCheckoutPageModule", function() { return AddressCheckoutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _address_checkout_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./address-checkout.page */ "./src/app/address-checkout/address-checkout.page.ts");







var routes = [
    {
        path: '',
        component: _address_checkout_page__WEBPACK_IMPORTED_MODULE_6__["AddressCheckoutPage"]
    }
];
var AddressCheckoutPageModule = /** @class */ (function () {
    function AddressCheckoutPageModule() {
    }
    AddressCheckoutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_address_checkout_page__WEBPACK_IMPORTED_MODULE_6__["AddressCheckoutPage"]]
        })
    ], AddressCheckoutPageModule);
    return AddressCheckoutPageModule;
}());



/***/ }),

/***/ "./src/app/address-checkout/address-checkout.page.html":
/*!*************************************************************!*\
  !*** ./src/app/address-checkout/address-checkout.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Your Saved Addresses</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"header-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <div *ngIf= \"newUser === true\" class=\"text-right\">\n        <ion-button class=\"btn-view\" (click)=\"editAddress()\">\n          Add\n        </ion-button>\n      </div>\n  <ion-card class=\"address-Card\">\n    <h2> Billing Address</h2>\n    <ion-label>{{billingAddressFirstName }} {{billingAddressLastName}}</ion-label><br>\n    <ion-card-content class=\"content-Card\">\n      <ion-label> Company: {{billCompanyName}} </ion-label><br>\n      <ion-label> Address: {{billAddress}} </ion-label><br>\n      <ion-label> City: {{billCity}} </ion-label><br>\n      <ion-label> Post Code: {{billPostCode}} </ion-label><br>\n      <ion-label> Country: {{billCountry}} </ion-label><br>\n      <ion-label> Phone: {{billPhone}} </ion-label><br>\n      <ion-label> Email: {{billEmail}} </ion-label><br>\n    </ion-card-content>\n  </ion-card>\n  <ion-card class=\"address-Card\">\n    <h2> Shipping Address</h2>\n    <ion-label>{{shippingAddressFirstName }} {{shippingAddressLastName}}</ion-label><br>\n    <ion-card-content class=\"content-Card\">\n      <ion-label> Company: {{shipCompanyName}} </ion-label><br>\n      <ion-label> Address: {{shipAddress}} </ion-label><br>\n      <ion-label> City: {{shipCity}} </ion-label><br>\n      <ion-label> Post Code: {{shipPostCode}} </ion-label><br>\n      <ion-label> Country: {{shipCountry}} </ion-label><br>\n      <ion-label> Phone: {{shipPhone}} </ion-label><br>\n      <ion-label> Email: {{shipEmail}} </ion-label><br>\n    </ion-card-content>\n  </ion-card>\n\n  <div class=\"apply-coupon-section\">\n    <ion-row class=\"apply-coupon-content\">\n      <ion-col size=\"9\">\n        <div class=\"apply-coupon-box\">\n          <button class=\"apply-coupon-btn\" ion-button (click)=\"hideMe = !hideMe\">Any Notes(optional)</button>\n        </div>\n      </ion-col>\n      <div *ngIf=\"hideMe\" class=\"apply-coupon-form\">\n        <ion-item class=\"password-input\">\n          <ion-input class=\"input-form-control\" (keydown.space)=\"$event.preventDefault();\" [type]=\"text\"\n            placeholder=\"Any notes(optional)\">\n          </ion-input>\n        </ion-item>\n      </div>\n    </ion-row>\n  </div>\n\n  <div class=\"text-center\">\n    <ion-button class=\"btn-view\" (click)=\"placeOrder()\" [disabled]=\"disableButton\">\n      Proceed to Checkout\n    </ion-button>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/address-checkout/address-checkout.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/address-checkout/address-checkout.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.header-arrow {\n  color: white; }\n\n.address-Card {\n  background: #ffffff;\n  padding: 15px;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0; }\n\n.address-Card h2 {\n  font-size: 18px;\n  font-weight: 600;\n  padding-top: 0;\n  margin-top: 0;\n  color: #000000; }\n\n.content-Card {\n  padding: 0px 0 0 0;\n  margin-top: 0;\n  color: #000000;\n  font-weight: 250;\n  font-size: 13px; }\n\n.btn-view {\n  color: #fff;\n  font-weight: bold;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated: #b31117; }\n\n.text-center {\n  text-align: center; }\n\n.apply-coupon-section {\n  background: #fff;\n  margin-bottom: 30px; }\n\n.apply-coupon-form {\n  width: 100%; }\n\n.apply-coupon-box {\n  display: flex;\n  align-items: center;\n  height: 100%; }\n\n.offer-btn-box {\n  text-align: right;\n  padding: 10px 0; }\n\n.apply-coupon-btn {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #213051;\n  font-weight: 600;\n  font-size: 14px;\n  outline: none; }\n\n.text-right {\n  text-align: right; }\n\n.btn-view {\n  color: #fff;\n  font-weight: bold;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated: #b31117; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2FkZHJlc3MtY2hlY2tvdXQvYWRkcmVzcy1jaGVja291dC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWE7RUFDYixZQUFZLEVBQUE7O0FBR2hCO0VBRUksWUFBWSxFQUFBOztBQUdoQjtFQUVJLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsb0JBQW9CLEVBQUE7O0FBR3hCO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsYUFBYTtFQUNiLGNBQWMsRUFBQTs7QUFJbEI7RUFDSSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUduQjtFQUNJLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG9CQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsU0FBUztFQUNULFlBQVk7RUFDWixtQkFBYztFQUNkLHNCQUFpQjtFQUNqQixxQkFBZ0I7RUFDaEIsbUJBQWM7RUFDZCxxQkFBYTtFQUNiLCtCQUF1QixFQUFBOztBQUczQjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxpQkFBaUI7RUFDakIsZUFBZSxFQUFBOztBQUduQjtFQUNJLGdCQUFnQjtFQUNoQix5QkFBYTtFQUNiLGlDQUFxQjtFQUNyQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksV0FBVztFQUNYLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2Ysb0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixTQUFTO0VBQ1QsWUFBWTtFQUNaLG1CQUFjO0VBQ2Qsc0JBQWlCO0VBQ2pCLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLHFCQUFhO0VBQ2IsK0JBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hZGRyZXNzLWNoZWNrb3V0L2FkZHJlc3MtY2hlY2tvdXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG59XG5cbi5oZWFkZXItdGl0bGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlclxufVxuXG4uYmFkZ2UtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cbi5oZWFkZXItYXJyb3cge1xuXG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uYWRkcmVzcy1DYXJkIHtcblxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMDtcbn1cblxuLmFkZHJlc3MtQ2FyZCBoMiB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBjb2xvcjogIzAwMDAwMDtcblxufVxuXG4uY29udGVudC1DYXJkIHtcbiAgICBwYWRkaW5nOiAwcHggMCAwIDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgICBmb250LXdlaWdodDogMjUwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmJ0bi12aWV3IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLS1wYWRkaW5nLXRvcDogMTVweDtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMzBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAzMHB4O1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjYjMxMTE3O1xufVxuXG4udGV4dC1jZW50ZXIge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmFwcGx5LWNvdXBvbi1zZWN0aW9uIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5hcHBseS1jb3Vwb24tZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5hcHBseS1jb3Vwb24tYm94IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ub2ZmZXItYnRuLWJveCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgcGFkZGluZzogMTBweCAwO1xufVxuXG4uYXBwbHktY291cG9uLWJ0biB7XG4gICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiB0cmFuc3BhcmVudDtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBvdXRsaW5lOiBub25lO1xufVxuXG4udGV4dC1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5idG4tdmlldyB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC0tcGFkZGluZy10b3A6IDE1cHg7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDMwcHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMzBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI2IzMTExNztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/address-checkout/address-checkout.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/address-checkout/address-checkout.page.ts ***!
  \***********************************************************/
/*! exports provided: AddressCheckoutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressCheckoutPage", function() { return AddressCheckoutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../edit-address/edit-address.page */ "./src/app/edit-address/edit-address.page.ts");








var AddressCheckoutPage = /** @class */ (function () {
    function AddressCheckoutPage(homePageService, menuCtrl, router, alert, route, navCtrl, modalController, loadingService) {
        var _this = this;
        this.homePageService = homePageService;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.alert = alert;
        this.route = route;
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.loadingService = loadingService;
        this.billCompanyName = "";
        this.billAddress = "";
        this.billCity = "";
        this.billPostCode = "";
        this.billCountry = "";
        this.billPhone = "";
        this.billEmail = "";
        this.billingAddressFirstName = "";
        this.billingAddressLastName = "";
        this.shipCompanyName = "";
        this.shipAddress = "";
        this.shipCity = "";
        this.shipPostCode = "";
        this.shipCountry = "";
        this.shipPhone = "";
        this.shipEmail = "";
        this.shippingAddressFirstName = "";
        this.shippingAddressLastName = "";
        this.totalAmount = "";
        this.membershipDiscount = "";
        this.isMember = "";
        this.totalTax = "";
        this.newUser = false;
        this.disableButton = false;
        this.getWishlist();
        this.route.queryParams.subscribe(function (params) {
            console.log("params", params);
            _this.totalAmount = params.total_amount;
            _this.membershipDiscount = params.membership_discount;
            _this.totalTax = params.total_tax;
            _this.isMember = params.is_member;
        });
    }
    AddressCheckoutPage.prototype.ngOnInit = function () {
        this.disableButton = false;
    };
    AddressCheckoutPage.prototype.hide = function () {
        this.hideMe = true;
    };
    AddressCheckoutPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    AddressCheckoutPage.prototype.placeOrder = function () {
        var _this = this;
        this.disableButton = true;
        this.loadingService.present();
        this.homePageService.stripeCheckoutApi('cod', "", this.totalAmount, this.totalTax, this.isMember, this.membershipDiscount).subscribe(function (resp) {
            console.log("COD response", JSON.stringify(resp));
            if (resp.success == true) {
                _this.alert.myAlertMethod("Successfully", resp.message, function (data) {
                    console.log("data", data);
                    _this.router.navigate(['your-orders']);
                });
            }
            else {
                _this.alert.myAlertMethod("Something went wrong. Please try again after sometime.", resp.message, function (data) {
                    console.log("Something went wrong.");
                });
            }
        }, function (err) {
            _this.alert.myAlertMethod("error", err.message, function (data) {
                console.log("error");
            });
            console.log(err);
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    AddressCheckoutPage.prototype.editAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_7__["EditAddressPage"],
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                console.log("sizeChartResponse", sizeChartResponse);
                                _this.getWishlist();
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AddressCheckoutPage.prototype.getWishlist = function () {
        var _this = this;
        this.loadingService.present();
        this.homePageService.getWishlistProducts().subscribe(function (resp) {
            if (resp.data == null || resp.data.address_details == "") {
                _this.disableButton = true;
                _this.newUser = true;
                return;
            }
            else {
                console.log(false);
                _this.disableButton = false;
                _this.newUser = false;
            }
            _this.billCompanyName = resp.data.address_details.account_bill_company_name;
            _this.billAddress = resp.data.address_details.account_bill_adddress_line_1;
            _this.billCity = resp.data.address_details.account_bill_title;
            _this.billPostCode = resp.data.address_details.account_bill_zip_or_postal_code;
            _this.billCountry = resp.data.address_details.account_bill_select_country;
            _this.billPhone = resp.data.address_details.account_bill_phone_number;
            _this.billEmail = resp.data.address_details.account_bill_email_address;
            _this.billingAddressFirstName = resp.data.address_details.account_bill_first_name;
            _this.billingAddressLastName = resp.data.address_details.account_bill_last_name;
            _this.shipCompanyName = resp.data.address_details.account_shipping_company_name;
            _this.shipAddress = resp.data.address_details.account_shipping_adddress_line_1;
            _this.shipCity = resp.data.address_details.account_shipping_title;
            _this.shipPostCode = resp.data.address_details.account_shipping_zip_or_postal_code;
            _this.shipCountry = resp.data.address_details.account_shipping_select_country;
            _this.shipPhone = resp.data.address_details.account_shipping_phone_number;
            _this.shipEmail = resp.data.address_details.account_shipping_email_address;
            _this.shippingAddressFirstName = resp.data.address_details.account_shipping_first_name;
            _this.shippingAddressLastName = resp.data.address_details.account_shipping_last_name;
        }, function (err) {
            console.log("error", err);
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    AddressCheckoutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-address-checkout',
            template: __webpack_require__(/*! ./address-checkout.page.html */ "./src/app/address-checkout/address-checkout.page.html"),
            styles: [__webpack_require__(/*! ./address-checkout.page.scss */ "./src/app/address-checkout/address-checkout.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__["AlerServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]])
    ], AddressCheckoutPage);
    return AddressCheckoutPage;
}());



/***/ })

}]);
//# sourceMappingURL=address-checkout-address-checkout-module.js.map