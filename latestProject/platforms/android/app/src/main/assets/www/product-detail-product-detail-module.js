(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-detail-product-detail-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/product-detail/product-detail.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.module.ts ***!
  \*********************************************************/
/*! exports provided: ProductDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPageModule", function() { return ProductDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _product_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-detail.page */ "./src/app/product-detail/product-detail.page.ts");







var routes = [
    {
        path: '',
        component: _product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]
    }
];
var ProductDetailPageModule = /** @class */ (function () {
    function ProductDetailPageModule() {
    }
    ProductDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]]
        })
    ], ProductDetailPageModule);
    return ProductDetailPageModule;
}());



/***/ }),

/***/ "./src/app/product-detail/product-detail.page.html":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"home-content\">\n  <ion-toolbar class=\"product-single-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"header-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <div *ngIf=\"detailsContent.post_image_url === ''\">\n    <ion-card-content class=\"product-card-media\">\n      <img src=\"../../assets/imgs/icon.png\">\n    </ion-card-content>\n  </div>\n  <div *ngIf=\"detailsContent.post_image_url != '' \" class=\"product-single-slider\">\n    <ion-slides autoplay=\"5000\" loop=\"true\" speed=\"500\" class=\"slides\" pager=\"true\">\n      <ion-slide *ngFor=\"let cat of detailImageList\">\n        <img src=\"{{apiUrl}}{{cat.url}}\" (click)=\"openPreview(cat.url)\">\n      </ion-slide>\n    </ion-slides>\n</div>\n  <div *ngIf=\"productType === 'variation'\">\n    <ion-card-content class=\"product-card-media\">\n      <img id=\"variationImage\">\n    </ion-card-content>\n  </div>\n  <!-- <div class=\"product-single-content\" *ngIf=\"checkStockOfProduct != 'In Stock'\">\n    <h2 style=\"color: red\">{{checkStockOfProduct}}</h2>\n  </div>\n  <div class=\"product-single-content\" *ngIf=\"checkStockOfProduct === 'In Stock'\">\n    <h2 style=\"color: green\">{{checkStockOfProduct}}</h2>\n  </div> -->\n  <div class=\"product-single-content\">\n    <h2>\n      {{detailsContent.post_title}}\n    </h2>\n\n    <div *ngIf=\"averageRating === 0\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:0%\"></span>\n          </div>\n        </li>\n        <li><span (click)=\"openRatingModal(detailsContent.id)\">({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating > 0 && averageRating < 1\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:18%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating === 1\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:20%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating > 1 && averageRating < 2\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:35%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating === 2\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:40%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating > 2 && averageRating < 3\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:55%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating === 3\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:55%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating > 3 && averageRating < 4\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:69%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating === 4\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:80%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating > 4 && averageRating < 5\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:90%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <div *ngIf=\"averageRating === 5\" class=\"star-rating-section\">\n      <ul>\n        <li>\n          <div class=\"star-rating\">\n            <span style=\"width:100%\"></span>\n          </div>\n        </li>\n        <li (click)=\"openRatingModal(detailsContent.id)\"><span>({{totalRating}}) Ratings</span></li>\n      </ul>\n    </div>\n    <!-- <ion-button class=\"button-rating\" (click)=\"openRatingModal(detailsContent.id)\">\n      Rating\n    </ion-button> -->\n    <!-- <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.\n      The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of\n      Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.\n    </p> -->\n    <p>{{detailsContent.post_content}}\n    </p>\n  </div>\n  <div *ngIf='colorArray.length > 0'>\n    <div class=\"product-single-size\">\n      <h2>Color</h2>\n      <div class=\"size-scroll\" scrollX=\"true\">\n        <button [ngClass]=\"{'selected':color.isSelected}\" class=\"btn-size size-span\"\n          *ngFor=\"let color of colorArray;let i = index\" (click)=\"btnColor(i)\">{{color.color}}</button>\n      </div>\n    </div>\n  </div>\n  <div *ngIf='colorArray.length > 0'>\n    <div class=\"product-single-size\">\n      <h2>Size</h2>\n      <div class=\"size-scroll\" scrollX=\"true\">\n        <button [ngClass]=\"{'selected':category.isSelected}\" class=\"btn-size size-span\"\n          *ngFor=\"let category of sizeArray;let i = index\" (click)=\"btnSize(i)\">{{category.size}}</button>\n      </div>\n      <ion-button *ngIf=\"postType === 'configurable_product'\" class=\"btn-size-chart\" (click)=\"openSizeChart()\">\n        See Size Guide\n      </ion-button>\n    </div>\n    \n  </div>\n  <div class=\"product-single-size\" *ngIf='colorArray.length > 0'>\n    <div>\n      <button class=\"btn-increment\" (click)=\"decrementQty()\">\n        <ion-icon class=\"ion-icon-increment\" name=\"remove-circle\"></ion-icon>\n      </button>\n      <div class=\"quantity-value\"> {{qty}} </div>\n\n      <button class=\"btn-increment\" (click)=\"incrementQty()\">\n        <ion-icon class=\"ion-icon-increment\" name=\"add-circle\"></ion-icon>\n      </button>\n    </div>\n  </div>\n</ion-content>\n<ion-footer class=\"product-single-footer\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"8\">\n        <ion-button *ngIf=\"!cartAdded && postType === 'configurable_product' && '!variationProductStock'\" class=\"addtocart-btn\"\n          (click)=\"addToCartVariation(detailsContent.id,qty)\" [hidden]=\"variationProductStock\">\n          Add To Cart\n          <ion-spinner *ngIf=\"buttonClick\" name=\"dots\"></ion-spinner>\n        </ion-button>\n        <ion-button class=\"addtocart-btn\" *ngIf=\"variationProductStock\">\n          Out of Stock\n        </ion-button>\n        <ion-button\n          *ngIf=\"!cartAdded && checkStockOfProduct === 'In Stock'&& postType === 'simple_product' && '!variationProductStock'\"\n          class=\"addtocart-btn\" [hidden]=\"variationProductStock\" (click)=\"addToCart(detailsContent.id,qty)\">\n          Add To Cart\n          <ion-spinner *ngIf=\"buttonClick\" name=\"dots\"></ion-spinner>\n        </ion-button>\n        <ion-button *ngIf=\"cartAdded\" class=\"addtocart-btn\" routerLink=\"/cart\">\n          Go To Cart\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n          <div *ngIf = \"postType === 'configurable_product';else variationPriceContent \" class=\"single-product-price\">\n              <ion-label id=\"changePrice\" class=\"price\" >\n                {{priceDisplay | currency:'USD':'symbol'}}\n              </ion-label>\n            </div>\n            <ng-template #variationPriceContent>\n                <div class=\"single-product-price\">\n                    <ion-label id=\"changePrice\" class=\"price\">\n                      {{variationPrice | currency:'USD':'symbol'}}\n                    </ion-label>\n                  </div>\n            </ng-template>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>"

/***/ }),

/***/ "./src/app/product-detail/product-detail.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":root {\n  --ion-item-background-activated: #051b35; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.new-background-color {\n  --background: #b31117; }\n\n.selected {\n  background-color: #b31117 !important;\n  color: white !important;\n  outline: none;\n  box-shadow: none; }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 17px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 94px; }\n\n.star-rating {\n  margin: 0px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.container ::-webkit-scrollbar {\n  display: none; }\n\n.container .size-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n.size-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n.highlight-color {\n  color: blue; }\n\nion-slides {\n  color: #fff; }\n\nion-slides ion-slide {\n    align-items: start;\n    text-align: center; }\n\nion-slides ion-slide .slide-content .text p {\n      margin-top: 0;\n      margin-bottom: 10px; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\nion-toolbar.product-single-toolbar {\n  position: absolute;\n  top: 25px;\n  right: 0;\n  left: 0;\n  --background: transparent;\n  --ion-color-base: transparent !important; }\n\n.star-rating-section ul li {\n  display: inline-block;\n  padding-right: 10px;\n  color: #b31117; }\n\n.star-rating-section ul {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n\n.button-rating {\n  float: right; }\n\n.product-single-content {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.product-single-content h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.product-single-content p {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.product-single-size {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.product-single-size h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.btn-size-chart {\n  color: #80889a;\n  font-size: 11px;\n  font-weight: normal;\n  line-height: 24px;\n  background: none;\n  border: none;\n  --background: transparent;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  text-decoration: underline;\n  --background-activated: var(--background);\n  --box-shadow: none;\n  height: auto;\n  margin: 0; }\n\n.btn-size-chart.activated {\n  --background-activated: transparent;\n  --color: #80889a;\n  --color-activated: #80889a; }\n\nion-footer.product-single-footer {\n  background: #fff;\n  padding: 0px 20px;\n  box-shadow: 0 -4px 12px 0 rgba(158, 169, 181, 0.55); }\n\n.addtocart-btn {\n  color: #fff;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  width: 100%;\n  --background-activated:#b31117; }\n\nion-icon.ion-icon-increment {\n  font-size: 14px;\n  color: white; }\n\n.single-product-price {\n  padding: 10px 0;\n  text-align: center; }\n\n.single-product-price .price {\n  font-size: 20px;\n  font-weight: bold;\n  color: #213051; }\n\n.btn-increment {\n  background: #b31117;\n  border-radius: 5px;\n  color: white;\n  font-size: 18px;\n  padding: 8px 10px;\n  display: inline-block; }\n\n.quantity-value {\n  display: inline-block;\n  width: 40px;\n  text-align: center;\n  font-size: 20px;\n  font-weight: 800; }\n\n.btn-size {\n  background: white;\n  border: 1px solid #d4dee8;\n  color: #213051;\n  --background-activated: var(--ion-item-background-activated); }\n\n.button.active {\n  background-color: green !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3Byb2R1Y3QtZGV0YWlsL3Byb2R1Y3QtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdDQUFnQyxFQUFBOztBQUdwQztFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWEsRUFBQTs7QUFLakI7RUFDSSxvQ0FBb0M7RUFDcEMsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1Y7RUFDSSxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFFUSxhQUFhLEVBQUE7O0FBRnJCO0VBTVEsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsNEJBQTRCLEVBQUE7O0FBSXBDO0VBQ0ksbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxXQUFXLEVBQUE7O0FBRGY7SUFJUSxrQkFBa0I7SUFDbEIsa0JBQWtCLEVBQUE7O0FBTDFCO01BYWdCLGFBQWE7TUFDYixtQkFBbUIsRUFBQTs7QUFNbkM7RUFDSSxtQkFBbUI7RUFDbkI7QUFBYSxFQUFBOztBQUdqQjtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtFQUNSLE9BQU87RUFDUCx5QkFBYTtFQUNiLHdDQUFpQixFQUFBOztBQUVyQjtFQUNJLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsY0FDSixFQUFBOztBQUNBO0VBQ0ksZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxVQUFVLEVBQUE7O0FBRWQ7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBQ0ksYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsY0FBYyxFQUFBOztBQUdsQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWix5QkFBYTtFQUNiLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixvQkFBb0I7RUFDcEIsMEJBQTBCO0VBQzFCLHlDQUF1QjtFQUN2QixrQkFBYTtFQUNiLFlBQVk7RUFDWixTQUFTLEVBQUE7O0FBR2I7RUFDSSxtQ0FBdUI7RUFDdkIsZ0JBQVE7RUFDUiwwQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBRWpCLG1EQUFtRCxFQUFBOztBQUd2RDtFQUNJLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixvQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFNBQVM7RUFDVCxZQUFZO0VBQ1osbUJBQWM7RUFDZCxzQkFBaUI7RUFDakIscUJBQWdCO0VBQ2hCLG1CQUFjO0VBQ2QscUJBQWE7RUFDYixXQUFXO0VBQ1gsOEJBQXVCLEVBQUE7O0FBRTNCO0VBQ0ksZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFaEI7RUFDSSxlQUFlO0VBQ2Ysa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUVsQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGVBQWU7RUFDZixpQkFBaUI7RUFDakIscUJBQXFCLEVBQUE7O0FBRXpCO0VBQ0kscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVwQjtFQUNJLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsY0FBYztFQUNkLDREQUF1QixFQUFBOztBQUczQjtFQUNJLGtDQUFrQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOnJvb3Qge1xuICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwNTFiMzU7XG59XG5cbi5oZWFkZXItdGl0bGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlclxufVxuXG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cbi5oZWFkZXItYXJyb3d7XG4gICAgXG59XG4uc2VsZWN0ZWQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMzExMTcgIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5zdGFyLXJhdGluZyB7XG4gICAgZm9udC1mYW1pbHk6IFwiRm9udEF3ZXNvbWVcIjtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiA5NHB4O1xufVxuXG4uc3Rhci1yYXRpbmcge1xuICAgIG1hcmdpbjogMHB4IGF1dG87XG59XG5cbi5zdGFyLXJhdGluZzo6YmVmb3JlIHtcbiAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW4ge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGxlZnQ6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMS41ZW07XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW46OmJlZm9yZSB7XG4gICAgY29sb3I6ICNmYWI5MDI7XG4gICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbGVmdDogMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyAucmF0aW5nIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICA6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICAuc2l6ZS1zY3JvbGwge1xuICAgICAgICBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2IzMTExNztcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcbiAgICB9XG59XG5cbi5zaXplLXNwYW4ge1xuICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZDogIzg0MTAxNDtcbiAgICBwYWRkaW5nOiA4cHggMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5oaWdobGlnaHQtY29sb3Ige1xuICAgIGNvbG9yOiBibHVlO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgICBjb2xvcjogI2ZmZjtcblxuICAgIGlvbi1zbGlkZSB7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBzdGFydDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgIC5zbGlkZS1jb250ZW50IHtcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDBweCAxNCU7XG5cblxuXG4gICAgICAgICAgICAudGV4dCBwIHtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZjRmOGZjKVxufVxuXG5pb24tdG9vbGJhci5wcm9kdWN0LXNpbmdsZS10b29sYmFyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAyNXB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuLnN0YXItcmF0aW5nLXNlY3Rpb24gdWwgbGkge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGNvbG9yOiAjYjMxMTE3XG59XG4uc3Rhci1yYXRpbmctc2VjdGlvbiB1bCB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbn1cbi5idXR0b24tcmF0aW5nIHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gIH1cbi5wcm9kdWN0LXNpbmdsZS1jb250ZW50IHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLnByb2R1Y3Qtc2luZ2xlLWNvbnRlbnQgaDIge1xuICAgIGNvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG59XG5cbi5wcm9kdWN0LXNpbmdsZS1jb250ZW50IHAge1xuICAgIGNvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xufVxuXG4ucHJvZHVjdC1zaW5nbGUtc2l6ZSB7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5wcm9kdWN0LXNpbmdsZS1zaXplIGgyIHtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uYnRuLXNpemUtY2hhcnQge1xuICAgIGNvbG9yOiAjODA4ODlhO1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgcGFkZGluZzogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMDtcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiB2YXIoLS1iYWNrZ3JvdW5kKTtcbiAgICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmJ0bi1zaXplLWNoYXJ0LmFjdGl2YXRlZCB7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1jb2xvcjogIzgwODg5YTtcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogIzgwODg5YTtcbn1cblxuaW9uLWZvb3Rlci5wcm9kdWN0LXNpbmdsZS1mb290ZXIge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgcGFkZGluZzogMHB4IDIwcHg7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIC00cHggMTJweCAwIHJnYmEoMTU4LCAxNjksIDE4MSwgMC41NSk7XG4gICAgYm94LXNoYWRvdzogMCAtNHB4IDEycHggMCByZ2JhKDE1OCwgMTY5LCAxODEsIDAuNTUpO1xufVxuXG4uYWRkdG9jYXJ0LWJ0biB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLS1wYWRkaW5nLXRvcDogMTVweDtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMzBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAzMHB4O1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiNiMzExMTc7XG59XG5pb24taWNvbi5pb24taWNvbi1pbmNyZW1lbnQge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogd2hpdGU7XG59XG4uc2luZ2xlLXByb2R1Y3QtcHJpY2Uge1xuICAgIHBhZGRpbmc6IDEwcHggMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuLnNpbmdsZS1wcm9kdWN0LXByaWNlIC5wcmljZSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiAjMjEzMDUxO1xufVxuLmJ0bi1pbmNyZW1lbnR7XG4gICAgYmFja2dyb3VuZDogI2IzMTExNztcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBwYWRkaW5nOiA4cHggMTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4ucXVhbnRpdHktdmFsdWUge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogNDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG4uYnRuLXNpemUge1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNkNGRlZTg7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZC1hY3RpdmF0ZWQpO1xufVxuXG4uYnV0dG9uLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcbn1cblxuIl19 */"

/***/ }),

/***/ "./src/app/product-detail/product-detail.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.ts ***!
  \*******************************************************/
/*! exports provided: ProductDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPage", function() { return ProductDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sizechart_modal_sizechart_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../sizechart-modal/sizechart-modal.page */ "./src/app/sizechart-modal/sizechart-modal.page.ts");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _image_modal_image_modal_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../image-modal/image-modal.page */ "./src/app/image-modal/image-modal.page.ts");
/* harmony import */ var _rating_modal_rating_modal_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../rating-modal/rating-modal.page */ "./src/app/rating-modal/rating-modal.page.ts");












var ProductDetailPage = /** @class */ (function () {
    function ProductDetailPage(alert, events, loadingService, route, toastController, cartService, homepageService, navCtrl, modalController) {
        var _this = this;
        this.alert = alert;
        this.events = events;
        this.loadingService = loadingService;
        this.route = route;
        this.toastController = toastController;
        this.cartService = cartService;
        this.homepageService = homepageService;
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.bannerSlider = {
            initialSlide: 0,
            speed: 2000,
            slidesPerView: 1,
            autoplay: true,
        };
        this.productType = '';
        this.manageStock = '';
        this.postType = '';
        this.sizeNumberArray = [];
        this.checkStockOfProduct = '';
        this.apiUrl = "https://www.niletechinnovations.com/eq8tor";
        this.sizeArray = [];
        this.colorArray = [];
        this.variationData = [];
        this.priceDisplay = '';
        this.variationID = '';
        this.selectedColor = '';
        this.selectedSize = '';
        this.variationPrice = '';
        this.jsonDataVariation = [];
        this.buttonClick = false;
        this.variationProductStock = false;
        this.route.queryParams.subscribe(function (params) {
            console.log("params", params);
            var productName = params.id;
            console.log("product name", productName);
            _this.getProductDetail(productName);
        });
        this.qty = 1;
    }
    ProductDetailPage.prototype.ngOnInit = function () {
        this.productType = '';
        this.sizeArray = [];
        this.colorArray = [];
        this.variationData = [];
        this.variationID = '';
        this.selectedColor = '';
        this.selectedSize = '';
        this.variationPrice = '';
        this.jsonDataVariation = [];
        this.sizeNumberArray = [];
        console.log('size number array in ngoninit', this.sizeNumberArray);
    };
    ProductDetailPage.prototype.viewWillAppear = function () {
        console.log('viewWillAppear');
    };
    ProductDetailPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter');
        this.cartAdded = false;
        this.variationProductStock = false;
    };
    ProductDetailPage.prototype.openPreview = function (img) {
        this.modalController.create({
            component: _image_modal_image_modal_page__WEBPACK_IMPORTED_MODULE_9__["ImageModalPage"],
            componentProps: {
                img: img
            }
        }).then(function (modal) {
            modal.present();
        });
    };
    ProductDetailPage.prototype.getProductDetail = function (product) {
        var _this = this;
        this.sizeArray = [];
        console.log('this.sizeArray', this.sizeArray);
        this.colorArray = [];
        this.productType = '';
        this.loadingService.present();
        this.homepageService.productDetail(product).subscribe(function (response) {
            var resp = response.data.single_product_details;
            _this.postType = resp.post_type;
            _this.manageStock = resp['_product_manage_stock'];
            if (_this.manageStock === 'yes') {
                _this.productQuantityAvailable = resp.post_stock_qty;
            }
            _this.checkStockOfProduct = resp.post_stock_availability;
            if (_this.checkStockOfProduct == 'in_stock') {
                _this.checkStockOfProduct = 'In Stock';
            }
            var dataChoice = response.data.variations_data;
            _this.totalRating = response.data._rating.total;
            _this.averageRating = response.data._rating.average;
            console.log("total rating", _this.totalRating);
            if (dataChoice.length > 0) {
                _this.jsonDataVariation = response.data.variations_data;
                _this.priceDisplay = dataChoice[0]._variation_post_regular_price;
                for (var i = 0; i < _this.jsonDataVariation.length; i++) {
                    _this.variationData = _this.jsonDataVariation[i]['_variation_array_data'];
                    _this.productType = 'variation';
                    console.log('variationData', _this.variationData);
                    var number = _this.variationData[1]['attr_val'];
                    var color = _this.variationData[0]['attr_val'];
                    _this.sizeArray.push({
                        "size": number,
                        "color": color,
                        "isSelected": false
                    });
                    _this.colorArray.push({
                        "color": color,
                        "isSelected": false
                    });
                }
                _this.colorArray = _this.removeDuplicates(_this.colorArray, "color");
                _this.sizeArray = _this.removeDuplicates(_this.sizeArray, "size");
                console.log("testArray", _this.colorArray);
                console.log("size array", _this.sizeArray);
            }
            _this.detailsContent = resp;
            _this.variationPrice = _this.detailsContent.post_price;
            _this.detailImageList = resp._product_related_images_url.product_gallery_images;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    ProductDetailPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    };
    ProductDetailPage.prototype.btnSize = function (ind) {
        for (var i = 0; i < this.sizeArray.length; i++) {
            this.sizeArray[i].isSelected = false;
        }
        this.sizeArray[ind].isSelected = true;
        console.log('', this.sizeArray[ind].size);
        console.log(this.colorArray);
        this.selectedSize = this.sizeArray[ind].size;
        if ((this.selectedSize == '' && this.selectedColor == '') || (this.selectedSize == '') || (this.selectedColor == '')) {
            this.presentToast('Please select Color.');
        }
        else {
            this.filterData();
            this.qty = 1;
        }
    };
    ProductDetailPage.prototype.btnColor = function (ind) {
        for (var i = 0; i < this.colorArray.length; i++) {
            this.colorArray[i].isSelected = false;
        }
        this.colorArray[ind].isSelected = true;
        console.log('', this.colorArray[ind].color);
        this.selectedColor = this.colorArray[ind].color;
        if ((this.selectedSize == '' && this.selectedColor == '') || (this.selectedSize == '') || (this.selectedColor == '')) {
            this.presentToast('Please select size.');
        }
        else {
            this.filterData();
            this.qty = 1;
        }
    };
    ProductDetailPage.prototype.filterData = function () {
        console.log(this.selectedColor, this.selectedSize);
        for (var i = 0; i < this.jsonDataVariation.length; i++) {
            var filterData = this.jsonDataVariation[i]['_variation_array_data'];
            var number = filterData[1]['attr_val'];
            var color = filterData[0]['attr_val'];
            if (number == this.selectedSize && color == this.selectedColor) {
                var filteredData = filterData;
                console.log("filteredData", filteredData);
                console.log('', this.jsonDataVariation[i]);
                var salePrice = this.jsonDataVariation[i]['_variation_post_sale_price'];
                if (salePrice == '0') {
                    this.variationPrice = this.jsonDataVariation[i]['_variation_post_regular_price'];
                    console.log("variation price", this.variationPrice);
                }
                else {
                    this.variationPrice = salePrice;
                    console.log("sale Price", salePrice);
                }
                this.variationID = this.jsonDataVariation[i].id;
                console.log(this.variationID);
                this.variationProductQuantityAvailable = this.jsonDataVariation[i]['_variation_post_manage_stock_qty'];
                console.log("this.variationProductQuantityAvailable", this.variationProductQuantityAvailable);
                if (this.variationProductQuantityAvailable == 0) {
                    this.variationProductStock = true;
                }
                document.getElementById('changePrice').innerText = "$" + this.variationPrice;
                //console.log((document.getElementById('variationImage') as HTMLImageElement).src);
                console.log('this.jsonDataVariation[i]', this.jsonDataVariation[i]['_variation_post_img_url']);
                this.variationImage = this.jsonDataVariation[i]['_variation_post_img_url'];
                this.variationImage = document.getElementById('variationImage');
            }
        }
    };
    ProductDetailPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ProductDetailPage.prototype.getCartContent = function () {
        console.log("getCartContent");
        this.homepageService.getCartProducts().subscribe(function (resp) {
            console.log("getCartContent", JSON.stringify(resp));
        }, function (err) {
            console.log("getCartContent error", err);
        });
    };
    ProductDetailPage.prototype.openSizeChart = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _sizechart_modal_sizechart_modal_page__WEBPACK_IMPORTED_MODULE_5__["SizechartModalPage"],
                            componentProps: {
                                "testParam": 123,
                                "testurl": "https://www.google.com"
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                _this.sizeChartResponse = sizeChartResponse.data;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ProductDetailPage.prototype.incrementQty = function () {
        if (this.manageStock == 'yes') {
            if (this.qty < this.productQuantityAvailable) {
                this.qty += 1;
            }
            if (this.qty == this.productQuantityAvailable) {
                this.presentToast('You Cannot add more than ' + this.productQuantityAvailable + ' products.');
            }
        }
        else if (this.productType == 'variation') {
            if (this.selectedColor == '' || this.selectedSize == '') {
                this.presentToast('please select the color and size both');
                console.log('product type variation');
            }
            if (this.qty < this.variationProductQuantityAvailable) {
                this.qty += 1;
            }
            if (this.qty == this.variationProductQuantityAvailable) {
                this.presentToast('You Cannot add more than ' + this.variationProductQuantityAvailable + ' products.');
            }
            if (this.qty > this.variationProductQuantityAvailable) {
                this.presentToast('product out of stock ' + this.variationProductQuantityAvailable + ' products.');
            }
        }
        else {
            this.qty += 1;
        }
    };
    ProductDetailPage.prototype.openRatingModal = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _rating_modal_rating_modal_page__WEBPACK_IMPORTED_MODULE_10__["RatingModalPage"],
                            componentProps: {
                                "idProduct": id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                _this.sizeChartResponse = sizeChartResponse.data;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ProductDetailPage.prototype.decrementQty = function () {
        if (this.qty - 1 < 1) {
            this.qty = 1;
            console.log('1->' + this.qty);
        }
        else {
            this.qty -= 1;
            console.log('2->' + this.qty);
        }
    };
    ProductDetailPage.prototype.addToCartVariation = function (productID, Quantity) {
        var _this = this;
        if (this.selectedColor == '' || this.selectedColor == null) {
            this.presentToast('Please select the color');
        }
        else if (this.selectedSize == '' || this.selectedSize == null) {
            this.presentToast('Please select the size');
        }
        else {
            this.buttonClick = true;
            this.homepageService.addToCart(productID, Quantity, this.variationID, this.selectedColor, this.selectedSize).subscribe(function (resp) {
                console.log("response add to cart", resp);
                _this.presentToast('Product Added to cart.');
                _this.cartAdded = true;
                _this.buttonClick = false;
            }, function (err) {
                console.log("error add to cart", err);
                _this.presentToast('error add to cart.');
                _this.buttonClick = false;
            });
        }
    };
    ProductDetailPage.prototype.addToCart = function (productID, Quantity) {
        var _this = this;
        this.buttonClick = true;
        this.homepageService.addToCart(productID, Quantity, '', '', '').subscribe(function (resp) {
            console.log("response add to cart", resp);
            _this.presentToast('Product Added to cart.');
            _this.cartAdded = true;
            _this.buttonClick = false;
        }, function (err) {
            console.log("error add to cart", err);
            _this.presentToast('error add to cart.');
            _this.buttonClick = false;
        });
    };
    ProductDetailPage.prototype.presentToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonSlides"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonSlides"])
    ], ProductDetailPage.prototype, "slides", void 0);
    ProductDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! ./product-detail.page.html */ "./src/app/product-detail/product-detail.page.html"),
            styles: [__webpack_require__(/*! ./product-detail.page.scss */ "./src/app/product-detail/product-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__["AlerServiceService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__["CartService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])
    ], ProductDetailPage);
    return ProductDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=product-detail-product-detail-module.js.map