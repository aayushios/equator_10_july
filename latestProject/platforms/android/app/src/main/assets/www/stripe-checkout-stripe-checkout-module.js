(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stripe-checkout-stripe-checkout-module"],{

/***/ "./src/app/stripe-checkout/stripe-checkout.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/stripe-checkout/stripe-checkout.module.ts ***!
  \***********************************************************/
/*! exports provided: StripeCheckoutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StripeCheckoutPageModule", function() { return StripeCheckoutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _stripe_checkout_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./stripe-checkout.page */ "./src/app/stripe-checkout/stripe-checkout.page.ts");







var routes = [
    {
        path: '',
        component: _stripe_checkout_page__WEBPACK_IMPORTED_MODULE_6__["StripeCheckoutPage"]
    }
];
var StripeCheckoutPageModule = /** @class */ (function () {
    function StripeCheckoutPageModule() {
    }
    StripeCheckoutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_stripe_checkout_page__WEBPACK_IMPORTED_MODULE_6__["StripeCheckoutPage"]]
        })
    ], StripeCheckoutPageModule);
    return StripeCheckoutPageModule;
}());



/***/ }),

/***/ "./src/app/stripe-checkout/stripe-checkout.page.html":
/*!***********************************************************!*\
  !*** ./src/app/stripe-checkout/stripe-checkout.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Payment</ion-title>\n    <ion-buttons slot=\"start\">\n      <!-- <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button> -->\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"payment-content\" padding>\n  <div class=\"payment-card\">\n    <div [formGroup]=\"cardDetails\">\n      <ion-row>\n        <ion-col size=\"12\">\n          <div class=\"form-group\">\n            <label class=\"form-label-text\">Card Number</label>\n            <ion-input type=\"tel\" class=\"form-control-input\" maxlength=\"16\" placeholder=\"**** **** **** ****\"\n              (keydown.space)=\"$event.preventDefault();\" formControlName=\"cardNumber\">\n            </ion-input>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\">\n          <div class=\"form-group\">\n            <label class=\"form-label-text\">Month</label>\n            <ion-select name=\"Month\" class=\"form-control-select\" placeholder=\"Month\" formControlName='Month'>\n              <ion-select-option value=\"01\">January</ion-select-option>\n              <ion-select-option value=\"02\">February</ion-select-option>\n              <ion-select-option value=\"03\">March</ion-select-option>\n              <ion-select-option value=\"04\">April</ion-select-option>\n              <ion-select-option value=\"05\">May</ion-select-option>\n              <ion-select-option value=\"06\">June</ion-select-option>\n              <ion-select-option value=\"07\">July</ion-select-option>\n              <ion-select-option value=\"08\">August</ion-select-option>\n              <ion-select-option value=\"09\">September</ion-select-option>\n              <ion-select-option value=\"10\">October</ion-select-option>\n              <ion-select-option value=\"11\">November</ion-select-option>\n              <ion-select-option value=\"12\">December</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-col>\n        <ion-col size=\"4\">\n          <div class=\"form-group\">\n            <label class=\"form-label-text\">Year</label>\n            <ion-select name=\"Year\" class=\"form-control-select\" placeholder=\"Year\" formControlName='Year'>\n              <ion-select-option value=\"2020\">2020</ion-select-option>\n              <ion-select-option value=\"2021\">2021</ion-select-option>\n              <ion-select-option value=\"2022\">2022</ion-select-option>\n              <ion-select-option value=\"2023\">2023</ion-select-option>\n              <ion-select-option value=\"2024\">2024</ion-select-option>\n              <ion-select-option value=\"2025\">2025</ion-select-option>\n              <ion-select-option value=\"2026\">2026</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-col>\n        <ion-col size=\"4\">\n          <div class=\"form-group\">\n            <label class=\"form-label-text\">CVV</label>\n            <ion-input placeholder=\"CVV\" class=\"form-control-input\" (keydown.space)=\"$event.preventDefault();\"\n              formControlName=\"cvv\">\n            </ion-input>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <div class=\"form-group\">\n            <ion-button class=\"btn-checkout\" type=\"submit\" (click)=\"sendDetails(cardDetails.value)\" [disabled]=\"disableButton\">\n              Checkout\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col size=\"12\">\n            <div class=\"form-group\">\n              <ion-button class=\"btn-cancel\" type=\"submit\" (click)=\"goBack()\" [disabled]=\"disableButton\">\n                Cancel\n              </ion-button>\n            </div>\n          </ion-col>\n      </ion-row>\n    </div>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/stripe-checkout/stripe-checkout.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/stripe-checkout/stripe-checkout.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n:host .back-arrow {\n  color: #fff; }\n\n:host * {\n  box-sizing: border-box; }\n\n:host body,\n:host html {\n  height: 100%;\n  min-height: 100%; }\n\n:host body {\n  font-family: 'Roboto', sans-serif;\n  margin: 0;\n  background-color: #e7e7e7; }\n\n:host .credit-card {\n  width: 360px;\n  height: 400px;\n  margin: 60px auto 0;\n  border: 1px solid #ddd;\n  border-radius: 6px;\n  background-color: #fff;\n  box-shadow: 1px 2px 3px 0 rgba(0, 0, 0, 0.1); }\n\n:host .title {\n  font-size: 18px;\n  margin: 0;\n  color: #5e6977; }\n\n:host .payment-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc); }\n\n:host .payment-card {\n  padding: 10px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n:host .form-control-input {\n  border: 1px solid #eee;\n  padding: 10px;\n  font-size: 12px;\n  border-radius: 2px;\n  --padding-top: 12px;\n  --padding-end: 0;\n  --padding-bottom: 12px;\n  --padding-start: 8px;\n  color: #524e4e; }\n\n:host .form-control-select {\n  border: 1px solid #eee;\n  padding: 10px;\n  font-size: 13px;\n  border-radius: 2px; }\n\n:host .form-label-text {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600; }\n\n:host .btn-checkout {\n  color: #fff;\n  font-weight: bolder;\n  font-size: 10px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated: #b31117;\n  width: 100%; }\n\n:host .btn-cancel {\n  color: #fff;\n  font-weight: bolder;\n  font-size: 10px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #003954;\n  --background-activated: #003954;\n  width: 100%; }\n\n:host .cvv-input input,\n:host .month select,\n:host .year select {\n  font-size: 14px;\n  font-weight: 100;\n  line-height: 14px; }\n\n:host .card-number,\n:host .month select,\n:host .year select {\n  font-size: 14px;\n  font-weight: 100;\n  line-height: 14px; }\n\n:host .card-number,\n:host .cvv-details,\n:host .cvv-input input,\n:host .month select,\n:host .year select {\n  opacity: .7;\n  color: #86939e; }\n\n:host .card-number {\n  width: 100%;\n  margin-bottom: 20px;\n  padding-left: 20px;\n  border: 2px solid #e1e8ee;\n  border-radius: 6px; }\n\n:host .month select,\n:host .year select {\n  width: 145px;\n  margin-bottom: 20px;\n  padding-left: 20px;\n  border: 2px solid #e1e8ee;\n  border-radius: 6px;\n  background-position: 85% 50%;\n  -moz-appearance: none;\n  -webkit-appearance: none; }\n\n:host .month select {\n  float: left; }\n\n:host .year select {\n  float: right; }\n\n:host .cvv-input input {\n  float: left;\n  width: 145px;\n  padding-left: 20px;\n  border: 2px solid #e1e8ee;\n  border-radius: 6px;\n  background: #fff; }\n\n:host .cvv-details {\n  font-size: 12px;\n  font-weight: 300;\n  line-height: 16px;\n  float: right;\n  margin-bottom: 20px; }\n\n:host .cvv-details p {\n  margin-top: 6px; }\n\n:host .proceed-btn {\n  cursor: pointer;\n  font-size: 16px;\n  width: 100%;\n  border-color: transparent;\n  border-radius: 6px; }\n\n:host .proceed-btn {\n  margin-bottom: 10px;\n  background: #7dc855; }\n\n:host .paypal-btn a,\n:host .proceed-btn a {\n  text-decoration: none; }\n\n:host .proceed-btn a {\n  color: #fff; }\n\n:host .paypal-btn a {\n  color: rgba(242, 242, 242, 0.7); }\n\n:host .paypal-btn {\n  padding-right: 95px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3N0cmlwZS1jaGVja291dC9zdHJpcGUtY2hlY2tvdXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUkscUJBQWEsRUFBQTs7QUFGakI7RUFLUSxXQUFVO0VBQ1Ysa0JBQ0osRUFBQTs7QUFQSjtFQVNRLFdBQVcsRUFBQTs7QUFUbkI7RUFZUSxzQkFBc0IsRUFBQTs7QUFaOUI7O0VBaUJRLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTs7QUFsQnhCO0VBc0JRLGlDQUNVO0VBQ1YsU0FBUztFQUNULHlCQUF5QixFQUFBOztBQXpCakM7RUE0QlEsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsNENBQXlDLEVBQUE7O0FBbENqRDtFQXFDUSxlQUFlO0VBQ2YsU0FBUztFQUNULGNBQWMsRUFBQTs7QUF2Q3RCO0VBNENJLG1CQUFtQjtFQUNuQixpREFBYSxFQUFBOztBQTdDakI7RUFpREksYUFBWTtFQUNaLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFuRHZCO0VBc0RvQixzQkFBc0I7RUFDdEMsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQWM7RUFDZCxnQkFBYztFQUNkLHNCQUFpQjtFQUNqQixvQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQTlEbEI7RUFnRXFCLHNCQUFzQjtFQUMzQyxhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQW5FbEI7RUFvRXFCLGNBQWM7RUFDL0IsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQXRFcEI7RUEwRUksV0FBVztFQUNYLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixTQUFTO0VBQ1QsWUFBWTtFQUNaLG1CQUFjO0VBQ2Qsc0JBQWlCO0VBQ2pCLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLHFCQUFhO0VBQ2IsK0JBQXVCO0VBQ3ZCLFdBQVcsRUFBQTs7QUF2RmY7RUEyRkksV0FBVztFQUNYLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixTQUFTO0VBQ1QsWUFBWTtFQUNaLG1CQUFjO0VBQ2Qsc0JBQWlCO0VBQ2pCLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLHFCQUFhO0VBQ2IsK0JBQXVCO0VBQ3ZCLFdBQVcsRUFBQTs7QUF4R2Y7OztFQStHSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQWpIckI7OztFQXVISSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQXpIckI7Ozs7O0VBaUlJLFdBQVc7RUFDWCxjQUFjLEVBQUE7O0FBbElsQjtFQXFJSSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIsa0JBQWtCLEVBQUE7O0FBekl0Qjs7RUE2SUksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUVsQiw0QkFBNEI7RUFDNUIscUJBQXFCO0VBQ3JCLHdCQUF3QixFQUFBOztBQXJKNUI7RUF5SkksV0FBVyxFQUFBOztBQXpKZjtFQTZKSSxZQUFZLEVBQUE7O0FBN0poQjtFQWdLSSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQXJLcEI7RUF5S0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLG1CQUFtQixFQUFBOztBQTdLdkI7RUFpTEksZUFBZSxFQUFBOztBQWpMbkI7RUFvTEksZUFBZTtFQUNmLGVBQWU7RUFDZixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLGtCQUFrQixFQUFBOztBQXhMdEI7RUE0TEksbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQTdMdkI7O0VBa01JLHFCQUFxQixFQUFBOztBQWxNekI7RUFzTUksV0FBVyxFQUFBOztBQXRNZjtFQTBNSSwrQkFBOEIsRUFBQTs7QUExTWxDO0VBOE1JLG1CQUFtQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc3RyaXBlLWNoZWNrb3V0L3N0cmlwZS1jaGVja291dC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgfVxuICAgIC5oZWFkZXItdGl0bGV7XG4gICAgICAgIGNvbG9yOiNmZmY7IFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbiAgICB9XG4gICAgLmJhY2stYXJyb3d7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgIH1cbiAgICAqe1xuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIH1cbiAgICAgXG4gICAgYm9keSxcbiAgICBodG1sIHtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBtaW4taGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAgXG4gICAgYm9keSB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJyxcbiAgICAgICAgc2Fucy1zZXJpZjtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTdlN2U3O1xuICAgIH1cbiAgICAuY3JlZGl0LWNhcmQge1xuICAgICAgICB3aWR0aDogMzYwcHg7XG4gICAgICAgIGhlaWdodDogNDAwcHg7XG4gICAgICAgIG1hcmdpbjogNjBweCBhdXRvIDA7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICAgICAgYm94LXNoYWRvdzogMXB4IDJweCAzcHggMCByZ2JhKDAsMCwwLC4xMCk7XG4gICAgfVxuICAgIC50aXRsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBjb2xvcjogIzVlNjk3NztcbiAgICB9XG5cblxuLnBheW1lbnQtY29udGVudHtcbiAgICBiYWNrZ3JvdW5kOiAjZjRmOGZjO1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsI2Y0ZjhmYyk7XG59XG5cbi5wYXltZW50LWNhcmR7XG4gICAgcGFkZGluZzoxMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLmZvcm0tY29udHJvbC1pbnB1dHtib3JkZXI6IDFweCBzb2xpZCAjZWVlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAtLXBhZGRpbmctdG9wOiAxMnB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDA7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDhweDtcbiAgICBjb2xvcjogIzUyNGU0ZTt9XG5cbi5mb3JtLWNvbnRyb2wtc2VsZWN0e2JvcmRlcjogMXB4IHNvbGlkICNlZWU7XG5wYWRkaW5nOiAxMHB4O1xuZm9udC1zaXplOiAxM3B4O1xuYm9yZGVyLXJhZGl1czogMnB4O31cbi5mb3JtLWxhYmVsLXRleHR7ICAgIGNvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogNjAwO31cblxuXG4uYnRuLWNoZWNrb3V0e1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC0tcGFkZGluZy10b3A6IDE1cHg7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDMwcHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMzBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI2IzMTExNztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmJ0bi1jYW5jZWx7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLS1wYWRkaW5nLXRvcDogMTVweDtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMzBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAzMHB4O1xuICAgIC0tYmFja2dyb3VuZDogIzAwMzk1NDtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMDAzOTU0O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5cbiAgICAuY3Z2LWlucHV0IGlucHV0LFxuLm1vbnRoIHNlbGVjdCxcbi55ZWFyIHNlbGVjdCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG59XG4gXG4uY2FyZC1udW1iZXIsXG4ubW9udGggc2VsZWN0LFxuLnllYXIgc2VsZWN0IHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbn1cbiBcbi5jYXJkLW51bWJlcixcbi5jdnYtZGV0YWlscyxcbi5jdnYtaW5wdXQgaW5wdXQsXG4ubW9udGggc2VsZWN0LFxuLnllYXIgc2VsZWN0IHtcbiAgICBvcGFjaXR5OiAuNztcbiAgICBjb2xvcjogIzg2OTM5ZTtcbn1cbi5jYXJkLW51bWJlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2UxZThlZTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG59XG4ubW9udGggc2VsZWN0LFxuLnllYXIgc2VsZWN0IHtcbiAgICB3aWR0aDogMTQ1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2UxZThlZTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgLy8gYmFja2dyb3VuZDogdXJsKCdjYXJldC5wbmcnKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogODUlIDUwJTtcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7XG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xufVxuIFxuLm1vbnRoIHNlbGVjdCB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG4gXG4ueWVhciBzZWxlY3Qge1xuICAgIGZsb2F0OiByaWdodDtcbn1cbi5jdnYtaW5wdXQgaW5wdXQge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHdpZHRoOiAxNDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2UxZThlZTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbn1cbiBcbi5jdnYtZGV0YWlscyB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4gXG4uY3Z2LWRldGFpbHMgcCB7XG4gICAgbWFyZ2luLXRvcDogNnB4O1xufVxuLnByb2NlZWQtYnRuIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xufVxuIFxuLnByb2NlZWQtYnRuIHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6ICM3ZGM4NTU7XG59XG4gXG4ucGF5cGFsLWJ0biBhLFxuLnByb2NlZWQtYnRuIGEge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbiBcbi5wcm9jZWVkLWJ0biBhIHtcbiAgICBjb2xvcjogI2ZmZjtcbn1cbiBcbi5wYXlwYWwtYnRuIGEge1xuICAgIGNvbG9yOiByZ2JhKDI0MiwgMjQyLCAyNDIsIC43KTtcbn1cbiBcbi5wYXlwYWwtYnRuIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA5NXB4O1xufVxufSAiXX0= */"

/***/ }),

/***/ "./src/app/stripe-checkout/stripe-checkout.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/stripe-checkout/stripe-checkout.page.ts ***!
  \*********************************************************/
/*! exports provided: StripeCheckoutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StripeCheckoutPage", function() { return StripeCheckoutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/stripe/ngx */ "./node_modules/@ionic-native/stripe/ngx/index.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");









var StripeCheckoutPage = /** @class */ (function () {
    function StripeCheckoutPage(navCtrl, router, menuCtrl, loadingService, route, alert, homePageService, formBuilder, stripe) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.loadingService = loadingService;
        this.route = route;
        this.alert = alert;
        this.homePageService = homePageService;
        this.formBuilder = formBuilder;
        this.stripe = stripe;
        this.cardNumber = "";
        this.totalAmount = "";
        this.membershipDiscount = "";
        this.isMember = "";
        this.totalTax = "";
        this.disableButton = false;
        // angular.module('Eq8tor', [
        //   require('angular-credit-cards')
        // ]);
        this.route.queryParams.subscribe(function (params) {
            console.log("params", params);
            _this.totalAmount = params.total_amount;
            _this.membershipDiscount = params.membership_discount;
            _this.totalTax = params.total_tax;
            _this.isMember = params.is_member;
        });
        this.stripe.setPublishableKey('pk_test_4sjCZIFhfIeMDj3bpJsFapZf');
        this.cardDetails = this.formBuilder.group({
            cardNumber: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            Month: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            Year: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            cvv: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    }
    StripeCheckoutPage.prototype.ngOnInit = function () {
        this.disableButton = false;
    };
    StripeCheckoutPage.prototype.sendDetails = function (form) {
        var _this = this;
        console.log(form);
        if (form["cardNumber"] == "" || form["Month"] == "" || form["Year"] == "" || form["cvv"] == "") {
            this.alert.myAlertMethod("Please enter Mandatory Field.", '', function (data) {
                console.log("data", data);
            });
            return;
        }
        console.log("hr=e", this.cardNumber, this.Month, this.Year, this.cvv);
        var card = {
            number: form["cardNumber"],
            expMonth: form["Month"],
            expYear: form["Year"],
            cvc: form["cvv"]
        };
        console.log(card);
        this.disableButton = true;
        this.stripe.createCardToken(card)
            .then(function (token) {
            console.log("token received", token.id);
            _this.loadingService.present();
            _this.homePageService.stripeCheckoutApi('stripe', token.id, _this.totalAmount, _this.totalTax, _this.isMember, _this.membershipDiscount).subscribe(function (resp) {
                console.log("stripe response", JSON.stringify(resp));
                _this.loadingService.dismiss();
                if (resp.success == true) {
                    _this.alert.myAlertMethod("Successfully", resp.message, function (data) {
                        console.log("data", data);
                        _this.router.navigate(['your-orders']);
                    });
                }
                else {
                    _this.alert.myAlertMethod("Something went wrong. Please try again after sometime.", resp.message, function (data) {
                        console.log("Something went wrong.");
                    });
                }
            }, function (err) {
                _this.alert.myAlertMethod("error", err.message, function (data) {
                    console.log("error");
                });
                console.log(err);
                _this.loadingService.dismiss();
            });
        })
            .catch(function (error) { return console.error(error); });
    };
    StripeCheckoutPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    StripeCheckoutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-stripe-checkout',
            template: __webpack_require__(/*! ./stripe-checkout.page.html */ "./src/app/stripe-checkout/stripe-checkout.page.html"),
            styles: [__webpack_require__(/*! ./stripe-checkout.page.scss */ "./src/app/stripe-checkout/stripe-checkout.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_7__["AlerServiceService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__["HomepageService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_4__["Stripe"]])
    ], StripeCheckoutPage);
    return StripeCheckoutPage;
}());



/***/ })

}]);
//# sourceMappingURL=stripe-checkout-stripe-checkout-module.js.map