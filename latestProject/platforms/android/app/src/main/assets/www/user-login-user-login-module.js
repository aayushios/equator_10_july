(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-login-user-login-module"],{

/***/ "./src/app/user-login/user-login.module.ts":
/*!*************************************************!*\
  !*** ./src/app/user-login/user-login.module.ts ***!
  \*************************************************/
/*! exports provided: UserLoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoginPageModule", function() { return UserLoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-login.page */ "./src/app/user-login/user-login.page.ts");







var routes = [
    {
        path: '',
        component: _user_login_page__WEBPACK_IMPORTED_MODULE_6__["UserLoginPage"]
    }
];
var UserLoginPageModule = /** @class */ (function () {
    function UserLoginPageModule() {
    }
    UserLoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_user_login_page__WEBPACK_IMPORTED_MODULE_6__["UserLoginPage"]]
        })
    ], UserLoginPageModule);
    return UserLoginPageModule;
}());



/***/ }),

/***/ "./src/app/user-login/user-login.page.html":
/*!*************************************************!*\
  !*** ./src/app/user-login/user-login.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding class=\"background\">\n  <div class=\"home-screen-content\">\n  <div class=\"logo\">\n      <img src=\"../../assets/imgs/logo.png\" height=\"120px\">\n  </div>\n  <div class=\"heading-title\">\n    <h1>\n      Shop For Leading Brands\n      <br>\n     At Factory Pricing\n    </h1>\n  </div>\n  <div >\n    <ion-button routerLink=\"/login\" class=\"loginBtn\" expand=\"block\">Login</ion-button>\n  </div>\n  <div>\n    <ion-button routerLink=\"/signup\" class=\"SignUpBtn\" expand=\"block\">Sign Up</ion-button>\n  </div>\n</div>\n</ion-content>"

/***/ }),

/***/ "./src/app/user-login/user-login.page.scss":
/*!*************************************************!*\
  !*** ./src/app/user-login/user-login.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content.background {\n  --background: url('welcome.jpg') 0 0/100% 100% no-repeat; }\n\n:host .logo {\n  margin-bottom: 3rem;\n  margin-left: auto;\n  margin-right: auto;\n  text-align: center; }\n\n:host .heading-title {\n  margin-bottom: 3rem; }\n\n:host .heading-title h1 {\n  font-size: 22px;\n  margin: 0;\n  padding: 0;\n  font-weight: 700;\n  color: #fff;\n  text-align: center;\n  line-height: 35px;\n  font-family: 'Montserrat', sans-serif; }\n\n:host ion-button.loginBtn {\n  --border-radius: 0px;\n  margin-top: 0;\n  margin-bottom: 10px;\n  height: 52px;\n  --background:#b31117;\n  color: white;\n  font-size: 16px;\n  font-weight: 700;\n  text-transform: uppercase;\n  font-family: 'Montserrat', sans-serif;\n  --background-activated:var(--ion-color-shade,#b31117); }\n\n:host ion-button.SignUpBtn {\n  --border-radius: 0px;\n  margin-top: 0;\n  margin-bottom: 0;\n  height: 52px;\n  --background: white;\n  color: black;\n  font-size: 16px;\n  font-weight: 700;\n  text-transform: uppercase;\n  font-family: 'Montserrat', sans-serif;\n  --background-activated:var(--ion-color-shade,white); }\n\n.home-screen-content {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  height: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3VzZXItbG9naW4vdXNlci1sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSx3REFBYSxFQUFBOztBQUZyQjtFQUtRLG1CQUFtQjtFQUVuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBOztBQVQxQjtFQVdtQixtQkFBbUIsRUFBQTs7QUFYdEM7RUFhUSxlQUFlO0VBQ2YsU0FBUztFQUNULFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIscUNBQXFDLEVBQUE7O0FBcEI3QztFQXdCUSxvQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLG1CQUFrQjtFQUNsQixZQUFZO0VBQ1osb0JBQWE7RUFDYixZQUFZO0VBQ1osZUFBZTtFQUNmLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIscUNBQXFDO0VBQ3JDLHFEQUF1QixFQUFBOztBQWxDL0I7RUFxQ1Esb0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixnQkFBZTtFQUNmLFlBQVk7RUFDWixtQkFBYTtFQUNiLFlBQVk7RUFDWixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixxQ0FBcUM7RUFDckMsbURBQXVCLEVBQUE7O0FBSS9CO0VBQ0ksYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdXNlci1sb2dpbi91c2VyLWxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24tY29udGVudC5iYWNrZ3JvdW5kIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWdzL3dlbGNvbWUuanBnJykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG4gICAgfVxuICAgIC5sb2dvIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbiAgIFxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIC5oZWFkaW5nLXRpdGxle21hcmdpbi1ib3R0b206IDNyZW19XG4gICAgLmhlYWRpbmctdGl0bGUgaDF7XG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBsaW5lLWhlaWdodDogMzVweDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcbiAgICB9XG5cbiAgICBpb24tYnV0dG9uLmxvZ2luQnRuIHtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgICAgIG1hcmdpbi1ib3R0b206MTBweDtcbiAgICAgICAgaGVpZ2h0OiA1MnB4O1xuICAgICAgICAtLWJhY2tncm91bmQ6I2IzMTExNztcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6dmFyKC0taW9uLWNvbG9yLXNoYWRlLCNiMzExMTcpO1xuICAgIH1cbiAgICBpb24tYnV0dG9uLlNpZ25VcEJ0biB7XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOjA7XG4gICAgICAgIGhlaWdodDogNTJweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6dmFyKC0taW9uLWNvbG9yLXNoYWRlLHdoaXRlKTtcbiAgICB9XG59XG5cbi5ob21lLXNjcmVlbi1jb250ZW50IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/user-login/user-login.page.ts":
/*!***********************************************!*\
  !*** ./src/app/user-login/user-login.page.ts ***!
  \***********************************************/
/*! exports provided: UserLoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoginPage", function() { return UserLoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var UserLoginPage = /** @class */ (function () {
    function UserLoginPage(authService, menuCtrl) {
        this.authService = authService;
        this.menuCtrl = menuCtrl;
        // this.menuCtrl.enable(false);
    }
    UserLoginPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    UserLoginPage.prototype.ngOnInit = function () {
        // this.authState$ = this.authService.getAuthStateObserver().subscribe();
    };
    UserLoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-login',
            template: __webpack_require__(/*! ./user-login.page.html */ "./src/app/user-login/user-login.page.html"),
            styles: [__webpack_require__(/*! ./user-login.page.scss */ "./src/app/user-login/user-login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], UserLoginPage);
    return UserLoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=user-login-user-login-module.js.map