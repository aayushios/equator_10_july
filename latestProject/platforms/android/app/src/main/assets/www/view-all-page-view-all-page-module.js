(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-all-page-view-all-page-module"],{

/***/ "./src/app/view-all-page/view-all-page.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/view-all-page/view-all-page.module.ts ***!
  \*******************************************************/
/*! exports provided: ViewAllPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewAllPagePageModule", function() { return ViewAllPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _view_all_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view-all-page.page */ "./src/app/view-all-page/view-all-page.page.ts");







var routes = [
    {
        path: '',
        component: _view_all_page_page__WEBPACK_IMPORTED_MODULE_6__["ViewAllPagePage"]
    }
];
var ViewAllPagePageModule = /** @class */ (function () {
    function ViewAllPagePageModule() {
    }
    ViewAllPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_view_all_page_page__WEBPACK_IMPORTED_MODULE_6__["ViewAllPagePage"]]
        })
    ], ViewAllPagePageModule);
    return ViewAllPagePageModule;
}());



/***/ }),

/***/ "./src/app/view-all-page/view-all-page.page.html":
/*!*******************************************************!*\
  !*** ./src/app/view-all-page/view-all-page.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">{{title}}</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n  <!-- <div *ngIf=\"subCategoryArray && subCategoryArray.length \" class=\"container\">\n      <div class=\"category-scroll\" scrollX=\"true\">\n        <span class=\"category-span\"\n          *ngFor=\"let subCategory of subCategoryArray\" (click)=\"filterProduct(subCategory.slug)\">{{subCategory.name}}</span>\n      </div>\n    </div> -->\n  <ion-grid>\n       <ion-row>\n         <ion-col size=\"6\" *ngFor=\"let products of allProductsArray\">\n              <ion-card class=\"product-card\">\n                <div *ngIf=\"products.post_image_url === ''; else placeholder\">\n                  <ion-card-content class=\"product-card-media\">\n                    <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"../../assets/imgs/icon.png\">\n                  </ion-card-content>\n                </div>\n                <ng-template #placeholder>\n                  <ion-card-content class=\"product-card-media\" >\n                      <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"https://www.niletechinnovations.com/eq8tor{{products.post_image_url}}\">\n                  </ion-card-content>\n                </ng-template>\n              </ion-card>\n              <ion-card-content class=\"product-content\">\n                  <h2 class=\"product-title\">\n                      {{products.post_title}}\n                  </h2>\n                  <div *ngIf=\"products._rating === 0\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:0%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 0 && products._rating < 10\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:6%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 10\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:10%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 10 && products._rating < 20\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:16%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 20\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:20%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 20 && products._rating < 30\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:26%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 30\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:30%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 30 && products._rating < 40\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:36%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 40\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:40%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 40 && products._rating < 50\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:46%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 50\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:50%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 50 && products._rating < 60\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:56%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 60\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:60%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 60 && products._rating < 70\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:66%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 70\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:70%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 70 && products._rating < 80\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:76%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 80\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:80%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 80 && products._rating < 90\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:86%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 90\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:90%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating > 90 && products._rating < 100\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:96%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products._rating === 100\">\n                      <div class=\"star-rating\">\n                        <span style=\"width:100%\"></span>\n                      </div>\n                    </div>\n                    <div *ngIf=\"products.post_type === 'simple_product' \" class=\"product-price\">\n                        <span>{{products.post_price | currency:'USD':'symbol'}}</span>\n                      </div>\n                      <div *ngIf=\"products.post_type === 'configurable_product'\" class=\"product-price\">\n                        <div *ngFor=\"let variation of products.product_variations; let i = index\">\n                          <span *ngIf=\"i == 0\">{{variation._variation_post_price | currency:'USD':'symbol'}}</span>\n                        </div>\n                      </div>  \n              </ion-card-content>\n         </ion-col>\n       </ion-row>\n   </ion-grid>\n </ion-content>\n\n"

/***/ }),

/***/ "./src/app/view-all-page/view-all-page.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/view-all-page/view-all-page.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\nion-card.product-card {\n  -webkit-margin-start: 0;\n  margin-inline-start: 0px;\n  background: #fff;\n  border-radius: 3px;\n  --background: var(--ion-item-background, #fff);\n  box-shadow: none;\n  margin-bottom: 0;\n  min-height: 200px;\n  -webkit-margin-end: 10px;\n  margin-inline-end: 10px;\n  margin-bottom: 10px;\n  margin-top: 0px; }\n\n.product-card-media {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 200px;\n  overflow: hidden; }\n\n.product-card-media img {\n  width: 100% !important;\n  max-height: none !important;\n  max-width: none !important; }\n\n.product-title {\n  color: #656c72;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 16px;\n  -webkit-line-clamp: 3;\n  display: -webkit-box;\n  text-overflow: ellipsis;\n  max-height: 48px;\n  overflow: hidden; }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 18px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.product-content {\n  padding: 0 10px;\n  text-align: center; }\n\n.new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.back-arrow {\n  color: #fff; }\n\n.product-price span {\n  color: #656c72;\n  font-size: 16px;\n  font-weight: 600; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3ZpZXctYWxsLXBhZ2Uvdmlldy1hbGwtcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUI7RUFDbkI7QUFBYSxFQUFBOztBQUdqQjtFQUNJLHVCQUF1QjtFQUN2Qix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQiw4Q0FBYTtFQUNiLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHdCQUF3QjtFQUN4Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSwwQkFBMEI7RUFDMUIseUJBQXlCO0VBQ3pCLHdCQUF3QjtFQUN4Qix1QkFBdUI7RUFDdkIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksc0JBQXNCO0VBQ3RCLDJCQUEyQjtFQUMzQiwwQkFBMEIsRUFBQTs7QUFHOUI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQix1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVyxFQUFBOztBQUdmO0VBQ0ksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksY0FBYztFQUNkLG9DQUFvQztFQUNwQyxXQUFXO0VBQ1gsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLFdBQVc7RUFDWCxPQUFPO0VBQ1AsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsTUFBTSxFQUFBOztBQUdWO0VBQ0ksY0FBYztFQUNkLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsT0FBTztFQUNQLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsTUFBTSxFQUFBOztBQUdWO0VBQ0ksYUFBYSxFQUFBOztBQUdqQjtFQUNJLGVBQWU7RUFDZixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXctYWxsLXBhZ2Uvdmlldy1hbGwtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5ob21lLWNvbnRlbnQge1xuICAgIGJhY2tncm91bmQ6ICNmNGY4ZmM7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvciwgI2Y0ZjhmYylcbn1cblxuaW9uLWNhcmQucHJvZHVjdC1jYXJkIHtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLCAjZmZmKTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgbWluLWhlaWdodDogMjAwcHg7XG4gICAgLXdlYmtpdC1tYXJnaW4tZW5kOiAxMHB4O1xuICAgIG1hcmdpbi1pbmxpbmUtZW5kOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xufVxuXG4ucHJvZHVjdC1jYXJkLW1lZGlhIHtcbiAgICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xuICAgIC13ZWJraXQtcGFkZGluZy1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5wcm9kdWN0LWNhcmQtbWVkaWEgaW1nIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1heC1oZWlnaHQ6IG5vbmUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnByb2R1Y3QtdGl0bGUge1xuICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMztcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICBtYXgtaGVpZ2h0OiA0OHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5zdGFyLXJhdGluZyB7XG4gICAgZm9udC1mYW1pbHk6IFwiRm9udEF3ZXNvbWVcIjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiA3OHB4O1xufVxuXG4uc3Rhci1yYXRpbmcge1xuICAgIG1hcmdpbjogMTBweCBhdXRvO1xufVxuXG4uc3Rhci1yYXRpbmc6OmJlZm9yZSB7XG4gICAgY29sb3I6ICM2NTZjNzI7XG4gICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGVmdDogMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyBzcGFuIHtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBsZWZ0OiAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcGFkZGluZy10b3A6IDEuNWVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyBzcGFuOjpiZWZvcmUge1xuICAgIGNvbG9yOiAjZmFiOTAyO1xuICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxlZnQ6IDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xufVxuXG4uc3Rhci1yYXRpbmcgLnJhdGluZyB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLnByb2R1Y3QtY29udGVudCB7XG4gICAgcGFkZGluZzogMCAxMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG59XG5cbi5oZWFkZXItdGl0bGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlclxufVxuXG4uYmFjay1hcnJvdyB7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5wcm9kdWN0LXByaWNlIHNwYW4ge1xuICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/view-all-page/view-all-page.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/view-all-page/view-all-page.page.ts ***!
  \*****************************************************/
/*! exports provided: ViewAllPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewAllPagePage", function() { return ViewAllPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var ViewAllPagePage = /** @class */ (function () {
    function ViewAllPagePage(router, homepageService, navCtrl, route, loadingService) {
        var _this = this;
        this.router = router;
        this.homepageService = homepageService;
        this.navCtrl = navCtrl;
        this.route = route;
        this.loadingService = loadingService;
        this.productType = '';
        this.allProductsArray = [];
        this.title = '';
        this.route.queryParams.subscribe(function (params) {
            console.log(params.type);
            _this.productType = params.type;
            if (_this.productType == 'featured') {
                _this.title = 'All Featured Products';
            }
            else if (_this.productType == 'latest') {
                _this.title = 'All Latest Products';
            }
            _this.getAllFeaturedProducts(_this.productType);
        });
    }
    ViewAllPagePage.prototype.ngOnInit = function () {
    };
    ViewAllPagePage.prototype.getAllFeaturedProducts = function (productType) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getAllProducts(productType).subscribe(function (resp) {
            _this.allProductsArray = resp.data.all;
            console.log("view all", _this.allProductsArray);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.loadingService.dismiss();
        });
    };
    ViewAllPagePage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ViewAllPagePage.prototype.goToProductDetail = function (product) {
        this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
        console.log("response product", product);
    };
    ViewAllPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-view-all-page',
            template: __webpack_require__(/*! ./view-all-page.page.html */ "./src/app/view-all-page/view-all-page.page.html"),
            styles: [__webpack_require__(/*! ./view-all-page.page.scss */ "./src/app/view-all-page/view-all-page.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]])
    ], ViewAllPagePage);
    return ViewAllPagePage;
}());



/***/ })

}]);
//# sourceMappingURL=view-all-page-view-all-page-module.js.map