(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cart-cart-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/cart/cart.module.ts":
/*!*************************************!*\
  !*** ./src/app/cart/cart.module.ts ***!
  \*************************************/
/*! exports provided: CartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartPageModule", function() { return CartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cart.page */ "./src/app/cart/cart.page.ts");







var routes = [
    {
        path: '',
        component: _cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]
    }
];
var CartPageModule = /** @class */ (function () {
    function CartPageModule() {
    }
    CartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]]
        })
    ], CartPageModule);
    return CartPageModule;
}());



/***/ }),

/***/ "./src/app/cart/cart.page.html":
/*!*************************************!*\
  !*** ./src/app/cart/cart.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Cart</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n  <ion-label>{{outOfStockMessage}}</ion-label>\n  <ion-list class=\"cart-item-content\">\n    <ion-item class=\"cart-item-info\" *ngFor=\"let item of selectedItems\" lines=\"inset\">\n      <ion-row class=\"cart-item-grid\">\n        <ion-col size=\"3\">\n          <div class=\"cart-item-info-media\">\n            <img src=\"{{item.image_url}}\">\n          </div>\n          \n        </ion-col>\n        <ion-col size=\"8\">\n          <div class=\"cart-item-info-content\">\n            <h2>{{ item.slug }}</h2>\n            <h4>{{ item.quantity }}x</h4>\n            <div class=\"item-price\">\n              <!-- {{ item.post_price | currency:'USD':'symbol' }} -->\n              <span slot=\"end\" text-right>{{ (item.price * item.quantity) | currency:'USD':'symbol' }}</span>\n            </div>\n            <div>\n                <button class=\"btn-increment\" (click)=\"decrementQty(item.quantity,item.id,item.variation_id)\">\n                  <ion-icon class=\"ion-icon-increment\" name=\"remove-circle\"></ion-icon>\n                </button> <span class=\"quantity-span\"> {{item.quantity}} </span>\n                <button class=\"btn-increment\"  (click)=\"incrementQty(item.quantity,item.id,item.variation_id)\">\n                  <ion-icon class=\"ion-icon-increment\" name=\"add-circle\"></ion-icon>\n                </button>\n              </div>\n          </div>\n        </ion-col>\n        <ion-col size=\"1\">\n          <ion-button class=\"close-icon\" (click)=\"removeItem(item)\">\n            <ion-icon name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <div class=\"btn-content-info\">\n      <button routerLink=\"/home\" class=\"btn-add-more\">+ ADD MORE ITEMS</button>\n    </div>\n    <!-- <ion-item>\n      Total: <span slot=\"end\">{{ total | currency:'USD':'symbol' }}</span>\n    </ion-item> -->\n  </ion-list>\n\n  <div class=\"apply-coupon-section\">\n    <ion-row class=\"apply-coupon-content\">\n      <ion-col size=\"9\">\n        <div class=\"apply-coupon-box\">\n          <img src=\"../../assets/imgs/discount.svg\" height=\"40\">\n          <button class=\"apply-coupon-btn\" ion-button (click)=\"hideMe = !hideMe\">{{couponHeadingText}}</button>\n        </div>\n      </ion-col>\n      <ion-col size=\"3\">\n        <div class=\"offer-btn-box\">\n          <button class=\"offer-btn\">Offers</button>\n        </div>\n      </ion-col>\n      <div *ngIf=\"hideMe\" class=\"apply-coupon-form\">\n        <ion-item class=\"password-input\">\n          <ion-input class=\"input-form-control\" (keydown.space)=\"$event.preventDefault();\" [type]=\"text\"\n            placeholder=\"Apply Coupon\" [(ngModel)]=\"coupon\" [readonly]=\"status_loading\">\n          </ion-input>\n          <button class=\"showBtn Apply-btn\" item-right (click)='applyCoupon()'>{{couponBtnText}}</button>\n        </ion-item>\n      </div>\n    </ion-row>\n  </div>\n\n <div class=\"cart-total-section\">\n   <h2>Bill Details</h2>    \n    <ion-row>\n      <ion-col size=\"6\" >\n        <div class=\"cart-Subtotal-text\">Cart Sub Total:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"cart-Subtotal-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf = \"isMember === true\">\n        <ion-col size=\"6\" >\n          <div class=\"cart-discount-text\">Membership Discount:</div>\n        </ion-col>\n        <ion-col size=\"6\">\n          <div class=\"cart-discount-value\">{{ membershipDiscount | currency:'USD':'symbol' }}</div>\n        </ion-col>\n      </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-discount-text\">Offer & Discount:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"cart-discount-value\">{{- couponDiscount | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-tax-text\">Tax:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <div class=\"cart-tax-value\">{{ taxRate | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-shipping-text\">Shipping Cost:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <!-- <div class=\"cart-shipping-value\">{{ total | currency:'USD':'symbol' }}</div> -->\n          <div class=\"cart-shipping-value\">$ 0</div>\n        </ion-col>\n    </ion-row>\n    <ion-row class=\"grand-total\">\n      <ion-col size=\"6\">\n          <div class=\"cart-total-text\">Grand Total:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <div class=\"cart-total-value\">{{ totalAmount | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n\n<ion-footer class=\"product-single-footer\">\n  <ion-button class=\"checkout-btn\" (click)='goToCheckout()' [disabled]=\"checkoutDisable\">\n    CHECKOUT\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/cart/cart.page.scss":
/*!*************************************!*\
  !*** ./src/app/cart/cart.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n.back-arrow {\n  color: #fff; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\n.cart-item-content {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.cart-item-info {\n  border-bottom: 1px solid #eee;\n  --padding-start: 0px;\n  --inner-padding-end: 0px; }\n\n.cart-item-info-content h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.cart-item-info-content h4 {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  margin: 0;\n  padding: 0; }\n\n.item-price {\n  padding: 10px 0;\n  text-align: left; }\n\n.item-price span {\n  font-size: 16px;\n  font-weight: bold;\n  color: #213051; }\n\n.btn-add-more {\n  background: #b31117;\n  padding: 10px 16px;\n  color: #fff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 2px;\n  font-size: 13px; }\n\n.btn-content-info {\n  padding: 10px 0;\n  text-align: center; }\n\n.close-icon {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n.apply-coupon-section {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.apply-coupon-box {\n  display: flex;\n  align-items: center;\n  height: 100%; }\n\n.offer-btn-box {\n  text-align: right;\n  padding: 10px 0; }\n\n.offer-btn {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  outline: none; }\n\n.checkout-btn {\n  color: #fff;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated: #b31117;\n  width: 100%; }\n\nion-footer.product-single-footer {\n  background: #fff;\n  padding: 10px 20px;\n  box-shadow: 0 -4px 12px 0 rgba(158, 169, 181, 0.55); }\n\n.apply-coupon-btn {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #213051;\n  padding: 0 0 0 10px;\n  font-weight: 600;\n  font-size: 14px;\n  outline: none; }\n\n.apply-coupon-form {\n  width: 100%; }\n\n.password-input {\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n.Apply-btn {\n  background: #b31117;\n  padding: 10px 16px;\n  color: #fff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 2px;\n  font-size: 13px;\n  outline: none; }\n\n.btn-increment {\n  background: #b31117;\n  border-radius: 5px;\n  color: white;\n  font-size: 18px;\n  padding: 8px 10px;\n  display: inline-block; }\n\n.quantity-span {\n  display: inline-block;\n  width: 40px;\n  text-align: center;\n  font-size: 20px;\n  font-weight: 800; }\n\nion-icon.ion-icon-increment {\n  font-size: 14px;\n  color: white; }\n\n.input-form-control {\n  font-size: 14px;\n  font-weight: 500;\n  color: #213051; }\n\n.cart-total-section {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 0px; }\n\n.cart-total-section h2 {\n  color: #b31117;\n  font-size: 14px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.cart-Subtotal-text {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500; }\n\n.cart-Subtotal-value {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-discount-text {\n  color: #ff9800;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-discount-value {\n  color: #ff9800;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-tax-text {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-tax-value {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-shipping-text {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-shipping-value {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.grand-total {\n  border-top: 1px solid #eee; }\n\n.cart-total-text {\n  color: #213051;\n  font-size: 16px;\n  font-weight: bold; }\n\n.cart-total-value {\n  color: #213051;\n  font-size: 16px;\n  font-weight: bold;\n  text-align: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2NhcnQvY2FydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxxQkFBYSxFQUFBOztBQUZyQjtFQU1RLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUlKO0VBQ0ksV0FBVyxFQUFBOztBQUlmO0VBQ0ksbUJBQW1CO0VBQ25CO0FBQWEsRUFBQTs7QUFJakI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLDZCQUE2QjtFQUM3QixvQkFBZ0I7RUFDaEIsd0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsVUFBVSxFQUFBOztBQUdkO0VBQ0ksZUFBZTtFQUNmLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQix5QkFBYTtFQUNiLGlDQUFxQjtFQUNyQixjQUFjO0VBQ2QsVUFBVTtFQUNWLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQixrQkFBZ0I7RUFDaEIsZ0JBQWMsRUFBQTs7QUFJbEI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUloQjtFQUNJLGlCQUFpQjtFQUNqQixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZ0JBQWdCO0VBQ2hCLHlCQUFhO0VBQ2IsaUNBQXFCO0VBQ3JCLGNBQWM7RUFDZCxVQUFVO0VBQ1YsYUFBYSxFQUFBOztBQUlqQjtFQUNJLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixvQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFNBQVM7RUFDVCxZQUFZO0VBQ1osbUJBQWM7RUFDZCxzQkFBaUI7RUFDakIscUJBQWdCO0VBQ2hCLG1CQUFjO0VBQ2QscUJBQWE7RUFDYiwrQkFBdUI7RUFDdkIsV0FBVyxFQUFBOztBQUdmO0VBQ0ksZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUVsQixtREFBbUQsRUFBQTs7QUFHdkQ7RUFDSSxnQkFBZ0I7RUFDaEIseUJBQWE7RUFDYixpQ0FBcUI7RUFDckIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGFBQWEsRUFBQTs7QUFJakI7RUFDSSxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGtCQUFnQjtFQUNoQixnQkFBYyxFQUFBOztBQUdsQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxxQkFBcUI7RUFDckIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQ0osRUFBQTs7QUFFQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQ0osRUFBQTs7QUFFQTtFQUNJLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsaUJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NhcnQvY2FydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIH1cblxuICAgIC5oZWFkZXItdGl0bGUge1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyXG4gICAgfVxuXG59XG5cbi5iYWNrLWFycm93IHtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuXG5pb24tY29udGVudC5ob21lLWNvbnRlbnQge1xuICAgIGJhY2tncm91bmQ6ICNmNGY4ZmM7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvciwgI2Y0ZjhmYylcbn1cblxuXG4uY2FydC1pdGVtLWNvbnRlbnQge1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uY2FydC1pdGVtLWluZm8ge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbn1cblxuXG4uY2FydC1pdGVtLWluZm8tY29udGVudCBoMiB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbn1cblxuLmNhcnQtaXRlbS1pbmZvLWNvbnRlbnQgaDQge1xuICAgIGNvbG9yOiAjYTBhN2I1O1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG4uaXRlbS1wcmljZSB7XG4gICAgcGFkZGluZzogMTBweCAwO1xuICAgIHRleHQtYWxpZ246IGxlZnRcbn1cblxuLml0ZW0tcHJpY2Ugc3BhbiB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiAjMjEzMDUxO1xufVxuXG4uYnRuLWFkZC1tb3JlIHtcbiAgICBiYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIHBhZGRpbmc6IDEwcHggMTZweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5idG4tY29udGVudC1pbmZvIHtcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2xvc2UtaWNvbiB7XG4gICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiB0cmFuc3BhcmVudDtcbiAgICBjb2xvcjogI2IzMTExNztcbiAgICBwYWRkaW5nOiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDA7XG4gICAgLXdlYmtpdC1tYXJnaW4tZW5kOiAwO1xuICAgIG1hcmdpbi1pbmxpbmUtZW5kOiAwO1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuXG59XG5cbi5hcHBseS1jb3Vwb24tc2VjdGlvbiB7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5hcHBseS1jb3Vwb24tYm94IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuXG5cbi5vZmZlci1idG4tYm94IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG59XG5cbi5vZmZlci1idG4ge1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogdHJhbnNwYXJlbnQ7XG4gICAgY29sb3I6ICNiMzExMTc7XG4gICAgcGFkZGluZzogMDtcbiAgICBvdXRsaW5lOiBub25lO1xuXG59XG5cbi5jaGVja291dC1idG4ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC0tcGFkZGluZy10b3A6IDE1cHg7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDMwcHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMzBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI2IzMTExNztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuaW9uLWZvb3Rlci5wcm9kdWN0LXNpbmdsZS1mb290ZXIge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAtNHB4IDEycHggMCByZ2JhKDE1OCwgMTY5LCAxODEsIDAuNTUpO1xuICAgIGJveC1zaGFkb3c6IDAgLTRweCAxMnB4IDAgcmdiYSgxNTgsIDE2OSwgMTgxLCAwLjU1KTtcbn1cblxuLmFwcGx5LWNvdXBvbi1idG4ge1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogdHJhbnNwYXJlbnQ7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgcGFkZGluZzogMCAwIDAgMTBweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBvdXRsaW5lOiBub25lO1xuXG59XG5cbi5hcHBseS1jb3Vwb24tZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5wYXNzd29yZC1pbnB1dCB7XG4gICAgcGFkZGluZzogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbn1cblxuLkFwcGx5LWJ0biB7XG4gICAgYmFja2dyb3VuZDogI2IzMTExNztcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbi5idG4taW5jcmVtZW50e1xuICAgIGJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZzogOHB4IDEwcHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4ucXVhbnRpdHktc3BhbiB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuaW9uLWljb24uaW9uLWljb24taW5jcmVtZW50IHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uaW5wdXQtZm9ybS1jb250cm9sIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBjb2xvcjogIzIxMzA1MTtcbn1cblxuLmNhcnQtdG90YWwtc2VjdGlvbiB7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbn1cblxuLmNhcnQtdG90YWwtc2VjdGlvbiBoMiB7XG4gICAgY29sb3I6ICNiMzExMTc7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbn1cblxuLmNhcnQtU3VidG90YWwtdGV4dCB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5jYXJ0LVN1YnRvdGFsLXZhbHVlIHtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmNhcnQtZGlzY291bnQtdGV4dCB7XG4gICAgY29sb3I6ICNmZjk4MDA7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5jYXJ0LWRpc2NvdW50LXZhbHVlIHtcbiAgICBjb2xvcjogI2ZmOTgwMDtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmNhcnQtdGF4LXRleHQge1xuICAgIGNvbG9yOiAjYTBhN2I1O1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uY2FydC10YXgtdmFsdWUge1xuICAgIGNvbG9yOiAjYTBhN2I1O1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0XG59XG5cbi5jYXJ0LXNoaXBwaW5nLXRleHQge1xuICAgIGNvbG9yOiAjYTBhN2I1O1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uY2FydC1zaGlwcGluZy12YWx1ZSB7XG4gICAgY29sb3I6ICNhMGE3YjU7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgdGV4dC1hbGlnbjogcmlnaHRcbn1cblxuLmdyYW5kLXRvdGFsIHtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcbn1cblxuLmNhcnQtdG90YWwtdGV4dCB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2FydC10b3RhbC12YWx1ZSB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0XG59Il19 */"

/***/ }),

/***/ "./src/app/cart/cart.page.ts":
/*!***********************************!*\
  !*** ./src/app/cart/cart.page.ts ***!
  \***********************************/
/*! exports provided: CartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartPage", function() { return CartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");








var CartPage = /** @class */ (function () {
    function CartPage(cartService, alert, alertService, alertController, loadingService, homepageService, navCtrl, router) {
        this.cartService = cartService;
        this.alert = alert;
        this.alertService = alertService;
        this.alertController = alertController;
        this.loadingService = loadingService;
        this.homepageService = homepageService;
        this.navCtrl = navCtrl;
        this.router = router;
        this.selectedItems = [];
        this.total = 0;
        this.totalAmount = "";
        this.coupon = '';
        this.couponBtnText = "";
        this.couponHeadingText = "";
        this.couponDiscount = 0;
        this.status_loading = false;
        this.imgUrl = "https://www.niletechinnovations.com/eq8tor";
        this.stockArray = [];
        this.taxRate = "";
        this.isMember = false;
        this.membershipDiscount = "";
        this.outOfStockArray = [];
        this.outOfStockMessage = '';
        this.checkoutDisable = false;
    }
    CartPage.prototype.hide = function () {
        this.hideMe = true;
    };
    CartPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    CartPage.prototype.getCartItems = function () {
        var _this = this;
        this.loadingService.present();
        var items = [];
        this.stockArray = [];
        this.outOfStockArray = [];
        this.outOfStockMessage = "";
        this.checkoutDisable = false;
        this.totalProduct;
        this.homepageService.getCartProducts().subscribe(function (response) {
            console.log(response);
            items = response.data.products;
            _this.totalAmount = response.data.total;
            _this.couponDiscount = response.data.coupon_discount;
            _this.coupon = response.data.coupon_code;
            if (_this.coupon == "" || _this.coupon == null) {
                _this.status_loading = false;
                _this.couponBtnText = "Apply";
                _this.couponHeadingText = "Apply Coupon";
                console.log(_this.coupon, _this.status_loading, _this.couponBtnText, _this.couponHeadingText);
            }
            else {
                _this.status_loading = true;
                _this.couponBtnText = "Remove";
                _this.couponHeadingText = "Coupon Applied";
            }
            if (response.data.is_member == true) {
                _this.isMember = true;
                _this.membershipDiscount = response.data.member_discount;
            }
            _this.totalProduct = response.data.total_product;
            if (_this.totalProduct == 0) {
                _this.checkoutDisable = true;
            }
            _this.taxRate = response.data.taxRate;
            for (var i = 0; i < items.length; i++) {
                if (items[i].out_of_stock == false) {
                    _this.stockArray.push(items[i]);
                }
                else {
                    _this.outOfStockArray.push(items[i]);
                }
            }
            if (_this.outOfStockArray.length > 0) {
                _this.outOfStockMessage = "There are some products which you added to cart are out of stock,so removed.";
                for (var i = 0; i < _this.outOfStockArray.length; i++) {
                    console.log("this.outOfStockArray[i]", _this.outOfStockArray[i]);
                    _this.removeItem(_this.outOfStockArray[i]);
                }
                _this.alert.myAlertMethod("Out of Stock", "There are " + _this.outOfStockArray.length + " product out of stock so removed.", function (data) {
                    console.log("Out of stock removed.");
                });
            }
            items = _this.stockArray;
            console.log("this.outOfstockArray", _this.outOfStockArray);
            var selected = {};
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var obj = items_1[_i];
                if (selected[obj.id]) {
                    selected[obj.id].count++;
                }
                else {
                    selected[obj.id] = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, obj, { count: 1 });
                }
            }
            _this.selectedItems = Object.keys(selected).map(function (key) { return selected[key]; });
            _this.total = _this.selectedItems.reduce(function (a, b) { return a + (b.quantity * b.price); }, 0);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log("error get cart content", err);
            _this.alert.myAlertMethod("Error", err.error.message, function (data) {
                console.log("error");
            });
            _this.loadingService.dismiss();
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    CartPage.prototype.ngOnInit = function () {
        this.getCartItems();
        this.status_loading = false;
    };
    CartPage.prototype.removeItem = function (post) {
        var _this = this;
        this.loadingService.present();
        if (post.variation_id == undefined || null) {
            post.variation_id = "";
        }
        this.homepageService.removeFromCart(post.id, post.variation_id).subscribe(function (resp) {
            console.log(resp);
            var index = _this.selectedItems.indexOf(post);
            if (index > -1) {
                _this.selectedItems.splice(index, 1);
            }
            _this.total = _this.selectedItems.reduce(function (a, b) { return a + (b.count * b.price); }, 0);
            _this.getCartItems();
            if (_this.selectedItems.length == 0) {
                _this.checkoutDisable = true;
            }
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.loadingService.dismiss();
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    CartPage.prototype.incrementQty = function (quantity, productID, variationId) {
        var _this = this;
        this.loadingService.present();
        quantity += 1;
        if (variationId == null || undefined) {
            console.log("variationID is undefined");
            variationId = "";
        }
        this.homepageService.updateCartProducts(productID, quantity, variationId).subscribe(function (response) {
            console.log(response);
            _this.getCartItems();
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.loadingService.dismiss();
            _this.alertService.myAlertMethod("Error", err.error.message, function (data) {
                console.log("error");
            });
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    CartPage.prototype.decrementQty = function (quantity, productID, variationId) {
        var _this = this;
        if (variationId == null || undefined) {
            console.log("variationID is undefined");
            variationId = "";
        }
        if (quantity - 1 < 1) {
            quantity = 1;
            console.log('1->' + quantity);
        }
        else {
            quantity -= 1;
            this.homepageService.updateCartProducts(productID, quantity, variationId).subscribe(function (response) {
                console.log(response);
                _this.getCartItems();
                _this.loadingService.dismiss();
            }, function (err) {
                console.log(err);
                _this.loadingService.dismiss();
                _this.alertService.myAlertMethod("Error", err.error.message, function (data) {
                    console.log("error");
                });
            });
            console.log('2->' + quantity);
        }
    };
    CartPage.prototype.goToCheckout = function () {
        // this.router.navigate(['stripe-checkout']);
        this.presentAlertRadio();
    };
    CartPage.prototype.presentAlertRadio = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Choose Payment Method',
                            inputs: [
                                {
                                    name: 'Stripe',
                                    type: 'radio',
                                    label: 'Credit or Debit Card',
                                    value: 'Stripe'
                                },
                                {
                                    name: 'Cash on Delivery',
                                    type: 'radio',
                                    label: 'Cash on Delivery',
                                    value: 'COD'
                                },
                                {
                                    name: 'Paypal',
                                    type: 'radio',
                                    label: 'Paypal',
                                    value: 'Paypal'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Ok',
                                    handler: function (data) {
                                        console.log(data);
                                        if (data == "Paypal") {
                                            console.log("Paypal");
                                            _this.router.navigate(['stripe-checkout'], { queryParams: { 'total_amount': _this.totalAmount, 'total_tax': _this.taxRate, 'membership_discount': _this.membershipDiscount, 'is_member': _this.isMember }, });
                                        }
                                        else if (data == "COD") {
                                            console.log("COD");
                                            _this.router.navigate(['address-checkout'], { queryParams: { 'total_amount': _this.totalAmount, 'total_tax': _this.taxRate, 'membership_discount': _this.membershipDiscount, 'is_member': _this.isMember }, });
                                        }
                                        else if (data == "Stripe") {
                                            console.log("Stripe");
                                            _this.router.navigate(['stripe-checkout'], { queryParams: { 'total_amount': _this.totalAmount, 'total_tax': _this.taxRate, 'membership_discount': _this.membershipDiscount, 'is_member': _this.isMember }, });
                                        }
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CartPage.prototype.applyCoupon = function () {
        var _this = this;
        if (this.coupon == '' || this.coupon == null) {
            this.alertService.myAlertMethod("Invalid Coupon", "Please enter coupon code.", function (data) {
                console.log("error");
            });
        }
        else {
            if (this.couponBtnText == "Remove") {
                this.loadingService.present();
                this.homepageService.removeCoupon().subscribe(function (resp) {
                    console.log(resp);
                    if (resp.success == true) {
                        _this.status_loading = false;
                        _this.couponBtnText = "Apply";
                        _this.couponHeadingText = "Apply Coupon";
                        _this.coupon = '';
                        _this.getCartItems();
                    }
                    else {
                        _this.alertService.myAlertMethod("Error", "Please try again later.", function (data) {
                            console.log("error");
                        });
                    }
                    _this.loadingService.dismiss();
                }, function (err) {
                    console.log(err);
                    _this.alertService.myAlertMethod("Error", err.error.message, function (data) {
                        console.log("error");
                    });
                    _this.loadingService.dismiss();
                }, function () {
                });
            }
            else if (this.couponBtnText == "Apply") {
                this.loadingService.present();
                this.homepageService.applyCoupon(this.coupon).subscribe(function (resp) {
                    console.log("error", resp.error);
                    _this.alertService.myAlertMethod("Success", "Coupon Applied Successfully.", function (data) {
                        console.log("Coupon success.");
                    });
                    _this.couponBtnText = "Remove";
                    _this.couponHeadingText = "Coupon Applied";
                    _this.getCartItems();
                    _this.status_loading = true;
                }, function (err) {
                    console.log(err);
                    _this.alertService.myAlertMethod("Error", err.error.message, function (data) {
                        console.log("error");
                    });
                    _this.loadingService.dismiss();
                }, function () {
                    _this.loadingService.dismiss();
                });
            }
        }
    };
    CartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cart',
            template: __webpack_require__(/*! ./cart.page.html */ "./src/app/cart/cart.page.html"),
            styles: [__webpack_require__(/*! ./cart.page.scss */ "./src/app/cart/cart.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_7__["AlerServiceService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_7__["AlerServiceService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], CartPage);
    return CartPage;
}());



/***/ })

}]);
//# sourceMappingURL=cart-cart-module.js.map