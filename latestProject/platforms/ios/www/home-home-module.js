(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








// import { IonicRatingModule } from 'ionic4-rating';
// import { BarRatingModule } from 'ngx-bar-rating'
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\" (click)=\"RefreshPage()\">Home</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openCart()\">\n        <ion-badge class=\"badge-color\">{{ totalCount }}</ion-badge>\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-searchbar class=\"class-search\" (click)=\"openSearchPage()\" placeholder=\"Filter Schedules\"></ion-searchbar>\n  </ion-toolbar>\n  <div class=\"container\">\n    <div *ngIf=\"categories.length > 0\" class=\"category-scroll\" scrollX=\"true\">\n      <span class=\"category-span\" *ngFor=\"let category of categories\"\n        (click)=\"categoryClick(category.slug,category.name)\">{{category.name}}</span>\n    </div>\n  </div>\n</ion-header>\n\n<ion-content class=\"home-content\" padding>\n  <div>\n    <ion-slides *ngIf=\"bannerList && bannerList.length\" autoplay=\"5000\" loop=\"true\" speed=\"500\" class=\"slides\"\n      pager=\"true\">\n      <ion-slide *ngFor=\"let cat of bannerList\">\n        <div class=\"slide-content\" *ngIf=\"cat !== ''; else placeholder\">\n          <img class=\"image-wrap-slider\" src=\"https://www.niletechinnovations.com/eq8tor{{cat.img_url}}\">\n        </div>\n        <ng-template #placeholder><img src=\"../../assets/imgs/logo.png\"></ng-template>\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <ion-row no-padding>\n    <ion-col class=\"heading-title\">\n      Latest Items\n    </ion-col>\n    <ion-button class=\"btn-view\" float-right (click)=\"ViewAllLatest()\">View All</ion-button>\n  </ion-row>\n\n  <ion-slides [options]=\"sliderConfig\" class=\"Products-slider\">\n    <ion-slide *ngFor=\"let cat of latestProducts; let i = index\">\n      <div class=\"slide-content\">\n        <ion-card class=\"product-card\">\n          <div *ngIf=\"cat.post_image_url === ''\">\n            <ion-card-content class=\"product-card-media\">\n              <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\" src=\"../../assets/imgs/icon.png\">\n            </ion-card-content>\n          </div>\n          <div *ngIf=\"!cat.post_image_url == ''\">\n            <ion-card-content class=\"product-card-media\">\n              <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\"\n                src=\"https://www.niletechinnovations.com/eq8tor{{cat.post_image_url}}\">\n            </ion-card-content>\n          </div>\n          <div>\n            <div class=\"add-Wishlist\" *ngIf=\"!wishListAdded || cat._wishlist == false\" (click)=\"like(cat.id,i)\">\n              <img src=\"../../assets/imgs/Wishlist.svg\">\n            </div>\n          </div>\n          <div class=\"add-Wishlist\" *ngIf=\"wishListAdded || cat._wishlist == true\">\n            <img src=\"../../assets/imgs/Wishlisted.svg\">\n          </div>\n        </ion-card>\n        <ion-card-content class=\"product-content\">\n          <h2 class=\"product-title\">{{cat.post_title}}</h2>\n            \n            <div *ngIf=\"cat._rating === 0\">\n              <div class=\"star-rating\">\n                <span style=\"width:0%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 0 && cat._rating < 10\">\n              <div class=\"star-rating\">\n                <span style=\"width:6%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 10\">\n              <div class=\"star-rating\">\n                <span style=\"width:10%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 10 && cat._rating < 20\">\n              <div class=\"star-rating\">\n                <span style=\"width:16%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 20\">\n              <div class=\"star-rating\">\n                <span style=\"width:20%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 20 && cat._rating < 30\">\n              <div class=\"star-rating\">\n                <span style=\"width:26%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 30\">\n              <div class=\"star-rating\">\n                <span style=\"width:30%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 30 && cat._rating < 40\">\n              <div class=\"star-rating\">\n                <span style=\"width:36%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 40\">\n              <div class=\"star-rating\">\n                <span style=\"width:40%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 40 && cat._rating < 50\">\n              <div class=\"star-rating\">\n                <span style=\"width:46%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 50\">\n              <div class=\"star-rating\">\n                <span style=\"width:50%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 50 && cat._rating < 60\">\n              <div class=\"star-rating\">\n                <span style=\"width:56%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 60\">\n              <div class=\"star-rating\">\n                <span style=\"width:60%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 60 && cat._rating < 70\">\n              <div class=\"star-rating\">\n                <span style=\"width:66%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 70\">\n              <div class=\"star-rating\">\n                <span style=\"width:70%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 70 && cat._rating < 80\">\n              <div class=\"star-rating\">\n                <span style=\"width:76%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 80\">\n              <div class=\"star-rating\">\n                <span style=\"width:80%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 80 && cat._rating < 90\">\n              <div class=\"star-rating\">\n                <span style=\"width:86%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 90\">\n              <div class=\"star-rating\">\n                <span style=\"width:90%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating > 90 && cat._rating < 100\">\n              <div class=\"star-rating\">\n                <span style=\"width:96%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat._rating === 100\">\n              <div class=\"star-rating\">\n                <span style=\"width:100%\"></span>\n              </div>\n            </div>\n            <div *ngIf=\"cat.post_type === 'simple_product' \" class=\"product-price\">\n                <span>{{cat.post_price | currency:'USD':'symbol'}}</span>\n              </div>\n              <div *ngIf=\"cat.post_type === 'configurable_product'\" class=\"product-price\">\n                <div *ngFor=\"let variation of cat.product_variations; let i = index\">\n                  <span *ngIf=\"i == 0\">{{variation._variation_post_price | currency:'USD':'symbol'}}</span>\n                </div>\n              </div>\n        </ion-card-content>\n      </div>\n    </ion-slide>\n  </ion-slides>\n\n  <ion-row no-padding>\n    <ion-col class=\"heading-title\">\n      Featured Items\n    </ion-col>\n    <ion-button class=\"btn-view\" (click)=\"ViewAllFeatured()\">View All</ion-button>\n  </ion-row>\n\n\n  <ion-slides [options]=\"sliderConfig\" class=\"Products-slider\">\n    <ion-slide *ngFor=\"let cat of featuredItems; let i = index\">\n      <div class=\"slide-content\">\n        <ion-card class=\"product-card\">\n          <div *ngIf=\"cat.post_image_url === ''\">\n            <ion-card-content class=\"product-card-media\">\n              <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\" src=\"../../assets/imgs/icon.png\">\n            </ion-card-content>\n          </div>\n          <div *ngIf=\"!cat.post_image_url == ''\">\n            <ion-card-content class=\"product-card-media\">\n              <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\" src=\"{{apiUrl}}{{cat.post_image_url}}\">\n            </ion-card-content>\n          </div>\n          <div>\n            <div class=\"add-Wishlist\" *ngIf=\"!wishListAdded || cat._wishlist == false\" (click)=\"likeFeatured(cat.id,i)\">\n              <img src=\"../../assets/imgs/Wishlist.svg\">\n            </div>\n          </div>\n          <div class=\"add-Wishlist\" *ngIf=\"wishListAdded || cat._wishlist == true\">\n            <img src=\"../../assets/imgs/Wishlisted.svg\">\n          </div>\n        </ion-card>\n        <ion-card-content class=\"product-content\">\n          <ion-card-title class=\"product-title\">\n            {{cat.post_title}}\n          </ion-card-title>\n          <div *ngIf=\"cat._rating === 0\">\n            <div class=\"star-rating\">\n              <span style=\"width:0%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 0 && cat._rating < 10\">\n            <div class=\"star-rating\">\n              <span style=\"width:6%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 10\">\n            <div class=\"star-rating\">\n              <span style=\"width:10%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 10 && cat._rating < 20\">\n            <div class=\"star-rating\">\n              <span style=\"width:16%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 20\">\n            <div class=\"star-rating\">\n              <span style=\"width:20%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 20 && cat._rating < 30\">\n            <div class=\"star-rating\">\n              <span style=\"width:26%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 30\">\n            <div class=\"star-rating\">\n              <span style=\"width:30%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 30 && cat._rating < 40\">\n            <div class=\"star-rating\">\n              <span style=\"width:36%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 40\">\n            <div class=\"star-rating\">\n              <span style=\"width:40%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 40 && cat._rating < 50\">\n            <div class=\"star-rating\">\n              <span style=\"width:46%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 50\">\n            <div class=\"star-rating\">\n              <span style=\"width:50%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 50 && cat._rating < 60\">\n            <div class=\"star-rating\">\n              <span style=\"width:56%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 60\">\n            <div class=\"star-rating\">\n              <span style=\"width:60%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 60 && cat._rating < 70\">\n            <div class=\"star-rating\">\n              <span style=\"width:66%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 70\">\n            <div class=\"star-rating\">\n              <span style=\"width:70%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 70 && cat._rating < 80\">\n            <div class=\"star-rating\">\n              <span style=\"width:76%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 80\">\n            <div class=\"star-rating\">\n              <span style=\"width:80%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 80 && cat._rating < 90\">\n            <div class=\"star-rating\">\n              <span style=\"width:86%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 90\">\n            <div class=\"star-rating\">\n              <span style=\"width:90%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating > 90 && cat._rating < 100\">\n            <div class=\"star-rating\">\n              <span style=\"width:96%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat._rating === 100\">\n            <div class=\"star-rating\">\n              <span style=\"width:100%\"></span>\n            </div>\n          </div>\n          <div *ngIf=\"cat.post_type === 'simple_product' \" class=\"product-price\">\n            <span>{{cat.post_price | currency:'USD':'symbol'}}</span>\n          </div>\n          <div *ngIf=\"cat.post_type === 'configurable_product'\" class=\"product-price\">\n            <div *ngFor=\"let variation of cat.product_variations; let i = index\">\n              <span *ngIf=\"i == 0\">{{variation._variation_post_price | currency:'USD':'symbol'}}</span>\n            </div>\n          </div>\n        </ion-card-content>\n      </div>\n    </ion-slide>\n  </ion-slides>\n\n\n  <!-- <div *ngIf=\"users && users.length > 0\">\n          <ion-list *ngIf=\"users.length === 0\">\n              <ion-item *ngFor=\"let user of [1,1,1,1,1,1,1,1,1,1,1,1,1]\">\n                <ion-label>\n                  <ion-skeleton-text width=\"40%\" animated></ion-skeleton-text>\n                  <ion-skeleton-text width=\"80%\" animated></ion-skeleton-text>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n            <ion-list *ngIf=\"textToSearch.length > 0\">\n              <ion-item *ngFor=\"let user of users | filter:textToSearch\">\n                <ion-label>\n                  <h3>{{user.name}}</h3>\n                  <h5>{{user.email}}</h5>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n      </div> -->\n</ion-content>"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n:host .class-search {\n  --background: white; }\n\n:host .searchbar-input {\n  --border-radius: 2px !important; }\n\n:host .badge-color {\n  --background: #b31117;\n  color: white; }\n\n:host .swiper-pagination-bullet-active {\n  background: #b31117; }\n\n:host .btn-view {\n  color: #fff;\n  font-weight: bolder;\n  font-size: 10px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 8px;\n  --padding-bottom: 8px;\n  --padding-start: 10px;\n  --padding-end: 10px;\n  --background: #003a54;\n  --background-activated: #003a54;\n  max-height: -webkit-max-content;\n  max-height: -moz-max-content;\n  max-height: max-content; }\n\n:host .swiper-pagination-bullet {\n  background: #003a54; }\n\n:host .filters ion-col {\n  text-align: center;\n  font-size: 20px;\n  line-height: 20px; }\n\n:host .filters ion-col ion-icon {\n    color: #ccc; }\n\n:host .filters ion-col.col-with-arrow {\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n:host .filters p {\n  color: black;\n  margin: 0;\n  font-size: 15px;\n  line-height: 15px;\n  --background: red; }\n\n:host .filters .selected {\n  font-weight: 10px; }\n\n:host .container ::-webkit-scrollbar {\n  display: none; }\n\n:host .container .category-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n:host .heart-icon {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n:host .category-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n:host ion-badge {\n  color: #fff;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n  border-radius: 100%; }\n\n:host .category-block {\n  margin-bottom: 4px; }\n\n:host .category-banner {\n  border-left: 8px solid var(--ion-color-secondary);\n  background: var(--ion-color-light);\n  height: 40px;\n  padding: 10px;\n  font-weight: 500; }\n\n:host .card-title {\n  font-size: 12px;\n  font-weight: 300; }\n\n:host ion-slides {\n  color: #fff; }\n\n:host ion-slides ion-slide {\n    align-items: start;\n    text-align: center; }\n\n:host ion-slides ion-slide .slide-content .text p {\n      margin-top: 0;\n      margin-bottom: 10px; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\n.heading-title {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  padding: 5px 20px 5px 0;\n  margin-bottom: 0;\n  display: inline-block; }\n\nion-card.product-card {\n  -webkit-margin-start: 0;\n  margin-inline-start: 0px;\n  background: #fff;\n  border-radius: 3px;\n  --background: var(--ion-item-background, #fff);\n  box-shadow: none;\n  margin-bottom: 0;\n  min-height: 200px;\n  -webkit-margin-end: 10px;\n  margin-inline-end: 10px;\n  margin-bottom: 10px; }\n\n.product-card-media {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 200px;\n  overflow: hidden; }\n\n.product-card-media img {\n  width: 100% !important;\n  max-height: none !important;\n  max-width: none !important; }\n\n.product-title {\n  color: #656c72;\n  font-size: 12px;\n  font-weight: 500;\n  line-height: 16px;\n  -webkit-line-clamp: 3;\n  display: -webkit-box;\n  text-overflow: ellipsis;\n  max-height: 48px;\n  overflow: hidden; }\n\n.product-content {\n  padding: 0 10px; }\n\n.Products-slider {\n  height: auto;\n  /* slider height you want */ }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 16px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 14px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 14px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.product-price span {\n  color: #656c72;\n  font-size: 14px;\n  font-weight: 500; }\n\n.add-Wishlist {\n  position: absolute;\n  top: 0;\n  right: 2px; }\n\n.add-Wishlist img {\n  height: 16px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHUSxxQkFBYSxFQUFBOztBQUhyQjtFQVFRLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQVZKO0VBYVEsbUJBQWEsRUFBQTs7QUFickI7RUFpQlEsK0JBQWdCLEVBQUE7O0FBakJ4QjtFQXFCUSxxQkFBYTtFQUNiLFlBQVksRUFBQTs7QUF0QnBCO0VBMEJRLG1CQUFtQixFQUFBOztBQTFCM0I7RUE4QlEsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixTQUFTO0VBQ1QsWUFBWTtFQUNaLGtCQUFjO0VBQ2QscUJBQWlCO0VBQ2pCLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLHFCQUFhO0VBQ2IsK0JBQXVCO0VBQ3ZCLCtCQUF1QjtFQUF2Qiw0QkFBdUI7RUFBdkIsdUJBQXVCLEVBQUE7O0FBM0MvQjtFQStDUSxtQkFBbUIsRUFBQTs7QUEvQzNCO0VBb0RZLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBdEQ3QjtJQXlEZ0IsV0FBVyxFQUFBOztBQXpEM0I7SUE2RGdCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CLEVBQUE7O0FBL0RuQztFQXFFWSxZQUFZO0VBQ1osU0FBUztFQUNULGVBQWU7RUFDZixpQkFBaUI7RUFDakIsaUJBQWEsRUFBQTs7QUF6RXpCO0VBNkVZLGlCQUFpQixFQUFBOztBQTdFN0I7RUFtRlksYUFBYSxFQUFBOztBQW5GekI7RUF1RlksY0FBYztFQUNkLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsNEJBQTRCLEVBQUE7O0FBMUZ4QztFQStGUSxnQkFBZ0I7RUFDaEIseUJBQWE7RUFDYixpQ0FBcUI7RUFDckIsY0FBYztFQUNkLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixvQkFBb0I7RUFDcEIsa0JBQWdCO0VBQ2hCLGdCQUFjLEVBQUE7O0FBekd0QjtFQThHUSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQXJIdkI7RUEySFEsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsVUFBVTtFQUNWLG1CQUFtQixFQUFBOztBQS9IM0I7RUFtSVEsa0JBQWtCLEVBQUE7O0FBbkkxQjtFQXVJUSxpREFBaUQ7RUFDakQsa0NBQWtDO0VBQ2xDLFlBQVk7RUFDWixhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7O0FBM0l4QjtFQStJUSxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBaEp4QjtFQXFKUSxXQUFXLEVBQUE7O0FBckpuQjtJQXdKWSxrQkFBa0I7SUFDbEIsa0JBQWtCLEVBQUE7O0FBeko5QjtNQWlLb0IsYUFBYTtNQUNiLG1CQUFtQixFQUFBOztBQU92QztFQUNJLG1CQUFtQjtFQUNuQjtBQUFhLEVBQUE7O0FBR2pCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSx1QkFBdUI7RUFDdkIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsOENBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQix3QkFBd0I7RUFDeEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLDBCQUEwQjtFQUMxQix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLFlBQVk7RUFDWiwyQkFBQSxFQUE0Qjs7QUFHaEM7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1Y7RUFDSSxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sVUFBVSxFQUFBOztBQUdkO0VBQ0ksWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcblxuICAgIC5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbiAgICAgICAgXG4gICAgfVxuXG4gICAgLmhlYWRlci10aXRsZSB7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbiAgICB9XG5cbiAgICAuY2xhc3Mtc2VhcmNoIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB9XG5cbiAgICAuc2VhcmNoYmFyLWlucHV0IHtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAycHggIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICAuYmFkZ2UtY29sb3Ige1xuICAgICAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG5cbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZSB7XG4gICAgICAgIGJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgfVxuXG4gICAgLmJ0bi12aWV3IHtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgIC0tcGFkZGluZy10b3A6IDhweDtcbiAgICAgICAgLS1wYWRkaW5nLWJvdHRvbTogOHB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gICAgICAgIC0tcGFkZGluZy1lbmQ6IDEwcHg7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzAwM2E1NDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzAwM2E1NDtcbiAgICAgICAgbWF4LWhlaWdodDogbWF4LWNvbnRlbnQ7XG4gICAgfVxuXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gICAgICAgIGJhY2tncm91bmQ6ICMwMDNhNTQ7XG4gICAgfVxuXG4gICAgLmZpbHRlcnMge1xuICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuXG4gICAgICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgICAgICAgY29sb3I6ICNjY2M7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICYuY29sLXdpdGgtYXJyb3cge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcCB7XG4gICAgICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogcmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgLnNlbGVjdGVkIHtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiAxMHB4O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmNvbnRhaW5lciB7XG4gICAgICAgIDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5jYXRlZ29yeS1zY3JvbGwge1xuICAgICAgICAgICAgb3ZlcmZsb3c6IGF1dG87XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjMxMTE3O1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMTJweCAxMHB4IDEycHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuaGVhcnQtaWNvbiB7XG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiB0cmFuc3BhcmVudDtcbiAgICAgICAgY29sb3I6ICNiMzExMTc7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIC13ZWJraXQtbWFyZ2luLXN0YXJ0OiAwO1xuICAgICAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgICAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgICAgIG1hcmdpbi1pbmxpbmUtZW5kOiAwO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgICAgIC0tcGFkZGluZy1lbmQ6IDA7XG5cbiAgICB9XG5cbiAgICAuY2F0ZWdvcnktc3BhbiB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgYmFja2dyb3VuZDogIzg0MTAxNDtcbiAgICAgICAgcGFkZGluZzogOHB4IDEwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cblxuXG5cbiAgICBpb24tYmFkZ2Uge1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDBweDtcbiAgICAgICAgcmlnaHQ6IDBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICB9XG5cbiAgICAuY2F0ZWdvcnktYmxvY2sge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgfVxuXG4gICAgLmNhdGVnb3J5LWJhbm5lciB7XG4gICAgICAgIGJvcmRlci1sZWZ0OiA4cHggc29saWQgdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICAuY2FyZC10aXRsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICB9XG5cbiAgICBpb24tc2xpZGVzIHtcbiAgICAgICAgLy8gYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgICBjb2xvcjogI2ZmZjtcblxuICAgICAgICBpb24tc2xpZGUge1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IHN0YXJ0O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgICAgICAuc2xpZGUtY29udGVudCB7XG4gICAgICAgICAgICAgICAgLy8gcGFkZGluZzogMHB4IDE0JTtcblxuXG5cbiAgICAgICAgICAgICAgICAudGV4dCBwIHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZjRmOGZjKVxufVxuXG4uaGVhZGluZy10aXRsZSB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgcGFkZGluZzogNXB4IDIwcHggNXB4IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbmlvbi1jYXJkLnByb2R1Y3QtY2FyZCB7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCwgI2ZmZik7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMTBweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4ucHJvZHVjdC1jYXJkLW1lZGlhIHtcbiAgICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xuICAgIC13ZWJraXQtcGFkZGluZy1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5wcm9kdWN0LWNhcmQtbWVkaWEgaW1nIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1heC1oZWlnaHQ6IG5vbmUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnByb2R1Y3QtdGl0bGUge1xuICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMztcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICBtYXgtaGVpZ2h0OiA0OHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5wcm9kdWN0LWNvbnRlbnQge1xuICAgIHBhZGRpbmc6IDAgMTBweDtcbn1cblxuLlByb2R1Y3RzLXNsaWRlciB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC8qIHNsaWRlciBoZWlnaHQgeW91IHdhbnQgKi9cbn1cblxuLnN0YXItcmF0aW5nIHtcbiAgICBmb250LWZhbWlseTogXCJGb250QXdlc29tZVwiO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDc4cHg7XG59XG5cbi5zdGFyLXJhdGluZyB7XG4gICAgbWFyZ2luOiAxMHB4IGF1dG87XG59XG5cbi5zdGFyLXJhdGluZzo6YmVmb3JlIHtcbiAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW4ge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGxlZnQ6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMS41ZW07XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW46OmJlZm9yZSB7XG4gICAgY29sb3I6ICNmYWI5MDI7XG4gICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGVmdDogMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyAucmF0aW5nIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4ucHJvZHVjdC1wcmljZSBzcGFuIHtcbiAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmFkZC1XaXNobGlzdCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICByaWdodDogMnB4O1xufVxuXG4uYWRkLVdpc2hsaXN0IGltZyB7XG4gICAgaGVpZ2h0OiAxNnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm5/ionic-selectable.min.js");
/* harmony import */ var _Service_user_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/user-service.service */ "./src/app/Service/user-service.service.ts");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, sanitizer, menuCtrl, events, loadingService, toastController, alert, homepageService, cartService, router, toastCtrl, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.sanitizer = sanitizer;
        this.menuCtrl = menuCtrl;
        this.events = events;
        this.loadingService = loadingService;
        this.toastController = toastController;
        this.alert = alert;
        this.homepageService = homepageService;
        this.cartService = cartService;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.service = service;
        this.sliderConfig = {
            initialSlide: 0,
            speed: 400,
            slidesPerView: 2,
            autoplay: false,
        };
        this.bannerSlider = {
            initialSlide: 0,
            speed: 2000,
            slidesPerView: 1,
            autoplay: true,
        };
        this.value = "100";
        this.map = new Map();
        this.selectedPath = '';
        this.textToSearch = '';
        this.apiUrl = "https://www.niletechinnovations.com/eq8tor";
        this.users = [];
        this.homePageModel = [];
        this.featuredRatingArray = [];
        this.latestRatingArray = [];
        this.selectedCategory = [];
        this.wishListAdded = false;
        // menuArray = [];
        this.variationProduct = false;
        this.cart = [];
        this.totalCount = '';
        this.items = [];
        this.liked = false;
        this.router.events.subscribe(function (event) {
            _this.selectedPath = event.url;
        });
        this.dynamicColor = 'redish';
        this.showCategories();
        this.menuCtrl.enable(true);
        //this.showCategories123();
    }
    HomePage.prototype.openSearchPage = function () {
        this.router.navigate(['searchpage']);
    };
    HomePage.prototype.ngOnInit = function () {
        this.items = this.cartService.getProducts();
        // this.cart = this.cartService.getCart();
        this.getCartContent();
        this.getHomePage();
    };
    HomePage.prototype.addToCart = function (product) {
        this.cartService.addProduct(product);
        console.log(product);
    };
    HomePage.prototype.RefreshPage = function () {
        this.showCategories();
        this.getHomePage();
    };
    HomePage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter');
        this.getCartContent();
    };
    HomePage.prototype.openCart = function () {
        this.router.navigate(['cart']);
    };
    HomePage.prototype.userChanged = function (event) {
        console.log('event', event);
    };
    HomePage.prototype.goToProductDetail = function (product) {
        this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
        console.log("response product", product);
    };
    HomePage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    HomePage.prototype.getUsers = function () {
        console.log("resppppppp");
        this.service.getUsers()
            .subscribe(function (resp) {
            console.log("resppppppp", resp);
            // this.users = resp
        }, function (err) { console.log(err); });
    };
    HomePage.prototype.like = function (productId, id) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.addToWishlist(productId).subscribe(function (resp) {
            console.log(resp);
            _this.presentToast(resp.message);
            _this.latestProducts[id]._wishlist = true;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.presentToast(err.message);
            _this.loadingService.dismiss();
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.likeFeatured = function (productId, id) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.addToWishlist(productId).subscribe(function (resp) {
            console.log(resp);
            _this.presentToast(resp.message);
            _this.featuredItems[id]._wishlist = true;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.presentToast(err.message);
            _this.loadingService.dismiss();
        }, function () {
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.presentToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.getHomePage = function () {
        var _this = this;
        this.loadingService.present();
        var variationLatestProductsPriceArray = [];
        this.homepageService.getHomePageData().subscribe(function (response) {
            _this.homePageModel = response['data'];
            _this.featuredItems = _this.homePageModel["features_items"];
            _this.latestProducts = _this.homePageModel["latest_items"];
            console.log("latestProducts", _this.latestProducts);
            _this.bannerList = _this.homePageModel["slider"];
            if (_this.latestProducts.length > 0) {
                // for (let i = 0; i < this.latestProducts.length; i++) {
                //   let productType = this.latestProducts[i].post_type;
                //   if (productType == "configurable_product"){
                //     console.log(productType);
                //     variationLatestProductsPriceArray.push(this.latestProducts[i])
                //   }
                // }
                // console.log("variationPriceArray",variationLatestProductsPriceArray);
            }
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.userSearch = function (event) {
        this.getUsers();
        var textSearch = event.target.value;
        this.textToSearch = textSearch;
        console.log(textSearch);
    };
    HomePage.prototype.getCartContent = function () {
        var _this = this;
        console.log("getCartContent");
        this.homepageService.getCartProducts().subscribe(function (resp) {
            console.log("getCartContent", JSON.stringify(resp));
            _this.cart = resp.data.products;
            _this.totalCount = resp.data.total_product;
        }, function (err) {
            console.log("getCartContent error", err);
        });
    };
    HomePage.prototype.showCategories = function () {
        var _this = this;
        this.loadingService.present();
        this.homepageService.showCategories().subscribe(function (resp) {
            console.log("resp", resp);
            var data = resp.data.categories;
            _this.categories = data;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        // this.showLeftButton = currentIndex !=== 0;
        // this.showRightButton = currentIndex !=== Math.ceil(this.slides.length['length'] / 3);
    };
    // Method that shows the next slide
    HomePage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    // Method that shows the previous slide
    HomePage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    HomePage.prototype.ionViewDidEnter = function () {
        document.removeEventListener("backbutton", function (e) {
            console.log("disable back button");
        }, true);
    };
    HomePage.prototype.categoryClick = function (text, name) {
        console.log("hello", text);
        this.router.navigate(['subcategory'], { queryParams: { 'id': text, 'title': name }, });
    };
    HomePage.prototype.ViewAllFeatured = function () {
        this.router.navigate(['view-all-page'], { queryParams: { 'type': 'featured' }, });
    };
    HomePage.prototype.ViewAllLatest = function () {
        this.router.navigate(['view-all-page'], { queryParams: { 'type': 'latest' }, });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])("attr.style"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], HomePage.prototype, "value", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
    ], HomePage.prototype, "slides", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mySelect'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ionic_selectable__WEBPACK_IMPORTED_MODULE_4__["IonicSelectableComponent"])
    ], HomePage.prototype, "selectComponent", void 0);
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__["DomSanitizer"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__["AlerServiceService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_7__["HomepageService"], _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__["CartService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _Service_user_service_service__WEBPACK_IMPORTED_MODULE_5__["UserServiceService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map