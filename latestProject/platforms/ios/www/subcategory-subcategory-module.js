(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["subcategory-subcategory-module"],{

/***/ "./src/app/subcategory/subcategory.module.ts":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.module.ts ***!
  \***************************************************/
/*! exports provided: SubcategoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcategoryPageModule", function() { return SubcategoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _subcategory_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./subcategory.page */ "./src/app/subcategory/subcategory.page.ts");







var routes = [
    {
        path: '',
        component: _subcategory_page__WEBPACK_IMPORTED_MODULE_6__["SubcategoryPage"]
    }
];
var SubcategoryPageModule = /** @class */ (function () {
    function SubcategoryPageModule() {
    }
    SubcategoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_subcategory_page__WEBPACK_IMPORTED_MODULE_6__["SubcategoryPage"]]
        })
    ], SubcategoryPageModule);
    return SubcategoryPageModule;
}());



/***/ }),

/***/ "./src/app/subcategory/subcategory.page.html":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">{{headerTitle}}</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n    <div *ngIf=\"subCategoryArray && subCategoryArray.length \" class=\"container\">\n        <div class=\"category-scroll\" scrollX=\"true\">\n          <span class=\"category-span\"\n            *ngFor=\"let subCategory of subCategoryArray\" (click)=\"filterProduct(subCategory.slug)\">{{subCategory.name}}</span>\n        </div>\n      </div>\n    <ion-grid>\n         <ion-row>\n           <ion-col size=\"6\" *ngFor=\"let products of productsArray\">\n                <ion-card class=\"product-card\">\n                  <div *ngIf=\"products.post_image_url === ''; else placeholder\">\n                    <ion-card-content class=\"product-card-media\">\n                      <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"../../assets/imgs/icon.png\">\n                    </ion-card-content>\n                  </div>\n                  <ng-template #placeholder>\n                    <ion-card-content class=\"product-card-media\" >\n                        <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"https://www.niletechinnovations.com/eq8tor{{products.post_image_url}}\">\n                    </ion-card-content>\n                  </ng-template>\n                </ion-card>\n                <ion-card-content class=\"product-content\">\n                    <h2 class=\"product-title\">\n                        {{products.post_title}}\n                    </h2>\n                    <div *ngIf=\"products._rating === 0\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:0%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 0 && products._rating < 10\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:6%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 10\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:10%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 10 && products._rating < 20\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:16%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 20\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:20%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 20 && products._rating < 30\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:26%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 30\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:30%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 30 && products._rating < 40\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:36%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 40\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:40%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 40 && products._rating < 50\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:46%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 50\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:50%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 50 && products._rating < 60\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:56%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 60\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:60%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 60 && products._rating < 70\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:66%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 70\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:70%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 70 && products._rating < 80\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:76%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 80\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:80%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 80 && products._rating < 90\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:86%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 90\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:90%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating > 90 && products._rating < 100\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:96%\"></span>\n                        </div>\n                      </div>\n                      <div *ngIf=\"products._rating === 100\">\n                        <div class=\"star-rating\">\n                          <span style=\"width:100%\"></span>\n                        </div>\n                      </div>\n                    <div *ngIf=\"products.post_type === 'simple_product' \" class=\"product-price\">\n                        <span>{{products.post_price | currency:'USD':'symbol'}}</span>\n                      </div>\n                      <div *ngIf=\"products.post_type === 'configurable_product'\" class=\"product-price\">\n                        <div *ngFor=\"let variation of products._variations_data; let i = index\">\n                          <span *ngIf=\"i == 0\">{{variation._variation_post_price | currency:'USD':'symbol'}}</span>\n                        </div>\n                      </div>\n                </ion-card-content>\n           </ion-col>\n         </ion-row>\n     </ion-grid>\n   </ion-content>\n"

/***/ }),

/***/ "./src/app/subcategory/subcategory.page.scss":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container ::-webkit-scrollbar {\n  display: none; }\n\n.container .category-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n.category-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n.new-background-color {\n  --background: #b31117; }\n\n.text {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  line-height: 16px;\n  max-height: 48px;\n  -webkit-line-clamp: 3; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.back-arrow {\n  color: #fff; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\nion-card.product-card {\n  -webkit-margin-start: 0;\n  margin-inline-start: 0px;\n  background: #fff;\n  border-radius: 3px;\n  --background: var(--ion-item-background, #fff);\n  box-shadow: none;\n  margin-bottom: 0;\n  min-height: 200px;\n  -webkit-margin-end: 10px;\n  margin-inline-end: 10px;\n  margin-bottom: 10px;\n  margin-top: 0px; }\n\n.product-card-media {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 200px;\n  overflow: hidden; }\n\n.product-card-media img {\n  width: 100% !important;\n  max-height: none !important;\n  max-width: none !important; }\n\n.product-title {\n  color: #656c72;\n  font-size: 14px;\n  font-weight: 500;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  max-height: 48px;\n  -webkit-line-clamp: 3;\n  text-align: center; }\n\n.product-content {\n  padding: 0 10px; }\n\n.Products-slider {\n  height: auto;\n  /* slider height you want */ }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 18px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.product-price {\n  text-align: center; }\n\n.product-price span {\n  color: #656c72;\n  font-size: 16px;\n  font-weight: 600; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3N1YmNhdGVnb3J5L3N1YmNhdGVnb3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGFBQWEsRUFBQTs7QUFGckI7RUFNUSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQiw0QkFBNEIsRUFBQTs7QUFJcEM7RUFDSSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUduQjtFQUNJLHFCQUFhLEVBQUE7O0FBR2pCO0VBQ0ksZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixxQkFBcUIsRUFDTzs7QUFHaEM7RUFDSSxXQUFXO0VBQ1gsa0JBQ0osRUFBQTs7QUFFQTtFQUNJLFdBQVcsRUFBQTs7QUFHZjtFQUNJLG1CQUFtQjtFQUNuQjtBQUFhLEVBQUE7O0FBR2pCO0VBQ0ksdUJBQXVCO0VBQ3ZCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLDhDQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsd0JBQXdCO0VBQ3hCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBOztBQUduQjtFQUNJLDBCQUEwQjtFQUMxQix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIsb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLFlBQVk7RUFDWiwyQkFBQSxFQUE0Qjs7QUFHaEM7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1Y7RUFDSSxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxrQkFDSixFQUFBOztBQUVBO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3N1YmNhdGVnb3J5L3N1YmNhdGVnb3J5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xuICAgIDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIC5jYXRlZ29yeS1zY3JvbGwge1xuICAgICAgICBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2IzMTExNztcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcbiAgICB9XG59XG5cbi5jYXRlZ29yeS1zcGFuIHtcbiAgICBtYXJnaW4tYm90dG9tOiBhdXRvO1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQ6ICM4NDEwMTQ7XG4gICAgcGFkZGluZzogOHB4IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cblxuLnRleHQge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbWF4LWhlaWdodDogNDhweDtcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDM7XG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcbn1cblxuLmhlYWRlci10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyXG59XG5cbi5iYWNrLWFycm93IHtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuaW9uLWNvbnRlbnQuaG9tZS1jb250ZW50IHtcbiAgICBiYWNrZ3JvdW5kOiAjZjRmOGZjO1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsICNmNGY4ZmMpXG59XG5cbmlvbi1jYXJkLnByb2R1Y3QtY2FyZCB7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCwgI2ZmZik7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMTBweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDBweDtcbn1cblxuLnByb2R1Y3QtY2FyZC1tZWRpYSB7XG4gICAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDBweDtcbiAgICAtd2Via2l0LXBhZGRpbmctZW5kOiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgaGVpZ2h0OiAyMDBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucHJvZHVjdC1jYXJkLW1lZGlhIGltZyB7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtaGVpZ2h0OiBub25lICFpbXBvcnRhbnQ7XG4gICAgbWF4LXdpZHRoOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5wcm9kdWN0LXRpdGxlIHtcbiAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICAgIG1heC1oZWlnaHQ6IDQ4cHg7XG4gICAgLXdlYmtpdC1saW5lLWNsYW1wOiAzO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnByb2R1Y3QtY29udGVudCB7XG4gICAgcGFkZGluZzogMCAxMHB4O1xufVxuXG4uUHJvZHVjdHMtc2xpZGVyIHtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLyogc2xpZGVyIGhlaWdodCB5b3Ugd2FudCAqL1xufVxuXG4uc3Rhci1yYXRpbmcge1xuICAgIGZvbnQtZmFtaWx5OiBcIkZvbnRBd2Vzb21lXCI7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogNzhweDtcbn1cblxuLnN0YXItcmF0aW5nIHtcbiAgICBtYXJnaW46IDEwcHggYXV0bztcbn1cblxuLnN0YXItcmF0aW5nOjpiZWZvcmUge1xuICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxlZnQ6IDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xufVxuXG4uc3Rhci1yYXRpbmcgc3BhbiB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbGVmdDogMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBhZGRpbmctdG9wOiAxLjVlbTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xufVxuXG4uc3Rhci1yYXRpbmcgc3Bhbjo6YmVmb3JlIHtcbiAgICBjb2xvcjogI2ZhYjkwMjtcbiAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIC5yYXRpbmcge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5wcm9kdWN0LXByaWNlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuLnByb2R1Y3QtcHJpY2Ugc3BhbiB7XG4gICAgY29sb3I6ICM2NTZjNzI7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG59Il19 */"

/***/ }),

/***/ "./src/app/subcategory/subcategory.page.ts":
/*!*************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.ts ***!
  \*************************************************/
/*! exports provided: SubcategoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcategoryPage", function() { return SubcategoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");







var SubcategoryPage = /** @class */ (function () {
    function SubcategoryPage(route, loadingService, homepageService, router, alert, navCtrl) {
        this.route = route;
        this.loadingService = loadingService;
        this.homepageService = homepageService;
        this.router = router;
        this.alert = alert;
        this.navCtrl = navCtrl;
        // console.log("home page data",HomePageData);
    }
    SubcategoryPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    SubcategoryPage.prototype.getSubCategory = function (catID) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getCategoryProducts(catID).subscribe(function (response) {
            var resp = response.data;
            _this.subCategoryArray = resp.categories;
            console.log("this.subCategoryArray", _this.subCategoryArray);
            _this.productsArray = resp.products;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello getSubCategory");
            });
            _this.loadingService.dismiss();
        });
    };
    SubcategoryPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            console.log(params.id);
            _this.headerTitle = params.title;
            _this.getSubCategory(params.id);
        });
    };
    SubcategoryPage.prototype.showProducts = function (itemName) {
        console.log("showProducts", itemName);
        this.router.navigate(['productlisting'], { queryParams: { 'id': itemName } });
    };
    SubcategoryPage.prototype.goToProductDetail = function (product) {
        this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
        console.log("response product", product);
    };
    SubcategoryPage.prototype.filterProduct = function (product) {
        console.log(product);
        this.getSubCategoryFilter(product);
    };
    SubcategoryPage.prototype.getSubCategoryFilter = function (catID) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getCategoryProducts(catID).subscribe(function (response) {
            var resp = response.data;
            _this.productsArray = resp.products;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello getSubCategory");
            });
            _this.loadingService.dismiss();
        });
    };
    SubcategoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-subcategory',
            template: __webpack_require__(/*! ./subcategory.page.html */ "./src/app/subcategory/subcategory.page.html"),
            styles: [__webpack_require__(/*! ./subcategory.page.scss */ "./src/app/subcategory/subcategory.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__["HomepageService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__["AlerServiceService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], SubcategoryPage);
    return SubcategoryPage;
}());



/***/ })

}]);
//# sourceMappingURL=subcategory-subcategory-module.js.map