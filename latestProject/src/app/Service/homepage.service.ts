import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, delay } from 'rxjs/operators';
import { HomePageData } from '../models/home.model';
import { del } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {
  apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
  constructor(private http: HttpClient) {

  }

  showCategories(): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'categories', '')
      .pipe(
        tap(_ => this.log('categories')),
      // catchError(this.handleError('login', []))
    );
  }
  showSubCategory(id): Observable<any> {

    return this.http.post<any>(this.apiUrl + 'categories?' + 'parent_id=' + id, '').pipe(tap(_ => this.log('subcategories')), );

  }
  private log(message: string) {
    console.log(message);
  }
  getHomePageData() {
    return this.http.get<HomePageData[]>('https://www.niletechinnovations.com/eq8tor/api/home').pipe(delay(200));
  }

  getCategoryProducts(name) {
    return this.http.post<any>(this.apiUrl + 'cat-products?' + 'slug=' + name, '').pipe(tap(_ => this.log('subcategories')), );

  }
  productDetail(product) {
    return this.http.post<any>(this.apiUrl + 'products/detail?' + 'slug=' + product, '').pipe(tap(_ => this.log('Detail')), )
  }
  searchApi(textSearch) {
    return this.http.post<any>(this.apiUrl + 'search?' + 'srch_term=' + textSearch, '').pipe(tap(_ => this.log('search api call')), );
  }
  loadMoreData(textSearch) {
    return this.http.post<any>(textSearch, '').pipe(tap(_ => this.log('search more api call')), );

  }
  addToWishlist(textSearch) {
    return this.http.post<any>(this.apiUrl + 'products/add/wishlist?' + 'product_id=' + textSearch, '').pipe(tap(_ => this.log('search api call')), );
  }
  getWishlistProducts() {
    return this.http.get<any>(this.apiUrl + 'user/wishlist-content').pipe(delay(10));
  }
  getAllOrders() {
    return this.http.get<any>(this.apiUrl + 'user/orders-content').pipe(delay(10));
  }
  removeFromWishList(productId) {
    return this.http.post<any>(this.apiUrl + 'product/remove/wishlist?' + 'product_id=' + productId, '').pipe(delay(10));
  }
  getCartProducts() {
    return this.http.get<any>(this.apiUrl + 'cart/products').pipe(delay(10));
  }
  addToCart(product_id: any, qty: any, variationID: any, color: any, size: any) {
    return this.http.post<any>(this.apiUrl + 'product/addtocart?' + 'product_id=' + product_id + '&qty=' + qty + '&variation_id=' + variationID + '&color=' + color + '&size=' + size, '').pipe(delay(10));
  }

  removeFromCart(productId, variationID) {
    return this.http.post<any>(this.apiUrl + 'product/remove-from-cart?' + 'product_id=' + productId + '&variation_id=' + variationID, '').pipe(delay(10));
  }

  submitRating(ratingValue, productContent, productID) {
    return this.http.post<any>(this.apiUrl + 'user/review?' + 'selected_rating_value=' + ratingValue + '&product_review_content=' + productContent + '&object_id=' + productID, '').pipe(delay(10));
  }
  viewOrderDetails(processId, orderid) {
    return this.http.get<any>(this.apiUrl + 'user/order/details?' + 'order_process_id=' + processId + '&order_id=' + orderid).pipe(delay(10));
  }
  updateProfile(displayName, Username, emailId, password) {
    return this.http.post<any>(this.apiUrl + 'user/profile/update?' + 'display_name=' + displayName + '&user_name=' + Username + '&email_id=' + emailId + '&password=' + password, '').pipe(delay(10));
  }
  updateAddress(billFirstName, billLastName, billEmailAdd, billPhoneNumber, billCountry, billAddress1, billTownCity, billPostalCode, shipFirstName, shipLastName, shipEmailAddress, shipPhoneNumber, shipCountry, shipAddress1, shipCity, shipPostalcode) {
    return this.http.post<any>(this.apiUrl + 'user/address?' + 'account_bill_first_name=' + billFirstName + '&account_bill_last_name=' + billLastName + '&account_bill_email_address=' + billEmailAdd + '&account_bill_phone_number=' + billPhoneNumber + '&account_bill_select_country=' + billCountry + '&account_bill_adddress_line_1=' + billAddress1 + '&account_bill_town_or_city=' + billTownCity + '&account_bill_zip_or_postal_code=' + billPostalCode + '&account_shipping_first_name=' + shipFirstName + '&account_shipping_last_name=' + shipLastName + '&account_shipping_email_address=' + shipEmailAddress + '&account_shipping_phone_number=' + shipPhoneNumber + '&account_shipping_select_country=' + shipCountry + '&account_shipping_adddress_line_1=' + shipAddress1 + '&account_shipping_town_or_city=' + shipCity + '&account_shipping_zip_or_postal_code=' + shipPostalcode, '').pipe(delay(10));
  }
  updateAddressPost(data) {
    return this.http.post<any>(this.apiUrl + 'user/address', data).pipe(delay(10));
  }
  getCountries() {
    return this.http.get<any>(this.apiUrl + 'country/lists').pipe(delay(10));
  }

  getAllProducts(type) {
    return this.http.get<any>(this.apiUrl + 'all-products?' + 'type=' + type).pipe(delay(10));
  }
  updateCartProducts(productID, quantity, variationID) {
    return this.http.post<any>(this.apiUrl + 'update/cart/quantity?' + 'product_id=' + productID + '&qty=' + quantity + '&variation_id=' + variationID, '').pipe(delay(10));
  }
  stripeCheckoutApi(paymentOption, tokenId, totalAmount, totalTax, member, membershipDiscount,shippingCost) {
    return this.http.post<any>(this.apiUrl + 'checkout?' + 'payment_option=' + paymentOption + '&stripe_token=' + tokenId + '&total_amount=' + totalAmount + '&total_tax=' + totalTax + '&is_member=' + member + '&membership_discount=' + membershipDiscount + '&shipping_cost='+ shippingCost, '').pipe(delay(10));
  }
  cancelReturnOrder(orderID, Status) {
    return this.http.post<any>(this.apiUrl + 'update/order/status?' + 'order_id=' + orderID + '&change_order_status=' + Status, '').pipe(delay(10));
  }
  applyCoupon(code){
    return this.http.post<any>(this.apiUrl + 'apply/coupon?' + 'couponCode='+code,'').pipe(delay(10));
  }
  removeCoupon(){
    return this.http.get<any>(this.apiUrl + 'remove/coupon').pipe(delay(10));
  }
  cmsApiImplement(){
    return this.http.get<any>(this.apiUrl +'site/pages').pipe(delay(10));
  }
}
