import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular'
import { timeout } from 'q';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loaderToShow: any;
  constructor(public loadingController: LoadingController) {}

    async present(){
    //  const loader = this.loadingController.create({
    //     spinner: 'circles',
    //     translucent: true,
    //   }).then((loader)=>{loader.present()});
    return await this.loadingController.create({
      spinner: 'circles'
    });

    // this.loaderToShow = this.loadingController.create({
    //   message: 'This Loader will Not AutoHide'
    // }).then((res) => {
    //   res.present();

    //   res.onDidDismiss().then((dis) => {
    //     console.log('Loading dismissed! after 2 Seconds');
    //   });
    // });
    // this.dismiss();
    //  this.isLoading = true;
    //  return await this.loadingCtrl.create({}).then(a=>{
    //    a.present().then(()=>{
    //      if(!this.isLoading){
    //        a.dismiss().then(()=>console.log('abort presenting'));
    //      }
    //    });
    //  });
   }

    dismiss(){
    this.loadingController.dismiss();
  }
    //  this.isLoading = false;
    //  return await this.loadingCtrl.dismiss().then(()=> console.log('dismissed'));
   }

