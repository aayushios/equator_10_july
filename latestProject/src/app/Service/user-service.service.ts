import { Injectable } from '@angular/core';
import { HttpClient,HttpRequest } from '@angular/common/http';
import { User } from '../models/user.model';
import {delay, tap, catchError} from 'rxjs/operators'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  baseUrl:string = "https://www.niletechinnovations.com/eq8tor/api/";

  constructor(private http: HttpClient) {

  }

  getUsers() {
   return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users').pipe(delay(2500));
  }
  
}
