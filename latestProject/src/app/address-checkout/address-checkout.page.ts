import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../Service/loader.service';
import { ModalController, NavController, MenuController } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlerServiceService } from '../Service/aler-service.service';
import { EditAddressPage } from '../edit-address/edit-address.page';

@Component({
  selector: 'app-address-checkout',
  templateUrl: './address-checkout.page.html',
  styleUrls: ['./address-checkout.page.scss'],
})
export class AddressCheckoutPage implements OnInit {
  billCompanyName = "";
  billAddress1 = "";
  billAddress2 = "";
  billCity = "";
  billPostCode = "";
  billCountry = "";
  billPhone = "";
  billEmail = "";
  billingAddressFirstName = "";
  billingAddressLastName = "";
  shipCompanyName = "";
  shipAddress1 = "";
  shipAddress2 = "";
  shipCity = "";
  shipPostCode = "";
  shipCountry = "";
  shipPhone = "";
  shipEmail = "";
  shippingAddressFirstName = "";
  shippingAddressLastName = "";
  hideMe: boolean;
  totalAmount = "";
  membershipDiscount = "";
  isMember = "";
  totalTax = "";
  newUser = true;
  disableButton = false;
  paymentType = "";
  shippingCost = "";
  constructor(public homePageService: HomepageService, public menuCtrl: MenuController, private router: Router, private alert: AlerServiceService, private route: ActivatedRoute, private navCtrl: NavController, private modalController: ModalController, private loadingService: LoaderService) {
    this.getWishlist();
    this.route.queryParams.subscribe(params => {
      console.log("params", params);
      this.totalAmount = params.total_amount;
      this.membershipDiscount = params.membership_discount;
      this.totalTax = params.total_tax;
      this.isMember = params.is_member;
      this.paymentType = params.paymentType;
      this.shippingCost = params.shipping_cost;
    });
  }

  ngOnInit() {
    this.disableButton = false;
  }
  hide() {
    this.hideMe = true;
  }
  goBack() {
    this.navCtrl.back();
  }
  ionViewWillEnter() {
    this.disableButton = false;
  }
  placeOrder() {
    if (this.paymentType == 'Stripe') {
      let checkNewUser = localStorage.getItem("newUser");
      if (checkNewUser == 'true') {
        this.disableButton = true;
      } else {
        this.disableButton = false;
      }
      this.router.navigate(['stripe-checkout'], { queryParams: { 'total_amount': this.totalAmount, 'total_tax': this.totalTax, 'membership_discount': this.membershipDiscount, 'is_member': this.isMember, 'shipping_cost': this.shippingCost }, });
    } else {
      this.disableButton = true;
      this.loadingService.present().then(event => {
        event.present();
        this.homePageService.stripeCheckoutApi('cod', "", this.totalAmount, this.totalTax, this.isMember, this.membershipDiscount, this.shippingCost).subscribe(resp => {
          console.log("COD response", JSON.stringify(resp));
          event.dismiss();
          if (resp.success == true) {
            this.alert.myAlertMethod("Successfully", resp.message, data => {
              console.log("data", data)
              this.router.navigate(['your-orders']);
            });
          } else {
            this.alert.myAlertMethod("Something went wrong. Please try again after sometime.", resp.message, data => {
              console.log("Something went wrong.")
            });
          }
        }, (err) => {
          event.dismiss();
          this.alert.myAlertMethod("error", err.message, data => {
            console.log("error")
          });
          console.log(err);
        }, () => {
          event.dismiss();
        });
      });

    }

  }

  async editAddress() {
    const modal = await this.modalController.create({
      component: EditAddressPage,
    });

    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        console.log("sizeChartResponse", sizeChartResponse);
        this.getWishlist();
      }
    });
    return await modal.present();
  }

  getWishlist() {
    this.loadingService.present().then(event => {
      event.present();
      this.homePageService.getWishlistProducts().subscribe(resp => {

        if (resp.data == null || resp.data.address_details == "") {
          this.disableButton = true;
          // this.newUser = true;
          return;
        } else {
          console.log(false);
          this.disableButton = false;
          //  this.newUser = false;
        }
        this.billCompanyName = resp.data.address_details.account_bill_company_name;
        this.billAddress1 = resp.data.address_details.account_bill_adddress_line_1;
        this.billAddress2 = resp.data.address_details.account_bill_adddress_line_2;
        this.billCity = resp.data.address_details.account_bill_town_or_city;
        this.billPostCode = resp.data.address_details.account_bill_zip_or_postal_code;
        this.billCountry = resp.data.address_details.account_bill_select_country;
        this.billPhone = resp.data.address_details.account_bill_phone_number;
        this.billEmail = resp.data.address_details.account_bill_email_address;
        this.billingAddressFirstName = resp.data.address_details.account_bill_first_name;
        this.billingAddressLastName = resp.data.address_details.account_bill_last_name;

        this.shipCompanyName = resp.data.address_details.account_shipping_company_name;
        this.shipAddress1 = resp.data.address_details.account_shipping_adddress_line_1;
        this.shipAddress2 = resp.data.address_details.account_shipping_adddress_line_2;
        this.shipCity = resp.data.address_details.account_shipping_town_or_city;
        this.shipPostCode = resp.data.address_details.account_shipping_zip_or_postal_code;
        this.shipCountry = resp.data.address_details.account_shipping_select_country;
        this.shipPhone = resp.data.address_details.account_shipping_phone_number;
        this.shipEmail = resp.data.address_details.account_shipping_email_address;
        this.shippingAddressFirstName = resp.data.address_details.account_shipping_first_name;
        this.shippingAddressLastName = resp.data.address_details.account_shipping_last_name;
        event.dismiss();
      },
        (err) => {
          event.dismiss();
          console.log("error", err);
        }, () => {
          event.dismiss();
        });
    });

  }

}
