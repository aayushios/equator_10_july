import { Component, ViewChild, QueryList } from '@angular/core';

import { Platform, Events, IonRouterOutlet, ModalController, MenuController, ActionSheetController, PopoverController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HomepageService } from './Service/homepage.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage'
import { BehaviorSubject, Observable, Subscription } from 'rxjs'
import { AuthService } from './Service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['../app/app.scss'],
})
export class AppComponent {
  showSubmenu: boolean = false;
  private homepageService: HomepageService;
  @ViewChild(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  categories: any;
  authState$: BehaviorSubject<boolean> = new BehaviorSubject(null);
  menuArrayHome = [];
  window: any;
  pages = [
    {
      title: 'Home',
      icon: '../assets/imgs/home.svg'
    },
    // {
    //   title: 'Categories',
    //   icon: 'arrow-dropright',
    // },
    {
      title: 'Wishlist',
      icon: '../assets/imgs/Wishlist-menu.svg'
    }, {
      title: 'Your Orders',
      icon: '../assets/imgs/orders.svg'
    }, {
      title: 'My Profile',
      icon: '../assets/imgs/profile.svg'
    },
    {
      title: 'About Us',
      icon: '../assets/imgs/about.svg'
    },
    {
      title: 'Return Policy',
      icon: '../assets/imgs/return.svg'
    },
    {
      title: 'Terms Of Use',
      icon: '../assets/imgs/terms.svg'
    },
    {
      title: 'Help & Support',
      icon: '../assets/imgs/help.svg'
    },
    {
      title: 'Logout',
      icon: '../assets/imgs/close.svg'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public events: Events,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    private router: Router,
    public modalCtrl: ModalController,
    private toast: ToastController,
    private menu: MenuController,
    private storage: Storage,
    private auth: AuthService
  ) {
    this.menuArrayHome = [];
    console.log("events", this.events);
    this.initializeApp();
  }
  authState1$: Observable<boolean>;
  initializeApp() {
    console.log(this.events);
    this.platform.ready().then(() => {
      let token = localStorage.getItem("access_token");
      if (token) {
        this.router.navigate(['/home']);
      } else {
        this.router.navigate(['/user-login']);
      }
      // this.auth.checkToken();
      this.statusBar.styleDefault();
      if (this.platform.is("ios")) {
        console.log("ios running");
        this.statusBar.overlaysWebView(true);
      }
      if (this.platform.is("android")) {
        console.log("android running");
        this.platform.backButton.subscribeWithPriority(9999,()=>
      {
        console.log("harware back button disabled")
      })
        this.statusBar.overlaysWebView(false);
      }
      this.splashScreen.hide();
      this.events.subscribe('user:created', (menuArray) => {
        this.menuArrayHome = menuArray;
        console.log("menuArrayHome", [this.menuArrayHome])
      });
    });
  }
  
  menuClick(title) {
    console.log('menu called', title);
    if (title == 'LogOut') {
      this.storage.set('access_token', null);
      localStorage.clear();
      // this.authState$.next(false);
      this.router.navigate(['/user-login']);
    } else if (title == "Your Orders") {
      this.router.navigate(['/your-orders']);
    } else if (title == "Wishlist") {
      this.router.navigate(['/wishlist']);
    } else if (title == "My Profile") {
      this.router.navigate(['/profile']);
    }
    else if (title == "Home") {
      this.router.navigate(['/home']);
    }else if (title == "Return Policy") {
      this.router.navigate(['cms'], { queryParams: { 'title': title }, })
    } else if (title == "About Us") {
      this.router.navigate(['cms'], { queryParams: { 'title': title }, })
    } else if (title == "Terms Of Use") {
      this.router.navigate(['cms'], { queryParams: { 'title': title }, })
    } else if (title == "Help & Support") {
      this.router.navigate(['cms'], { queryParams: { 'title': title }, })
    }
  }

  async presentToast() {
    const toast = await this.toast.create({
      message: 'Press back again to exit App.',
      duration: 2000,
    });
    toast.present();
  }
}
