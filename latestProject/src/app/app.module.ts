import { NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ImageModalPageModule } from './image-modal/image-modal.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicSelectableModule } from 'ionic-selectable';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { SizechartModalPageModule } from './sizechart-modal/sizechart-modal.module';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { OrderDetailsPageModule } from './order-details/order-details.module';
import { EditAddressPage } from './edit-address/edit-address.page';
import { EditAddressPageModule } from './edit-address/edit-address.module';
import { Stripe } from '@ionic-native/stripe/ngx';
import { RatingModalPage } from './rating-modal/rating-modal.page';
import { RatingModalPageModule } from './rating-modal/rating-modal.module';
import { IonicRatingModule } from 'ionic4-rating';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,IonicRatingModule, ReactiveFormsModule, IonicModule.forRoot(),ImageModalPageModule,RatingModalPageModule, HttpClientModule, IonicStorageModule.forRoot() ,AppRoutingModule, IonicSelectableModule,SizechartModalPageModule,OrderDetailsPageModule ,EditAddressPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    Stripe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },ImagePicker,Crop,FileTransfer,FileTransferObject, {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ], exports: [FormsModule, ReactiveFormsModule],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule {

}
