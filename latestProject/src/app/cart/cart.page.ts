import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../Service/cart.service';
import { NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { ValueAccessor } from '@ionic/angular/dist/directives/control-value-accessors/value-accessor';
import { AlerServiceService } from '../Service/aler-service.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  selectedItems = [];
  total = 0;
  totalAmount = "";
  coupon = '';
  couponBtnText = "";
  couponHeadingText = "";
  couponDiscount = 0;
  hideMe: boolean;
  status_loading = false;
  imgUrl = "https://www.niletechinnovations.com/eq8tor";
  qty: any;
  stockArray = [];
  taxRate = "";
  isMember = false;
  membershipDiscount = "";
  outOfStockArray = [];
  outOfStockMessage = '';
  checkoutDisable = false;
  totalProduct;
  shippingCost = "";
  constructor(private cartService: CartService, private alert: AlerServiceService, private alertService: AlerServiceService, public alertController: AlertController, public loadingService: LoaderService, public homepageService: HomepageService, public navCtrl: NavController, private router: Router) {
  }

  hide() {
    this.hideMe = true;
  }

  goBack() {
    this.navCtrl.back();
  }
  getCartItems() {
    this.loadingService.present().then(event =>{
      event.present();
      let items = [];
      this.stockArray = [];
      this.outOfStockArray = [];
      this.outOfStockMessage = "";
      this.checkoutDisable = false;
      this.totalProduct;
      this.homepageService.getCartProducts().subscribe(response => {
        console.log(response);
        items = response.data.products;
  
        this.totalAmount = response.data.total;
        this.shippingCost = response.data.shipping_cost;
        this.couponDiscount = response.data.coupon_discount;
        this.coupon = response.data.coupon_code;
        if (this.coupon == "" || this.coupon == null) {
          this.status_loading = false;
          this.couponBtnText = "Apply"
          this.couponHeadingText = "Apply Coupon";
          console.log(this.coupon, this.status_loading, this.couponBtnText, this.couponHeadingText)
        } else {
          this.status_loading = true;
          this.couponBtnText = "Remove"
          this.couponHeadingText = "Coupon Applied";
        }
        if (response.data.is_member == true) {
          this.isMember = true;
          this.membershipDiscount = response.data.member_discount;
        }
        this.totalProduct = response.data.total_product;
        if (this.totalProduct == 0) {
          this.checkoutDisable = true;
        }
        this.taxRate = response.data.taxRate;
        for (let i = 0; i < items.length; i++) {
          if (items[i].out_of_stock == false) {
            this.stockArray.push(items[i]);
          } else {
            this.outOfStockArray.push(items[i]);
          }
        }
        event.dismiss();
        if (this.outOfStockArray.length > 0) {
          this.outOfStockMessage = "There are some items in your cart that are out of stock."
          for (let i = 0; i < this.outOfStockArray.length; i++) {
            console.log("this.outOfStockArray[i]", this.outOfStockArray[i]);
            this.removeItem(this.outOfStockArray[i]);
            this.loadingService.dismiss();
          }
          this.alert.myAlertMethod("Out of Stock", "There are some items in your cart that are out of stock.", data => {
            console.log("Out of stock removed.");
            event.dismiss();
          });
        }
        items = this.stockArray;
        console.log("this.outOfstockArray", this.outOfStockArray)
        let selected = {};
        for (let obj of items) {
          if (selected[obj.variation_id]) {
            selected[obj.variation_id].count++
          } else {
            selected[obj.variation_id] = { ...obj, count: 1 };
          }
        }
        this.selectedItems = Object.keys(selected).map(key => selected[key]);
        this.total = this.selectedItems.reduce((a, b) => a + (b.quantity * b.price), 0);
        event.dismiss();
      }, (err) => {
        console.log("error get cart content", err);
        this.alert.myAlertMethod("Error", err.error.message, data => {
          console.log("error")
          event.dismiss();
        });
        event.dismiss();
      }, () => {
        event.dismiss();
      });
    });
    
  }
  ngOnInit() {
    this.getCartItems();
    this.status_loading = false;
  }
  removeItem(post) {
    this.loadingService.present().then(event=>{
      event.present();
      if (post.variation_id == undefined || null) {
        post.variation_id = "";
      }
      this.homepageService.removeFromCart(post.id, post.variation_id).subscribe(resp => {
        console.log(resp);
        let index = this.selectedItems.indexOf(post);
        if (index > -1) {
          this.selectedItems.splice(index, 1);
        }
        this.total = this.selectedItems.reduce((a, b) => a + (b.count * b.price), 0);
        this.getCartItems();
        if (this.selectedItems.length == 0) {
          this.checkoutDisable = true;
        }
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
      }, () => {
        event.dismiss();
      });
    });
    

  }
  incrementQty(quantity, productID, variationId) {
    this.loadingService.present().then(event =>{
      event.present();
      quantity += 1;
      if (variationId == null || undefined) {
        console.log("variationID is undefined");
        variationId = "";
      }
      this.homepageService.updateCartProducts(productID, quantity, variationId).subscribe(response => {
        console.log(response);
        this.getCartItems();
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
        this.alertService.myAlertMethod("Error", err.error.message, data => {
          console.log("error")
        });
      }, () => {
        event.dismiss();
      });
    });
   
  }

  decrementQty(quantity, productID, variationId) {
    this.loadingService.present().then(event =>{
      event.present();
      if (variationId == null || undefined) {
        console.log("variationID is undefined");
        variationId = "";
        event.dismiss();
      }
      if (quantity - 1 < 1) {
        quantity = 1
        console.log('1->' + quantity);
        event.dismiss();
      } else {
        quantity -= 1;
        this.homepageService.updateCartProducts(productID, quantity, variationId).subscribe(response => {
          console.log(response);
          this.getCartItems();
          event.dismiss();
        }, (err) => {
          console.log(err);
          event.dismiss();
          this.alertService.myAlertMethod("Error", err.error.message, data => {
            console.log("error");
          });
        });
        console.log('2->' + quantity);
      }
    });
    
  }
  goToCheckout() {
    // this.router.navigate(['stripe-checkout']);
    this.presentAlertRadio();
  }
  async presentAlertRadio() {

    const alert = await this.alertController.create({
      header: 'Choose Payment Method',
      inputs: [
        {
          name: 'Stripe',
          type: 'radio',
          label: 'Credit or Debit Card',
          value: 'Stripe'
        }
        // {
        //   name: 'Cash on Delivery',
        //   type: 'radio',
        //   label: 'Cash on Delivery',
        //   value: 'COD'
        // }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log(data);
            if (data == "COD") {
              console.log("COD");
              this.router.navigate(['address-checkout'], { queryParams: { 'total_amount': this.totalAmount, 'total_tax': this.taxRate, 'membership_discount': this.membershipDiscount, 'is_member': this.isMember,'shipping_cost':this.shippingCost }, });
            } else if (data == "Stripe") {
              console.log("Stripe");
              let checkNewUser = localStorage.getItem("newUser");
              if (checkNewUser == 'true') {
                this.router.navigate(['address-checkout'], { queryParams: { 'total_amount': this.totalAmount, 'total_tax': this.taxRate, 'membership_discount': this.membershipDiscount, 'is_member': this.isMember, 'paymentType': 'Stripe','shipping_cost':this.shippingCost }, });
              }
              else {
                this.router.navigate(['address-checkout'], { queryParams: { 'total_amount': this.totalAmount, 'total_tax': this.taxRate, 'membership_discount': this.membershipDiscount, 'is_member': this.isMember, 'paymentType': 'Stripe', 'shipping_cost':this.shippingCost }, });
              }
            }
          }
        }
      ]
    });
    await alert.present();
  }

  applyCoupon() {
    if (this.coupon == '' || this.coupon == null) {
      this.alertService.myAlertMethod("Invalid Coupon", "Please enter coupon code.", data => {
        console.log("error")
      });
    } else {
      if (this.couponBtnText == "Remove") {
        this.loadingService.present().then(event=>{
          event.present();
          this.homepageService.removeCoupon().subscribe(resp => {
            console.log(resp);
            if (resp.success == true) {
              event.dismiss();
              this.alertService.myAlertMethod("Removed", "Coupon Removed Successfully.", data => {
                console.log("Coupon success.")
                this.getCartItems();
              });
              this.status_loading = false;
              this.couponBtnText = "Apply";
              this.couponHeadingText = "Apply Coupon";
              this.coupon = '';
            } else {
              event.dismiss();
              this.alertService.myAlertMethod("Error", "Please try again later.", data => {
                console.log("error")
              });
            }
          }, (err) => {
            console.log(err);
            event.dismiss();
            this.alertService.myAlertMethod("Error", err.error.message, data => {
              console.log("error")
            });
          }, () => {
            event.dismiss();
          });
        });
        
      }
      else if (this.couponBtnText == "Apply") {
        this.loadingService.present().then(event=>{
          event.present();
          this.homepageService.applyCoupon(this.coupon).subscribe(resp => {
            event.dismiss();
            this.alertService.myAlertMethod("Success", "Coupon Applied Successfully.", data => {
              console.log("Coupon success.")
              this.getCartItems();
            });
            this.couponBtnText = "Remove";
            this.couponHeadingText = "Coupon Applied";
            this.status_loading = true;
          }, (err) => {
            console.log(err);
            event.dismiss();
            this.alertService.myAlertMethod("Error", err.error.message, data => {
              console.log("error")
            });
          }, () => {
            event.dismiss();
          });
        });
        
      }
    }
  }
}