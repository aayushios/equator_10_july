import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../Service/loader.service';
import { HomepageService } from '../Service/homepage.service';
import { NavController, ModalController } from '@ionic/angular';
import { AuthService } from '../Service/auth.service'
import { ToastController } from '@ionic/angular';
import { AlerServiceService } from '../Service/aler-service.service';

import { ProfileData } from '../models/home.model';
import { empty } from 'rxjs';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  profilePageModel: ProfileData[] = [];
  name = '';
  email = '';
  UrlImage = '';
  displayName: string;
  password = '';
  cpassword ='';
  passwordType1: string = 'password';
  passwordType2: string = 'password';

  passwordIcon: string = 'Show';
  passwordIcon1: string = 'Show';

  constructor(public homePageService: HomepageService, public toastController: ToastController,private alert: AlerServiceService, private authService: AuthService, private modalController: ModalController, private loadingService: LoaderService, private navCtrl: NavController) {
    this.getProfileDetail();


   }

  ngOnInit() {
  }
  goBack() {
    this.navCtrl.back();
  }

  getProfileDetail() {
    this.loadingService.present().then(event =>{
      event.present();
      this.authService.profile().subscribe(response => {
        this.profilePageModel = response['data'];
        this.name = this.profilePageModel['name'];
        this.email = this.profilePageModel['email'];
        this.UrlImage = this.profilePageModel['user_photo_url']
        this.displayName = this.profilePageModel['display_name'];
        console.log(this.profilePageModel);
        event.dismiss();
      }, (err) => {
        console.log(err);
        this.alert.myAlertMethod(err.message, "Please try again later.", data => {
          console.log("hello alert")
        });
        event.dismiss();
      });
    });
    
  }

  hideShowPassword() {
    this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'Show' ? 'Hide' : 'Show';
  }

  hideShowPassword1() {
    this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
    this.passwordIcon1 = this.passwordIcon1 === 'Show' ? 'Hide' : 'Show';
  }

  editAction() {

    if(this.password!=="" && this.password !==null && this.cpassword!=="" && this.cpassword !==null){
      if (this.password === this.cpassword ) {
        this.userProfileUpdate(this.displayName, this.name, this.email, this.password);
        console.log("this.password",this.password);
       
      } else {
        this.presentToast('password should be same.');
     console.log("this.password",this.password);
  
    }
    }else{
      this.presentToast('Please enter the password');
    }

    

}

  userProfileUpdate(displayName, username, emailId, password) {
    this.loadingService.present().then(event =>{
      console.log(password);
      this.homePageService.updateProfile(displayName, username, emailId, password).subscribe(resp => {
        this.alert.myAlertMethod(resp.message,"",data => {
          this.goBack();
          this.modalController.dismiss();
        });
       
        event.dismiss();
      },
        (err) => {
          console.log(err)
          if (err.status == 500) {
            console.log("500 status");
            this.alert.myAlertMethod("Error", err.message, data => {
              console.log("error")
            });
          }
          event.dismiss();
        });
    });
   
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "middle",
      duration: 2000
    });
    toast.present();
  }
}
