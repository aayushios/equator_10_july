import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { concat } from 'rxjs';
import { LoaderService } from '../Service/loader.service';

@Component({
  selector: 'app-cms',
  templateUrl: './cms.page.html',
  styleUrls: ['./cms.page.scss'],
})
export class CmsPage implements OnInit {
  pageName = "";
 cmsArray = [];
 pageContent = "";
  constructor(private route: ActivatedRoute, public homepageService: HomepageService, public loadingService: LoaderService) {
    this.route.queryParams.subscribe(params => {
      this.pageName = params.title;
      this.getCmsContent();
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
   // this.getCmsContent();
  }
  getCmsContent() {
    this.loadingService.present();
    console.log(this.pageName);
    this.homepageService.cmsApiImplement().subscribe(resp => {
      this.cmsArray = resp.data;
      console.log(this.cmsArray);
      for (let i = 0; i < this.cmsArray.length; i++) {
        if (this.cmsArray[i].post_title == this.pageName) {
          this.pageContent = (this.cmsArray[i].post_content).replace(/<[^>]*>/g, '');
          console.log(this.pageContent);
          console.log(this.cmsArray[i].post_title);
        }
      }
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.loadingService.dismiss();
    }, () => {
      this.loadingService.dismiss();
    })
  }
 }

