import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, NavController } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.page.html',
  styleUrls: ['./edit-address.page.scss'],
})
export class EditAddressPage implements OnInit {
  sameaddress: boolean;
  account_bill_company_name = "";
  account_bill_adddress_line_1 = "";
  account_bill_town_or_city = "";
  account_bill_zip_or_postal_code = "";
  account_bill_select_country = "";
  account_bill_phone_number = "";
  account_bill_email_address = "";
  account_bill_first_name = "";
  account_bill_last_name = "";
  account_bill_title = '';
  account_shipping_company_name = "";
  account_shipping_adddress_line_1 = "";
  account_shipping_town_or_city = "";
  account_shipping_zip_or_postal_code = "";
  account_shipping_select_country = "";
  account_shipping_phone_number = "";
  account_shipping_email_address = "";
  account_shipping_first_name = "";
  account_shipping_last_name = "";
  account_shipping_title = '';

  account_shipping_adddress_line_11 = "";
  account_shipping_town_or_city1 = "";
  account_shipping_zip_or_postal_code1 = "";
  account_shipping_select_country1 = "";
  account_shipping_phone_number1 = "";
  account_shipping_email_address1 = "";
  account_shipping_first_name1 = "";
  account_shipping_last_name1 = "";


  countryArray = [];
  editAddress: FormGroup;
  constructor(public homePageService: HomepageService, private alertService: AlerServiceService, private formBuilder: FormBuilder, public modalController: ModalController, private loadingService: LoaderService, private navCtrl: NavController) {
    this.homePageService.getCountries().subscribe(resp => {
      this.countryArray = resp.data;
    });
    this.editAddress = this.formBuilder.group({
      account_bill_adddress_line_1: ["", Validators.required],
     // account_bill_title: [""],
     // account_bill_company_name: ["", Validators.required],
      account_bill_select_country: ["", Validators.required],
      account_bill_last_name: ["", Validators.required],
      account_bill_town_or_city: ["", Validators.required],
      account_bill_zip_or_postal_code: ["", Validators.required],
      account_bill_first_name: ["", Validators.required],
      account_bill_phone_number: ["", Validators.required],
      account_bill_email_address: ["", Validators.required],
     // account_shipping_company_name: ["", Validators.required],
      account_shipping_adddress_line_1: ["", Validators.required],
      account_shipping_town_or_city: ["", Validators.required],
      account_shipping_zip_or_postal_code: ["", Validators.required],
      account_shipping_select_country: ["", Validators.required],
      account_shipping_phone_number: ["", Validators.required],
      account_shipping_email_address: ["", Validators.required],
      account_shipping_first_name: ["", Validators.required],
      account_shipping_last_name: ["", Validators.required],
     // account_shipping_title: [""],
    });
  }

  ngOnInit() {
    this.getWishlist();
  }

  updateSameaddress() {
    console.log('Cucumbers  state:',  this.sameaddress);
    console.log('account_bill_adddress_line_1:' , this.editAddress.controls.account_bill_adddress_line_1.value);
    console.log('account_bill_adddress_line_1222:' , this.editAddress.controls.account_shipping_first_name.value);

    
    if (this.sameaddress) {

      this.account_shipping_adddress_line_11 = this.editAddress.controls.account_shipping_adddress_line_1.value;
    console.log('account_shipping_adddress_line_11:' , this.account_shipping_adddress_line_11);

    this.account_shipping_town_or_city1 = this.editAddress.controls.account_shipping_town_or_city.value;
    this.account_shipping_zip_or_postal_code1 = this.editAddress.controls.account_shipping_zip_or_postal_code.value;
    this.account_shipping_select_country1 = this.editAddress.controls.account_shipping_select_country.value;
    this.account_shipping_phone_number1 = this.editAddress.controls.account_shipping_phone_number.value;
    this.account_shipping_email_address1 = this.editAddress.controls.account_shipping_email_address.value;
    this.account_shipping_first_name1 = this.editAddress.controls.account_shipping_first_name.value;
    this.account_shipping_last_name1 = this.editAddress.controls.account_shipping_last_name.value;

     
      this.editAddress.controls.account_shipping_adddress_line_1.setValue(this.editAddress.controls.account_bill_adddress_line_1.value);
      this.editAddress.controls.account_shipping_town_or_city.setValue(this.editAddress.controls.account_bill_town_or_city.value);
      this.editAddress.controls.account_shipping_zip_or_postal_code.setValue(this.editAddress.controls.account_bill_zip_or_postal_code.value);
      this.editAddress.controls.account_shipping_select_country.setValue(this.editAddress.controls.account_bill_select_country.value);
      this.editAddress.controls.account_shipping_phone_number.setValue(this.editAddress.controls.account_bill_phone_number.value);
      this.editAddress.controls.account_shipping_email_address.setValue(this.editAddress.controls.account_bill_email_address.value);
      this.editAddress.controls.account_shipping_first_name.setValue(this.editAddress.controls.account_bill_first_name.value);
      this.editAddress.controls.account_shipping_last_name.setValue(this.editAddress.controls.account_bill_last_name.value);
      console.log('account_bill_adddress_line_1:' , this.editAddress.controls.account_bill_adddress_line_1.value);

    } else {

      this.editAddress.controls.account_shipping_adddress_line_1.setValue(this.account_shipping_adddress_line_11);
      this.editAddress.controls.account_shipping_town_or_city.setValue(this.account_shipping_town_or_city1);
      this.editAddress.controls.account_shipping_zip_or_postal_code.setValue(this.account_shipping_zip_or_postal_code1);
      this.editAddress.controls.account_shipping_select_country.setValue(this.account_shipping_select_country1);
      this.editAddress.controls.account_shipping_phone_number.setValue(this.account_shipping_phone_number1);
      this.editAddress.controls.account_shipping_email_address.setValue(this.account_shipping_email_address1);
      this.editAddress.controls.account_shipping_first_name.setValue(this.account_shipping_first_name1);
      this.editAddress.controls.account_shipping_last_name.setValue(this.account_shipping_last_name1);
      console.log('Cucumbers false state:' + this.sameaddress);
    }

  }

  getWishlist() {
    this.loadingService.present().then(event =>{
      event.present();
      this.homePageService.getWishlistProducts().subscribe(resp => {
        if (resp.data == null || resp.data.address_details == "") {
  
        } else {
          //this.editAddress.controls.account_bill_title.setValue(resp.data.address_details.account_bill_title);
          // this.editAddress.controls.account_bill_company_name.setValue(resp.data.address_details.account_bill_company_name);
          this.editAddress.controls.account_bill_adddress_line_1.setValue(resp.data.address_details.account_bill_adddress_line_1);
          this.editAddress.controls.account_bill_town_or_city.setValue(resp.data.address_details.account_bill_town_or_city);
          this.editAddress.controls.account_bill_zip_or_postal_code.setValue(resp.data.address_details.account_bill_zip_or_postal_code);
          this.editAddress.controls.account_bill_select_country.setValue(resp.data.address_details.account_bill_select_country);
          this.editAddress.controls.account_bill_phone_number.setValue(resp.data.address_details.account_bill_phone_number);
          this.editAddress.controls.account_bill_email_address.setValue(resp.data.address_details.account_bill_email_address);
          this.editAddress.controls.account_bill_first_name.setValue(resp.data.address_details.account_bill_first_name);
          this.editAddress.controls.account_bill_last_name.setValue(resp.data.address_details.account_bill_last_name);
          //this.editAddress.controls.account_shipping_company_name.setValue(resp.data.address_details.account_shipping_company_name);
          this.editAddress.controls.account_shipping_adddress_line_1.setValue(resp.data.address_details.account_shipping_adddress_line_1);
          this.editAddress.controls.account_shipping_town_or_city.setValue(resp.data.address_details.account_shipping_town_or_city);
          this.editAddress.controls.account_shipping_zip_or_postal_code.setValue(resp.data.address_details.account_shipping_zip_or_postal_code);
          this.editAddress.controls.account_shipping_select_country.setValue(resp.data.address_details.account_shipping_select_country);
          this.editAddress.controls.account_shipping_phone_number.setValue(resp.data.address_details.account_shipping_phone_number);
          this.editAddress.controls.account_shipping_email_address.setValue(resp.data.address_details.account_shipping_email_address);
          this.editAddress.controls.account_shipping_first_name.setValue(resp.data.address_details.account_shipping_first_name);
          this.editAddress.controls.account_shipping_last_name.setValue(resp.data.address_details.account_shipping_last_name);
          // this.editAddress.controls.account_shipping_title.setValue(resp.data.address_details.account_shipping_title);
        }
  
        event.dismiss();
      },
        (err) => {
          console.log("error", err);
          event.dismiss();
        });
    });
    
  }
  async closeModal() {
    const onClosedData: string = "wrapped up";
    await this.modalController.dismiss(onClosedData);
  }


  updateFirmwareList() {
    console.log('Event Called');
  }

  FilterItems(code: any) {
    this.editAddress.controls.account_bill_select_country.setValue(code);
    this.account_bill_select_country = this.editAddress.controls.account_bill_select_country.value;
    console.log("bill country2", this.account_bill_select_country)
  }

  FilterItemsShipping(code: any) {
    this.editAddress.controls.account_shipping_select_country.setValue(code);
    this.account_shipping_select_country = this.editAddress.controls.account_shipping_select_country.value;
  }
  onFormSubmit(form: NgForm) {
    console.log(form)

    this.loadingService.present().then(event =>{
      console.log(form)
      event.present();
      this.homePageService.updateAddressPost(form).subscribe(res => {
        this.alertService.myAlertMethod(res.message, "", data => {
          console.log("", data);
          this.modalController.dismiss();
        });
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
        this.alertService.myAlertMethod(err.message, "", data => {
          console.log("", data);
        });
      });
    });
    
  }

}
