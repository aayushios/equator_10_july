import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AuthService } from '../Service/auth.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { NavController } from '@ionic/angular';
import { LoaderService } from '../Service/loader.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  forgotForm: FormGroup;
  passwordType1: string = 'password';
  passwordIcon1: string = 'Show';
  passwordType2: string = 'password';
  passwordIcon2: string = 'Show';


  constructor(private formBuilder: FormBuilder,public loadingService: LoaderService,private navCtrl:NavController, private alert: AlerServiceService, private authService: AuthService) {
    this.forgotForm = this.formBuilder.group({
      forgot_email_id: ["", Validators.required],
      forgot_password: ["", Validators.required],
      forgot_repeat_password: ["", Validators.required]
    });
  }

  ngOnInit() {

  }
  
  hideShowPassword1() {
    this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
    this.passwordIcon1 = this.passwordIcon1 === 'Show' ? 'Hide' : 'Show';
  }
  hideShowPassword2() {
    this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
    this.passwordIcon2 = this.passwordIcon2 === 'Show' ? 'Hide' : 'Show';
  }

  onFormSubmit(form: NgForm) {
    if (!(form['forgot_password'] == form['forgot_repeat_password'])) {
      this.alert.myAlertMethod("Error", "Password not same", data => {
        console.log("test forgot error");
      })
    } else {
      this.loadingService.present().then(event =>{
        event.present();
        this.authService.forgot(form).subscribe(res => {
          console.log("forgot password api called successfull.")
          this.alert.myAlertMethod("Success", "Password changed successfully.", data => {
            console.log("test forgot error");
          })
          event.dismiss();
        }, (err) => {
          this.alert.myAlertMethod("Error", err.error.message, data => {
            console.log("test forgot error");
          })
          console.log("error forgot", err.error.message);
        },()=>{
          event.dismiss();
        });
      });
      
    }

  }
  goBack() {
    this.navCtrl.back();
  }
}
