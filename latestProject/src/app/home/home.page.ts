import { Component, ViewChild, HostBinding } from '@angular/core';
import { NavController, ToastController, IonSlides, Events, MenuController } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router'

import { IonicSelectableModule, IonicSelectableComponent } from 'ionic-selectable';
import { UserServiceService } from '../Service/user-service.service';
import { User } from '../models/user.model';
import { CartService } from '../Service/cart.service';
import { HomepageService } from '../Service/homepage.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { LoaderService } from '../Service/loader.service';
import { HomePageData } from '../models/home.model';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  sliderConfig = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 2,
    autoplay: false,
  };
  bannerSlider = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: true,
  };
  @HostBinding("attr.style")
  value: string = "100";
  map = new Map()
  selectedPath = '';
  textToSearch = '';
  apiUrl = "https://www.niletechinnovations.com/eq8tor";
  @ViewChild(IonSlides) slides: IonSlides;
  @ViewChild('mySelect') selectComponent: IonicSelectableComponent;
  users: User[] = [];
  homePageModel: HomePageData[] = [];
  featuredItems: any;
  latestProducts: any;
  featuredRatingArray = [];
  latestRatingArray = [];
  bannerList: any;
  public selectedCategory: any[] = [];
  dynamicColor: string
  categories: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;
  public wishListAdded: boolean = false;
  // menuArray = [];
  variationProduct = false;
  cart = [];
  totalCount = '';
  items = [];
  liked: boolean = false;
  constructor(public navCtrl: NavController, public sanitizer: DomSanitizer,public menuCtrl: MenuController, public events: Events, public loadingService: LoaderService, public toastController: ToastController, private alert: AlerServiceService, private homepageService: HomepageService, private cartService: CartService, private router: Router, private toastCtrl: ToastController, private service: UserServiceService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
      console.log("testing");
    });
    this.dynamicColor = 'redish';
    this.showCategories();
    this.menuCtrl.enable(true);
  }

  openSearchPage() {
    this.router.navigate(['searchpage']);
  }

  ngOnInit() {
    this.items = this.cartService.getProducts();
  }

  addToCart(product) {
    this.cartService.addProduct(product);
    console.log(product);
  }
  RefreshPage() {
    this.showCategories();
    this.getHomePage();
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.getCartContent();
    this.menuCtrl.enable(true);
    this.getHomePage();
  }
  openCart() {
    this.router.navigate(['cart']);
  }

  userChanged(event: { component: IonicSelectableModule, value: any }) {
    console.log('event', event);
  }

  goToProductDetail(product) {
    this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
    console.log("response product", product);
  }
  goBack() {
    this.navCtrl.back();
  }

  // getUsers() {
  //   console.log("resppppppp");
  //   this.service.getUsers()
  //     .subscribe(resp => {
  //       console.log("resppppppp", resp);
  //     }, (err) => { console.log(err) });
  // }
  like(productId, id) {
    this.loadingService.present().then(event =>{
      event.present();
      this.homepageService.addToWishlist(productId).subscribe(resp => {
        console.log(resp);
        this.presentToast(resp.message);
        this.latestProducts[id]._wishlist = true;
        event.dismiss();
    }, (err) => {
        console.log(err);
        this.presentToast(err.message);
        event.dismiss();
      }, () => {
        event.dismiss();
      });
  })
    // this.homepageService.addToWishlist(productId).subscribe(resp => {
    //   console.log(resp);
    //   this.presentToast(resp.message);
    //   this.latestProducts[id]._wishlist = true;
    //   this.loadingService.dismiss();
    // }, (err) => {
    //   console.log(err);
    //   this.presentToast(err.message);
    //   this.loadingService.dismiss();
    // }, () => {
    //   this.loadingService.dismiss();
    // });
}

  likeFeatured(productId, id) {
    this.loadingService.present();
    this.homepageService.addToWishlist(productId).subscribe(resp => {
      console.log(resp);
      this.presentToast(resp.message);
      this.featuredItems[id]._wishlist = true;
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.presentToast(err.message);
      this.loadingService.dismiss();
    }, () => {
      this.loadingService.dismiss();
    });
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  getHomePage() {
    this.loadingService.present().then(event=>{
      event.present();
      let variationLatestProductsPriceArray = [];
      this.homepageService.getHomePageData().subscribe(response => {
        this.homePageModel = response['data'];
        let checkAddress = this.homePageModel["address"];
        if (checkAddress == true){
          console.log("address Saved");
          localStorage.setItem('newUser', 'true');
        } else {
          console.log("address not Saved");
          localStorage.setItem('newUser', 'false');
        }
        this.featuredItems = this.homePageModel["features_items"];
        this.latestProducts = this.homePageModel["latest_items"];
        console.log("latestProducts", this.latestProducts);
        this.bannerList = this.homePageModel["slider"];
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
        this.alert.myAlertMethod("OOPS ! NO INTERNET Please check your network connection.", err.error.message, data => {
          console.log("hello alert")
        });
      },()=>{
        event.dismiss();
      });
    });
    
      // if (this.latestProducts.length > 0) {
       
      // }
  }

  userSearch(event) {
    //this.getUsers();
    const textSearch = event.target.value;
    this.textToSearch = textSearch;
    console.log(textSearch);
  }

  getCartContent() {
    console.log("getCartContent");

    this.homepageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent", JSON.stringify(resp));
      this.cart = resp.data.products;
      this.totalCount = resp.data.total_product;
    }, err => {
      console.log("getCartContent error", err);
    });
  }

  public showCategories() {
    this.loadingService.present().then(event =>{
      event.present();
      this.homepageService.showCategories().subscribe(resp => {
        console.log("resp", resp)
        let data = resp.data.categories;
        event.dismiss();
        this.categories = data;
      }, (err) => {
        console.log(err);
        event.dismiss();
        this.alert.myAlertMethod("OOPS ! NO INTERNET Please check your network connection.", err.error.message, data => {
          console.log("hello alert")
        });
      },()=>{
        event.dismiss();
      });
    });
    
  }

  public slideChanged(): void {
    const currentIndex = this.slides.getActiveIndex();
    // this.showLeftButton = currentIndex !=== 0;
    // this.showRightButton = currentIndex !=== Math.ceil(this.slides.length['length'] / 3);
  }

  // Method that shows the next slide
  public slideNext(): void {
    this.slides.slideNext();
  }

  // Method that shows the previous slide
  public slidePrev(): void {
    this.slides.slidePrev();
  }
  ionViewDidEnter() {
    document.removeEventListener("backbutton", function (e) {
      console.log("disable back button")
    }, true);
  }

  categoryClick(text, name) {
    console.log("hello", text);
    this.router.navigate(['subcategory'], { queryParams: { 'id': text, 'title': name }, });
  }

  ViewAllFeatured() {
    this.router.navigate(['view-all-page'], { queryParams: { 'type': 'featured' }, })

  }
  ViewAllLatest() {
    this.router.navigate(['view-all-page'], { queryParams: { 'type': 'latest' }, })

  }
  // showCategories123() {
  //   this.homepageService.showCategories().subscribe(resp => {
  //     let menuArray = [];
  //     let data = resp.data.categories;
  //     this.categories = data;
  //     for (let i = 0; i < this.categories.length; i++) {
  //       console.log(this.categories[i]);
  //       menuArray.push({
  //         "title": this.categories[i].name,
  //         "icon": "arrow-dropright"
  //       });
  //     }
  //     this.events.publish('user:created', menuArray);
  //     console.log(menuArray);
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }
}
