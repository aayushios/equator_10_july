import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.page.html',
  styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage implements OnInit {
  @ViewChild('slider', { read: ElementRef })slider: ElementRef;
  img: any;
  apiUrl = "https://www.niletechinnovations.com/eq8tor"

  sliderOpts = {
    zoom: {
      maxRatio: 5,lockSwipes:false
    }
  };

  constructor(private modalController: ModalController,private navParams: NavParams) { 
  }

  ngOnInit() {
    this.img = this.navParams.get('img');
  }
  zoom(zoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }

  close() {
    this.modalController.dismiss();
  }

}
