import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
  } from '@angular/common/http';
  import {Storage} from '@ionic/storage'
  import { Observable, throwError } from 'rxjs';
  import { map, catchError } from 'rxjs/operators';
  import {
    Router
  } from '@angular/router';
  import { ToastController, LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';

  @Injectable()

  export class TokenInterceptor implements HttpInterceptor {
    loaderToShow: any;
    constructor(private router: Router,
        public toastController: ToastController,private storage:Storage,public loadingController: LoadingController) {

        }

        intercept(request:HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {
            const token = localStorage.getItem('access_token');
            if (token){
                request = request.clone({
                    setHeaders:{'Authorization': 'Bearer '+ token }
                });
            }
            if (!request.headers.has('Content-Type')) {
                request = request.clone({
                    setHeaders: {
                        'content-type': 'application/json'
                    }
                });
            }
            request = request.clone({
                headers: request.headers.set('Accept','application/json')
            });
            //this.showLoader();
            return next.handle(request).pipe(map((event:HttpEvent<any>)=>{
                if (event instanceof HttpResponse){
                    console.log('event --->',event);
                }
                //this.hideLoader();
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                  if (error.error.success === false) {
                    this.presentToast('Login failed');
                  } else {
                    this.router.navigate(['login']);
                  }
                }
                //this.hideLoader();
                return throwError(error);
              }));
        }
        async presentToast(msg) {
            const toast = await this.toastController.create({
              message: msg,
              duration: 2000,
              position: 'top'
            });
            toast.present();
          }
          showLoader() {
            this.loaderToShow = this.loadingController.create({
              message: 'Processing Server Request'
            }).then((res) => {
              res.present();
        
              res.onDidDismiss().then((dis) => {
                console.log('Loading dismissed!');
                
              });
              //this.hideLoader();
            });
            //this.hideLoader();
          }
        
          hideLoader() {
              this.loadingController.dismiss();
          }
        
  }