import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { AlerServiceService } from '../Service/aler-service.service'
import { UserServiceService } from '../Service/user-service.service';
import { AuthService } from '../Service/auth.service';
import { LoaderService } from '../Service/loader.service';
import { Observable ,BehaviorSubject, Subscription} from 'rxjs';
import { Storage } from '@ionic/storage'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  passwordType1: string = 'password';
  passwordIcon: string = 'Show';
  credentialsForm: FormGroup;
  isLoading = false;
  authState$:  Subscription 

  constructor(private authService: AuthService
    , private userService: UserServiceService,private storage:Storage, private navCtrl: NavController, private alert: AlerServiceService, public loadingService: LoaderService, public alertController: AlertController, private router: Router, private formBuilder: FormBuilder) {
    this.credentialsForm = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  hideShowPassword() {
    this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'Show' ? 'Hide' : 'Show';
  }

  ngOnInit() {
    // this.authState$ = this.authService.getAuthStateObserver().subscribe();

  }

  goToSignUp() {
    console.log("goToSignUp")
    this.router.navigateByUrl('signup');
  }

  onFormSubmit(form: NgForm) {
    this.loadingService.present().then(event=>{
      event.present();
      this.authService.login(form)
      .subscribe(res => {
        if (res.data.access_token) {
          localStorage.setItem('access_token', res.data.access_token);
          // this.storage.set('access_token',res.data.access_token);
          // this.authService.checkToken();
          this.router.navigate(['home']);
          event.dismiss();
        }
      }, (err) => {
        console.log(err);
        this.alert.myAlertMethod("Error", err.error.message, data => {
          console.log("error");
        });
        event.dismiss();
      },()=> {
        event.dismiss();
      });
    });
    // this.router.navigate(['home']);
    
  }

  forgotPassword() {
    // this.alertService.myAlertMethod("test alert","test message",data => {
    //   console.log("",data);
    // })
  }
  async presentAlertPrompt() {
    // const alert = await this.alertController.create({
    //   header: 'Prompt!',
    //   inputs: [
    //     {
    //       name: 'name1',
    //       type: 'text',
    //       placeholder: 'Placeholder 1'
    //     },
    //     {
    //       name: 'name2',
    //       type: 'text',
    //       id: 'name2-id',
    //       value: 'hello',
    //       placeholder: 'Placeholder 2'
    //     },
    //     {
    //       name: 'name3',
    //       value: 'http://ionicframework.com',
    //       type: 'url',
    //       placeholder: 'Favorite site ever'
    //     },
    //     // input date with min & max
    //     {
    //       name: 'name4',
    //       type: 'date',
    //       min: '2017-03-01',
    //       max: '2018-01-12'
    //     },
    //     // input date without min nor max
    //     {
    //       name: 'name5',
    //       type: 'date'
    //     },
    //     {
    //       name: 'name6',
    //       type: 'number',
    //       min: -5,
    //       max: 10
    //     },
    //     {
    //       name: 'name7',
    //       type: 'number'
    //     }
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       cssClass: 'secondary',
    //       handler: () => {
    //         console.log('Confirm Cancel');
    //       }
    //     }, {
    //       text: 'Ok',
    //       handler: data => {
    //         console.log('Confirm Ok');
    //         console.log("hello",data.name1);
    //       }
    //     }
    //   ]
    // });

    // await alert.present();
    this.router.navigate(['forgotpassword']);

  }
  goBack() {
    this.navCtrl.back();
  }
}
