import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, RouterEvent } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { NavController, ModalController, NavParams, AlertController, MenuController, IonLabel } from '@ionic/angular';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  productID: string;
  processID: string;
  data: any;
  buttonTextCancel = "";
  buttonTextReturn = "";
  hideMeCancel: boolean;
  hideMeReturn: boolean;
  reload: boolean = false;
  constructor(private route: ActivatedRoute, private alert: AlerServiceService, public alertController: AlertController, public menuCtrl: MenuController, private navParams: NavParams, public modalController: ModalController, public navCtrl: NavController, private loadingService: LoaderService, private homePageService: HomepageService) {
    this.route.queryParams.subscribe((event: RouterEvent) => {
      console.log(event);
    });
    //this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.productID = this.navParams.data.id;
    this.processID = this.navParams.data.process;
    this.getOrderDetails()
  }
  goBack() {
    this.navCtrl.back();
  }
  getOrderDetails() {
    this.loadingService.present().then(event => {
      event.present();
      this.homePageService.viewOrderDetails(this.processID, this.productID).subscribe(resp => {
        this.data = resp.data.order_details_by_order_id;
        if (this.data._order_status == 'cancelled') {
          this.buttonTextCancel = "Order Cancelled.";
        } else if (this.data._order_status == 'refunded') {
          this.buttonTextReturn = "Order Return Applied.";
        }
        if (this.data._order_status == 'on-hold' || this.data._order_status === 'processing' || this.data._order_status === 'pending') {
          this.hideMeCancel = false;
          this.hideMeReturn = true;
        } else if (this.data._order_status === 'completed') {
          this.hideMeReturn = false;
          this.hideMeCancel = true;
        }
        console.log(this.data);
        event.dismiss();
      }, (err) => {
        console.log("order details", err);
        event.dismiss();
      }, () => {
        event.dismiss();
      });
    });

  }
  async closeModal() {
    const onClosedData: string = "wrapped up";
    await this.modalController.dismiss(this.reload);
  }
  orderCancelledResponse: any = ""
  orderReturnResponse: any = ""
  async CancelAction(orderId) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure to cancel this order?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.loadingService.present().then(event => {
              event.present();
              console.log('Confirm Okay');
              this.homePageService.cancelReturnOrder(orderId, 'cancelled').subscribe(resp => {
                console.log(resp);
                this.orderCancelledResponse = "Order Cancelled";
                this.hideMeCancel = true;
                this.hideMeReturn = true;
                this.buttonTextReturn = "";
                this.reload = true;
                event.dismiss();
                this.alert.myAlertMethod("Success", "Order Cancelled Successfully.", data => {
                  this.orderCancelledResponse = "Order Cancelled";
                });
              }, (err) => {
                event.dismiss();
                this.alert.myAlertMethod("Success", err.error.message, data => {
                  console.log(err);
                });
              }, () => {
                event.dismiss();
              });
            });

          }
        }
      ]
    });

    await alert.present();

  }

  async ReturnAction(orderId) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure to return this order?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.loadingService.present().then(event => {
              event.present();
              this.homePageService.cancelReturnOrder(orderId, 'refunded').subscribe(resp => {
                console.log(resp);
                this.orderReturnResponse = "Order Return Applied.";
                this.buttonTextReturn = "";
                this.hideMeCancel = true;
                this.hideMeReturn = true;
                this.reload = true;
                event.dismiss();
                this.alert.myAlertMethod("Success", "Order Return Applied Successfully.", data => {
                  this.orderReturnResponse = "Order Return Applied.";
                });
              }, (err) => {
                event.dismiss();
                this.alert.myAlertMethod("Success", err.error.message, data => {
                  console.log(err);
                });
              }, () => {
                event.dismiss();
              });
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
