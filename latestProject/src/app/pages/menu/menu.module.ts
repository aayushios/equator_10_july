import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';
import { CategoriesPageModule } from '../categories/categories.module';
import { WishlistPageModule } from '../wishlist/wishlist.module';
import { HomePage } from '../../home/home.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriesPageModule,
    WishlistPageModule
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
