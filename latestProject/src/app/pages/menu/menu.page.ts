import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  selectedPath = '';
showSubMenu:boolean = false;
  // pages = [
  //   {
  //     title: 'category',
  //     url: '../menu/(menucontent:categories)'
  //   }, {
  //     title: 'wishlist',
  //     url: '../menu/(menucontent:wishlist)'
  //   }
  // ]
  constructor(private router:Router) {
    this.router.events.subscribe((event:RouterEvent)=>{
      this.selectedPath = event.url;
    });
   }

  ngOnInit() {
  }
menuItemHandler():void {
  this.showSubMenu = !this.showSubMenu;
}
}
