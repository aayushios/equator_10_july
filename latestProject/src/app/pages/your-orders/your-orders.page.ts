import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../../Service/homepage.service';
import { LoaderService } from '../../Service/loader.service';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { OrderDetailsPage } from '../../order-details/order-details.page';

@Component({
  selector: 'app-your-orders',
  templateUrl: './your-orders.page.html',
  styleUrls: ['./your-orders.page.scss'],
})
export class YourOrdersPage implements OnInit {

  ordersArray: any;
  totalCount = '';
  cart = [];
  constructor(private homepageService: HomepageService,private menuCtrl:MenuController, private router: Router, private navCtrl: NavController, private loadingService: LoaderService,private modalController:ModalController) {
  // this.menuCtrl.enable(true);
  }

  ngOnInit() {
    this.getCartContent();
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.getAllOrders();
  }

  async viewOrderDetails(order) {
    const modal = await this.modalController.create({
      component: OrderDetailsPage,
      componentProps: {
        "process": order._order_process_key,
        "id": order._post_id
      }
    });
    
    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null ) {
        if (sizeChartResponse.data == true){
          this.getAllOrders();
          console.log(sizeChartResponse);
        }
      }
    });
    return await modal.present();
  }
  openCart() {
    this.router.navigate(['cart']);
  }
  getCartContent() {
    this.homepageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent", JSON.stringify(resp));
      this.cart = resp.data.products;
      this.totalCount = resp.data.total_product;
    }, err => {
      console.log("getCartContent error", err);
    });
  }
  getAllOrders() {
    this.loadingService.present().then(event =>{
      event.present();
      this.homepageService.getAllOrders().subscribe(resp => {
        this.ordersArray = resp.data.orders_list_data;
        console.log(this.ordersArray);
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
      },()=>{
        event.dismiss();
      });
    });
   
  }
  goBack() {
    this.navCtrl.back();
  }
}
