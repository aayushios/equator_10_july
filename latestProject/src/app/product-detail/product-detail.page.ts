import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { NavController, IonSlides, ModalController, Events } from '@ionic/angular';
import { SizechartModalPage } from '../sizechart-modal/sizechart-modal.page';
import { CartService } from '../Service/cart.service';
import { ToastController } from '@ionic/angular';
import { LoaderService } from '../Service/loader.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { RatingModalPage } from '../rating-modal/rating-modal.page';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  bannerSlider = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: true,
  };
  productType: any = '';
  totalRating: any;
  averageRating: any;
  manageStock = '';
  isSelected: boolean;
  cartAdded: boolean;
  @ViewChild(IonSlides) slides: IonSlides;
  private selecteTextId: string;
  detailsContent: any;
  detailImageList: any;
  sizeChartResponse: any;
  qty: any;
  postType = '';
  sizeNumberArray = [];
  productQuantityAvailable: any;
  variationProductQuantityAvailable: any;
  checkStockOfProduct = '';
  apiUrl = "https://www.niletechinnovations.com/eq8tor"
  sizeArray = [];
  colorArray = [];
  variationData = [];
  priceDisplay = '';
  variationID = '';
  selectedColor = '';
  selectedSize = '';
  variationPrice = '';
  jsonDataVariation = [];
  variationImage: any;
  buttonClick = false;
  variationProductStock = false;
  sizeData: any;
  constructor(private alert: AlerServiceService, private events: Events, public loadingService: LoaderService, private route: ActivatedRoute, public toastController: ToastController, private cartService: CartService, private homepageService: HomepageService, private navCtrl: NavController, public modalController: ModalController) {
    this.route.queryParams.subscribe(params => {
      console.log("params", params);
      let productName = params.id;
      console.log("product name", productName);
      this.getProductDetail(productName);
    });
    this.qty = 1;
  }

  ngOnInit() {
    this.productType = '';
    this.sizeArray = [];
    this.colorArray = [];
    this.variationData = [];
    this.variationID = '';
    this.selectedColor = '';
    this.selectedSize = '';
    this.variationPrice = '';
    this.jsonDataVariation = [];
    this.sizeNumberArray = [];
    console.log('size number array in ngoninit', this.sizeNumberArray)
  }
  viewWillAppear() {
    console.log('viewWillAppear');
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.cartAdded = false;
    this.variationProductStock = false;
  }

  openPreview(img) {
    this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img: img
      }
    }).then(modal => {
      modal.present();
    });
  }
  getProductDetail(product) {
    this.sizeArray = [];
    console.log('this.sizeArray', this.sizeArray);
    this.colorArray = [];
    this.productType = '';
    this.loadingService.present().then(event =>{
      event.present();
      this.homepageService.productDetail(product).subscribe(response => {
        let resp = response.data.single_product_details;
        this.sizeData = response.data.size_chart;
        localStorage.setItem('sizedata',this.sizeData);
        this.postType = resp.post_type;
        this.manageStock = resp['_product_manage_stock'];
        if (this.manageStock === 'yes') {
          this.productQuantityAvailable = resp.post_stock_qty;
        }
        this.checkStockOfProduct = resp.post_stock_availability;
        if (this.checkStockOfProduct == 'in_stock') {
          this.checkStockOfProduct = 'In Stock';
        }
        let dataChoice = response.data.variations_data;
  
        this.totalRating = response.data._rating.total;
        this.averageRating = response.data._rating.average;
        console.log("total rating", this.totalRating);
        if (dataChoice.length > 0) {
          this.jsonDataVariation = response.data.variations_data;
          this.priceDisplay = dataChoice[0]._variation_post_regular_price;
          for (let i = 0; i < this.jsonDataVariation.length; i++) {
            this.variationData = this.jsonDataVariation[i]['_variation_array_data'];
            this.productType = 'variation';
            console.log('variationData', this.variationData);
            let number = this.variationData[1]['attr_val'];
            let color = this.variationData[0]['attr_val'];
            this.sizeArray.push({
              "size": number,
              "color": color,
              "isSelected": false
            });
            this.colorArray.push({
              "color": color,
              "isSelected": false
            });
          }
          this.colorArray = this.removeDuplicates(this.colorArray, "color");
          this.sizeArray = this.removeDuplicates(this.sizeArray, "size");
          console.log("testArray", this.colorArray);
          console.log("size array", this.sizeArray);
        }
        this.detailsContent = resp;
        this.variationPrice = this.detailsContent.post_price;
        this.detailImageList = resp._product_related_images_url.product_gallery_images;
        event.dismiss();
      }, (err) => {
        console.log(err);
        event.dismiss();
        this.alert.myAlertMethod("OOPS ! NO INTERNET Please check your network connection.", err.error.message, data => {
          console.log("hello alert")
        });
      });
    });
    

  }

  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }


  btnSize(ind) {
    for (let i = 0; i < this.sizeArray.length; i++) {
      this.sizeArray[i].isSelected = false;
    }
    this.sizeArray[ind].isSelected = true;
    console.log('', this.sizeArray[ind].size);
    console.log(this.colorArray);
    this.selectedSize = this.sizeArray[ind].size;
    if ((this.selectedSize == '' && this.selectedColor == '') || (this.selectedSize == '') || (this.selectedColor == '')) {
      this.presentToast('Please select Color.')
    } else {
      this.filterData();
      this.qty = 1;
    }
  }

  btnColor(ind) {
    for (let i = 0; i < this.colorArray.length; i++) {
      this.colorArray[i].isSelected = false;
    }
    this.colorArray[ind].isSelected = true;
    console.log('', this.colorArray[ind].color);
    this.selectedColor = this.colorArray[ind].color
    if ((this.selectedSize == '' && this.selectedColor == '') || (this.selectedSize == '') || (this.selectedColor == '')) {
      this.presentToast('Please select size.')
    } else {
      this.filterData();
      this.qty = 1;
    }
  }

  filterData() {
    console.log(this.selectedColor, this.selectedSize);
    for (let i = 0; i < this.jsonDataVariation.length; i++) {
      let filterData = this.jsonDataVariation[i]['_variation_array_data'];
      let number = filterData[1]['attr_val'];
      let color = filterData[0]['attr_val'];
      if (number == this.selectedSize && color == this.selectedColor) {
        let filteredData = filterData;
        console.log("filteredData", filteredData);
        console.log('', this.jsonDataVariation[i]);
        let salePrice = this.jsonDataVariation[i]['_variation_post_sale_price'];
        if (salePrice == '0') {
          this.variationPrice = this.jsonDataVariation[i]['_variation_post_regular_price'];
          console.log("variation price", this.variationPrice);
        } else {
          this.variationPrice = salePrice;
          console.log("sale Price", salePrice);
        }
        this.variationID = this.jsonDataVariation[i].id;
        console.log(this.variationID);
        this.variationProductQuantityAvailable = this.jsonDataVariation[i]['_variation_post_manage_stock_qty'];
        console.log("this.variationProductQuantityAvailable", this.variationProductQuantityAvailable);
        if (this.variationProductQuantityAvailable == 0) {
          this.variationProductStock = true;
        }
        document.getElementById('changePrice').innerText = "$" + this.variationPrice;
        //console.log((document.getElementById('variationImage') as HTMLImageElement).src);
        console.log('this.jsonDataVariation[i]', this.jsonDataVariation[i]['_variation_post_img_url']);
        this.variationImage = this.jsonDataVariation[i]['_variation_post_img_url'];
        this.variationImage = document.getElementById('variationImage') as HTMLImageElement;
      }
    }
  }

  goBack() {
    this.navCtrl.back();
  }

  getCartContent() {
    console.log("getCartContent");
    this.homepageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent", JSON.stringify(resp));
    }, err => {
      console.log("getCartContent error", err);
      this.loadingService.dismiss();
    });
  }

  async openSizeChart() {
    const modal = await this.modalController.create({
      component: SizechartModalPage,
      componentProps: {
        "testParam": 123,
        "testurl": "https://www.google.com"
      }
    });
    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        this.sizeChartResponse = sizeChartResponse.data;
      }
    });
    return await modal.present();
  }

  incrementQty() {
    if (this.manageStock == 'yes') {
      if (this.qty < this.productQuantityAvailable) {
        this.qty += 1;
      }
      if (this.qty == this.productQuantityAvailable) {
        this.presentToast('Currently  we have only' + this.productQuantityAvailable + ' items in the list.');
      }
    }
    else if (this.productType == 'variation') {
      if (this.selectedColor == '' || this.selectedSize == '') {
        this.presentToast('please select the color and size both')
        console.log('product type variation');
      }
      if (this.qty < this.variationProductQuantityAvailable) {
        this.qty += 1;
      } if (this.qty == this.variationProductQuantityAvailable) {
        this.presentToast('Currently  we have only ' + this.variationProductQuantityAvailable + ' items in the list.');
      } if (this.qty > this.variationProductQuantityAvailable) {
        this.presentToast('product out of stock.');
      }

    } else {
      this.qty += 1;
    }

  }

  async openRatingModal(id) {
    const modal = await this.modalController.create({
      component: RatingModalPage,
      componentProps: {
        "idProduct": id
      }
    });
    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        this.sizeChartResponse = sizeChartResponse.data;
      }
    });
    return await modal.present();
  }

  decrementQty() {
    if (this.qty - 1 < 1) {
      this.qty = 1
      console.log('1->' + this.qty);
    } else {
      this.qty -= 1;
      console.log('2->' + this.qty);
    }
  }


  addToCartVariation(productID, Quantity) {
    if (this.selectedColor == '' || this.selectedColor == null) {
      this.presentToast('Please select the color');
    } else if (this.selectedSize == '' || this.selectedSize == null) {
      this.presentToast('Please select the size');
    }
    else {
      this.buttonClick = true;
      this.homepageService.addToCart(productID, Quantity, this.variationID, this.selectedColor, this.selectedSize).subscribe(resp => {
        console.log("response add to cart", resp);
        this.presentToast('Product Added to cart.');
        this.cartAdded = true;
        this.buttonClick = false;
      }, (err) => {
        console.log("error add to cart", err);
        this.presentToast('error add to cart.');
        this.buttonClick = false;
      })
    }
  }

  addToCart(productID, Quantity) {
    this.buttonClick = true;
    this.homepageService.addToCart(productID, Quantity, '', '', '').subscribe(resp => {
      console.log("response add to cart", resp);
      this.presentToast('Product Added to cart.');
      this.cartAdded = true;
      this.buttonClick = false;
    }, (err) => {
      console.log("error add to cart", err);
      this.presentToast('error add to cart.');
      this.buttonClick = false;
    })
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "middle",
      duration: 2000
    });
    toast.present();
  }

}

