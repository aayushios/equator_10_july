import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, IonSlides } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router'
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { IonicSelectableModule, IonicSelectableComponent } from 'ionic-selectable';
import { UserServiceService } from '../Service/user-service.service';
import { User } from '../models/user.model';
import { CartService } from '../Service/cart.service';
import { AuthService } from '../Service/auth.service'
import { AlerServiceService } from '../Service/aler-service.service';
import { LoaderService } from '../Service/loader.service';
import { ProfileData } from '../models/home.model';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { HomepageService } from '../Service/homepage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  apiUrl = 'https://www.niletechinnovations.com/eq8tor';
  selectedPath = '';
  dynamicColor: string
  name = '';
  email = '';
  UrlImage = '';
  displayName: string;
  password = '';
  options: any;
  imageResponse: any;
  isReadOnly = true;
  editButton = "Edit"
  cancelBtn = "Cancel"
  profilePageModel: ProfileData[] = [];
  edited: boolean;
  fileUrl: any = null;
  respData: any;
  totalCount = '';
  cart = [];
  constructor(public navCtrl: NavController, private crop: Crop,
    private transfer: FileTransfer, private homePageService: HomepageService, private imagePicker: ImagePicker, public loadingService: LoaderService, private alert: AlerServiceService, private authService: AuthService, private router: Router, private toastCtrl: ToastController) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
    this.dynamicColor = 'redish';
    this.getProfileDetail();
  }

  ngOnInit() {
    console.log('ng init call')
    this.getCartContent();
  }


  getProfileDetail() {
    this.loadingService.present().then(event =>{
      event.present();
      this.authService.profile().subscribe(response => {
        this.profilePageModel = response['data'];
        this.name = this.profilePageModel['name'];
        this.email = this.profilePageModel['email'];
        this.UrlImage = this.profilePageModel['user_photo_url']
        this.displayName = this.profilePageModel['display_name'];
        console.log(this.profilePageModel);
        event.dismiss();
      }, (err) => {
        console.log(err);
        this.alert.myAlertMethod(err.message, "Please try again later.", data => {
          console.log("hello alert")
        });
        event.dismiss();
      });
    });
    
  }
  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }
  openCart() {
    this.router.navigate(['cart']);
  }
  // cropUpload() {
  //   this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then((results) => {
  //     for (let i = 0; i < results.length; i++) {
  //         console.log('Image URI: ' + results[i]);
  //         this.crop.crop(results[i], { quality: 100 })
  //           .then(
  //             newImage => {
  //               console.log('new image path is: ' + newImage);
  //               this.fileUrl = newImage;
  //               const fileTransfer: FileTransferObject = this.transfer.create();
  //               const uploadOpts: FileUploadOptions = {
  //                  fileKey: 'file',
  //                  fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
  //               };

  //               fileTransfer.upload(newImage, 'http://192.168.0.7:3000/api/upload', uploadOpts)
  //                .then((data) => {
  //                  console.log(data);
  //                  this.respData = JSON.parse(data.response);
  //                  console.log(this.respData);
  //                  this.fileUrl = this.respData.fileUrl;
  //                  console.log("this.file url", this.fileUrl);
  //                }, (err) => {
  //                  console.log(err);
  //                });
  //             },
  //             error => console.error('Error cropping image', error)
  //           );
  //     }
  //   }, (err) => { console.log("errorrorororo",err); });
  // }
 
  editAction() {
    if (this.editButton === "Edit") {
      this.editButton = "Save"
      this.edited = true;
    } else {
      this.editButton = "Edit"
      this.edited = false;
      console.log("this.password",this.password);
      this.userProfileUpdate(this.displayName, this.name, this.email, this.password);
    }
  }

  userProfileUpdate(displayName, username, emailId, password) {
    this.loadingService.present().then(event =>{
      event.present();
      console.log(password);
    this.homePageService.updateProfile(displayName, username, emailId, password).subscribe(resp => {
      event.dismiss();
    },
      (err) => {
        console.log(err)
        if (err.status == 500) {
          console.log("500 status");
          this.alert.myAlertMethod("Error", err.message, data => {
            console.log("error")
          });
        }
        event.dismiss();
      });
    });
    
  }
  openAddress() {
    this.router.navigate(['address']);
  }
  openChangePassword(){
    this.router.navigate(['change-password']);

  }
  cancelAction(){
      this.edited = false;   
      this.editButton = "Edit"
  }
  openOrders(){
    this.router.navigate(['your-orders']);
  }
  getCartContent() {
    console.log("getCartContent");
    this.homePageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent",JSON.stringify(resp));
      this.cart = resp.data.products;
      this.totalCount = resp.data.total_product ;
    }, err => {
      console.log("getCartContent error",err);
    });
  }

}