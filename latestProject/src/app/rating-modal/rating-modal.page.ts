import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-rating-modal',
  templateUrl: './rating-modal.page.html',
  styleUrls: ['./rating-modal.page.scss'],
})

export class RatingModalPage implements OnInit {
  rate: any;
  Review = '';
  productId: any;
  constructor(private modalController: ModalController, public loadingService: LoaderService, private homePageService: HomepageService, private navParams: NavParams,private alertService: AlerServiceService) {

  }

  ngOnInit() {
    this.productId = this.navParams.data.idProduct;
    console.log(this.productId);
  }
  async closeModal() {
    const onClosedData: string = "wrapped up";
    await this.modalController.dismiss(onClosedData);
  }

  onModelChange(ratingValue) {
    console.log(ratingValue);
    this.rate = ratingValue;
    console.log(this.Review);
  }

  submitRating() {
    this.loadingService.present().then(event =>{
      event.present();
      this.homePageService.submitRating(this.rate, this.Review, this.productId).subscribe(resp => {
        console.log(resp);
        this.alertService.myAlertMethod("Success", resp.message, data => {
          console.log("resp.message",resp.message)
          this.modalController.dismiss();
        });
        event.dismiss();
      }, (err) => {
        console.log(err)
        event.dismiss();
      });
    });
    
  }

}
