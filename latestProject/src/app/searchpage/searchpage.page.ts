import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router'
import { UserServiceService } from '../Service/user-service.service';
import { User } from '../models/user.model';
import { NavController } from '@ionic/angular';
import { IonInfiniteScroll } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { Results } from '../models/search.model';

@Component({
  selector: 'app-searchpage',
  templateUrl: './searchpage.page.html',
  styleUrls: ['./searchpage.page.scss'],
})
export class SearchpagePage implements OnInit {
  textToSearch = '';
  //productsArray: Results[] = [];
  productsArray = [];
  selectedPath = '';
  dynamicColor: string;
  nextUrl: string;
  totalNumberProducts: any;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(private router: Router, public navCtrl: NavController, private service: UserServiceService, private homeService: HomepageService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
    this.dynamicColor = 'redish';
    this.getSearchData('');
  }

  loadData(event, textSearch) {
    setTimeout(() => {
      console.log('Done');
      this.homeService.loadMoreData(textSearch).subscribe(resp => {
        let moredata = resp;
        console.log(moredata);
        let products = moredata.data.results.data;
        this.nextUrl = moredata.data.results.next_page_url;
        this.totalNumberProducts = moredata.data.results.total;
        console.log(this.totalNumberProducts);
        products.forEach(element => {
          this.productsArray.push(element)
        });;
        console.log(this.productsArray);
      })
      event.target.complete();
      console.log(this.totalNumberProducts.length);

      if (this.productsArray.length == this.totalNumberProducts) {
        // event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    //this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
  ngOnInit() {
  }
  userSearch(event) {
    //this.getUsers();
    const textSearch = event.target.value;
    this.textToSearch = textSearch;
    console.log(textSearch);
  }

  getUsers() {
    // this.service.getUsers()
    //   .subscribe(resp => { this.users = resp });
  }
  goBack() {
    this.navCtrl.back();
  }
  getSearchData(searchText) {
    this.homeService.searchApi(searchText).subscribe(resp => {
      let dataReceived = resp;
      console.log(dataReceived);

      let products = dataReceived.data.results.data;
      this.nextUrl = dataReceived.data.results.next_page_url;
      products.forEach(element => {
        this.productsArray.push(element)
      });;
      console.log(this.productsArray);
    });
  }
  goToDetail(data) {
    console.log(data.slug);
    let title = data.slug;
    this.router.navigate(['product-detail'], { queryParams: { 'id': title } });
  }
}
