import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizechartModalPage } from './sizechart-modal.page';

describe('SizechartModalPage', () => {
  let component: SizechartModalPage;
  let fixture: ComponentFixture<SizechartModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizechartModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizechartModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
