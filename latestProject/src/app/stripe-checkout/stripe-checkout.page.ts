import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Stripe } from '@ionic-native/stripe/ngx';
import { HomepageService } from '../Service/homepage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlerServiceService } from '../Service/aler-service.service';
import { LoaderService } from '../Service/loader.service';

@Component({
  selector: 'app-stripe-checkout',
  templateUrl: './stripe-checkout.page.html',
  styleUrls: ['./stripe-checkout.page.scss'],
})
export class StripeCheckoutPage implements OnInit {
  cardNumber: string = "";
  Month: number;
  Year: number;
  cvv: string;
  cardDetails: FormGroup;
  totalAmount = "";
  membershipDiscount = "";
  isMember = "";
  totalTax = "";
  disableButton = false;
  shippingCost = "";
  constructor(public navCtrl: NavController,private router: Router,public menuCtrl: MenuController, public loadingService: LoaderService, private route: ActivatedRoute, private alert: AlerServiceService, private homePageService: HomepageService, private formBuilder: FormBuilder, private stripe: Stripe) {
    // angular.module('Eq8tor', [
    //   require('angular-credit-cards')
    // ]);
    this.route.queryParams.subscribe(params => {
      console.log("params", params);
      this.totalAmount = params.total_amount;
      this.membershipDiscount = params.membership_discount;
      this.totalTax = params.total_tax;
      this.isMember = params.is_member;
      this.shippingCost = params.shipping_cost;
    });
    this.stripe.setPublishableKey('pk_test_4sjCZIFhfIeMDj3bpJsFapZf');
    this.cardDetails = this.formBuilder.group({
      cardNumber: ["", Validators.required],
      Month: ["", Validators.required],
      Year: ["", Validators.required],
      cvv: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.disableButton = false;
  }
  sendDetails(form: NgForm) {
    console.log(form)
    if (form["cardNumber"] == "" || form["Month"] == "" || form["Year"] == "" || form["cvv"] == ""){
      this.alert.myAlertMethod("Please enter Mandatory Field.",'', data =>{
        console.log("data",data)
      })
      return;
    }
    console.log("hr=e",this.cardNumber, this.Month, this.Year, this.cvv);
    let card =
    {
      number: form["cardNumber"],
      expMonth: form["Month"],
      expYear: form["Year"],
      cvc: form["cvv"]
    }
    console.log(card);
    this.disableButton = true;
    this.stripe.createCardToken(card)
      .then(token => {
        console.log("token received", token.id);
        this.loadingService.present().then(event =>{
          this.homePageService.stripeCheckoutApi('stripe', token.id, this.totalAmount, this.totalTax, this.isMember, this.membershipDiscount,this.shippingCost).subscribe(resp => {
            console.log("stripe response", JSON.stringify(resp));
            event.dismiss();
            if (resp.success == true) {
              this.alert.myAlertMethod("Successfully", resp.message, data => {
                console.log("data",data)
                this.router.navigate(['your-orders']);
              });
            } else {
              this.alert.myAlertMethod("Something went wrong. Please try again after sometime.", resp.message, data => {
                console.log("Something went wrong.")
              });
            }
          }, (err) => {
            this.alert.myAlertMethod("error", err.message, data => {
              console.log("error")
            });
            console.log(err);
            event.dismiss();
          });
        });
        
      })
      .catch(error => console.error(error));
  }

  goBack() {
    this.navCtrl.back();
  }
  
}
