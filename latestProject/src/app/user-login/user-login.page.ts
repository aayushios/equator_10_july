import { Component, OnInit } from '@angular/core';
import {AuthService} from '../Service/auth.service';
import { Observable,Subscription } from 'rxjs';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.page.html',
  styleUrls: ['./user-login.page.scss'],
})
export class UserLoginPage implements OnInit {
  authState$:  Subscription
  constructor(private authService: AuthService,public menuCtrl: MenuController) {
    // this.menuCtrl.enable(false);
   }
   ionViewWillEnter() {
     this.menuCtrl.enable(false);
  }
  ngOnInit() {
    // this.authState$ = this.authService.getAuthStateObserver().subscribe();
  }
}
