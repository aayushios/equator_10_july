import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../Service/homepage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../Service/loader.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-view-all-page',
  templateUrl: './view-all-page.page.html',
  styleUrls: ['./view-all-page.page.scss'],
})
export class ViewAllPagePage implements OnInit {
productType ='';
allProductsArray = [];
title = '';
  constructor(private router: Router,private homepageService: HomepageService,public navCtrl: NavController,private route:ActivatedRoute,public loadingService:LoaderService) {
    this.route.queryParams.subscribe(params => {
      console.log(params.type);
      this.productType = params.type;
      if(this.productType == 'featured'){
        this.title = 'All Featured Products';
      } else if (this.productType == 'latest'){
        this.title = 'All Latest Products';
      }
      this.getAllFeaturedProducts(this.productType)
    });
  }

  ngOnInit() {
    
  }

  getAllFeaturedProducts(productType) {
    this.loadingService.present();
    this.homepageService.getAllProducts(productType).subscribe(resp => {
      this.allProductsArray = resp.data.all
      console.log("view all", this.allProductsArray);
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.loadingService.dismiss();
    });
  }
  goBack(){
    this.navCtrl.back();
  }

  goToProductDetail(product) {
    this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
    console.log("response product", product);
  }
}
