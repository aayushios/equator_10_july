(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");







var routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());



/***/ }),

/***/ "./src/app/profile/profile.page.html":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Profile</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button>\n        <ion-badge class=\"badge-color\" *ngIf=\"cart.length > 0\">{{ totalCount }}</ion-badge>\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"profile-content\">\n  <div class=\"profile-avatar-info\">\n     <ion-card class=\"profile-card\">\n      <ion-card-content class=\"profile-card-content\">\n        <div class=\"profile\" *ngIf=\"!edited\">\n          <!-- <ion-avatar class=\"profile-media\">\n            <img src=\"{{apiUrl}}{{UrlImage}}\">\n          </ion-avatar> -->\n          <h2>{{name}}</h2>\n          <p>{{email}}</p>\n          <ion-button class=\"edit-btn\" (click)='editAction()'> {{editButton}} </ion-button>\n        </div>\n        <div class=\"profile\" *ngIf=\"edited\">\n          <!-- <ion-avatar class=\"profile-media\">\n             <img *ngIf=\"!fileUrl\" src=\"{{apiUrl}}{{UrlImage}}\">\n             <img src=\"///Users/aayushkatiyar/Library/Developer/CoreSimulator/Devices/0899108A-9867-41D7-A0ED-7BDCB88B51BB/data/Containers/Data/Application/641A3AE1-18BA-445B-B87D-2EF74457786D/tmp/cdv_photo_003.jpg\">\n          </ion-avatar>  -->\n          <ion-input class=\"input-form-control\" value=\"name\" [(ngModel)]=\"name\"></ion-input>\n          <ion-input class=\"input-form-control\" placeholder=\"Want to change password enter here new password\" [(ngModel)]=\"password\"></ion-input>\n          <ion-input class=\"input-form-control\" value=\"displayName\" [(ngModel)]=\"displayName\"></ion-input>\n          <ion-button class=\"edit-btn\" (click)='editAction()'> {{editButton}} </ion-button>\n          <ion-button class=\"edit-btn\" (click)='cancelAction()'> {{cancelBtn}} </ion-button>\n        </div>\n      </ion-card-content>\n    </ion-card>\n    <ion-grid>\n      <!-- <ion-row>\n        <ion-col text-center>\n          <h3>Image Picker</h3>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center>\n          <ion-button (click)=\"getImages()\">Choose Images</ion-button>\n        </ion-col>\n      </ion-row> -->\n      <ion-row>\n        <ion-col>\n          <!-- More Pinterest floating gallery style -->\n          <div class=\"images\">\n            <div class=\"one-image\" *ngFor=\"let img of imageResponse\">\n              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <div class=\"profile-links-info\">\n    <ion-card class=\"profile-card\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"6\">\n            <div class=\"profile-links-card\" (click)=\"openAddress()\">\n                <img src=\"../../assets/imgs/address.svg\" height=\"50\">\n                <h2>My Address</h2>\n            </div>\n          </ion-col>\n          <ion-col size=\"6\">\n              <div class=\"profile-links-card\" (click)=\"openOrders()\">\n                  <img src=\"../../assets/imgs/myorders.svg\" height=\"50\">\n                  <h2>My Orders</h2>\n                </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.profile-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc); }\n\n.profile-avatar-info {\n  padding: 10px 0;\n  background: #fff;\n  border: none;\n  margin-bottom: 10px; }\n\n.profile-card {\n  --background: #fff;\n  text-align: center;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  margin-top: 0;\n  margin-bottom: 0;\n  box-shadow: none; }\n\n.profile-media {\n  margin: 0 auto 18px auto; }\n\n.profile h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-bottom: 10px;\n  padding: 12px 0; }\n\n.profile p {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px;\n  margin-bottom: 12px;\n  padding: 10px 0; }\n\n.edit-btn {\n  color: #fff;\n  text-transform: uppercase;\n  font-size: 13px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0 10px 0 0;\n  height: auto;\n  --padding-top: 8px;\n  --padding-bottom: 8px;\n  --padding-start: 15px;\n  --padding-end: 15px;\n  --background: #003a54; }\n\n.input-form-control {\n  border: 1px solid #d9e1e8;\n  margin-bottom: 10px;\n  color: #213051;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px;\n  text-align: left;\n  padding: 10px;\n  --padding-top: 10px;\n  --padding-end: 8px;\n  --padding-bottom: 10px;\n  --padding-start: 10px; }\n\n.profile-links-info {\n  padding: 10px 0;\n  background: #fff;\n  border: none; }\n\n.profile-links-card {\n  border: 1px solid #d0d8e0;\n  border-radius: 5px;\n  padding: 15px 10px;\n  position: relative; }\n\n.profile-links-card h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-bottom: 10px;\n  padding: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWE7RUFDYixZQUFZLEVBQUE7O0FBSWhCO0VBQ0UsbUJBQW1CO0VBQ2pCLGlEQUFhLEVBQUE7O0FBR2pCO0VBQXNCLGVBQWU7RUFBRSxnQkFBZ0I7RUFBRSxZQUFZO0VBQUUsbUJBQW1CLEVBQUE7O0FBRTFGO0VBQWtCLGtCQUFhO0VBQzNCLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixvQkFBb0I7RUFDcEIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFFcEI7RUFBZSx3QkFBdUIsRUFBQTs7QUFFdEM7RUFBWSxjQUFjO0VBQ3RCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQUMsZUFBZSxFQUFBOztBQUVyQztFQUNFLGNBQWE7RUFDYixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDdkIsZUFBZSxFQUFBOztBQUNmO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2Ysb0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFjO0VBQ2QscUJBQWlCO0VBQ2pCLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLHFCQUFhLEVBQUE7O0FBR1g7RUFDRSx5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLG1CQUFjO0VBQ2Qsa0JBQWM7RUFDZCxzQkFBaUI7RUFDakIscUJBQWdCLEVBQUE7O0FBR3hCO0VBQXFCLGVBQWU7RUFBRSxnQkFBZ0I7RUFBRSxZQUFZLEVBQUE7O0FBQ3BFO0VBQW9CLHlCQUF5QjtFQUN6QyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBOztBQUN0QjtFQUF1QixjQUFjO0VBQ2pDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQUMsVUFBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xufVxuXG4uaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuLmJhZGdlLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG5cbi5wcm9maWxlLWNvbnRlbnR7XG4gIGJhY2tncm91bmQ6ICNmNGY4ZmM7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvciwjZjRmOGZjKTtcbn1cblxuLnByb2ZpbGUtYXZhdGFyLWluZm97IHBhZGRpbmc6IDEwcHggMDsgYmFja2dyb3VuZDogI2ZmZjsgYm9yZGVyOiBub25lOyBtYXJnaW4tYm90dG9tOiAxMHB4O31cblxuLnByb2ZpbGUtY2FyZHsgICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMDtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgYm94LXNoYWRvdzogbm9uZTt9XG5cbi5wcm9maWxlLW1lZGlhe21hcmdpbjowIGF1dG8gMThweCBhdXRvO31cblxuLnByb2ZpbGUgaDJ7Y29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtwYWRkaW5nOiAxMnB4IDA7fVxuICBcbiAgLnByb2ZpbGUgcHsgICAgXG4gICAgY29sb3I6IzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsaW5lLWhlaWdodDogMjRweDsgICAgXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcbnBhZGRpbmc6IDEwcHggMDt9XG4uZWRpdC1idG57XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCAxMHB4IDAgMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLS1wYWRkaW5nLXRvcDogOHB4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDhweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDE1cHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMTVweDtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNhNTQ7XG4gICAgICB9XG5cbiAgICAgIC5pbnB1dC1mb3JtLWNvbnRyb2wge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZDllMWU4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBjb2xvcjogIzIxMzA1MTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogOHB4O1xuICAgICAgICAtLXBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gICAgfVxuXG4ucHJvZmlsZS1saW5rcy1pbmZveyBwYWRkaW5nOiAxMHB4IDA7IGJhY2tncm91bmQ6ICNmZmY7IGJvcmRlcjogbm9uZX1cbi5wcm9maWxlLWxpbmtzLWNhcmR7Ym9yZGVyOiAxcHggc29saWQgI2QwZDhlMDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogMTVweCAxMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTt9XG4ucHJvZmlsZS1saW5rcy1jYXJkIGgye2NvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7cGFkZGluZzowO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");











var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, crop, transfer, homePageService, imagePicker, loadingService, alert, authService, router, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.crop = crop;
        this.transfer = transfer;
        this.homePageService = homePageService;
        this.imagePicker = imagePicker;
        this.loadingService = loadingService;
        this.alert = alert;
        this.authService = authService;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.apiUrl = 'https://www.niletechinnovations.com/eq8tor';
        this.selectedPath = '';
        this.name = '';
        this.email = '';
        this.UrlImage = '';
        this.password = '';
        this.isReadOnly = true;
        this.editButton = "Edit";
        this.cancelBtn = "Cancel";
        this.profilePageModel = [];
        this.fileUrl = null;
        this.totalCount = '';
        this.cart = [];
        this.router.events.subscribe(function (event) {
            _this.selectedPath = event.url;
        });
        this.dynamicColor = 'redish';
        this.getProfileDetail();
    }
    ProfilePage.prototype.ngOnInit = function () {
        console.log('ng init call');
        this.getCartContent();
    };
    ProfilePage.prototype.getProfileDetail = function () {
        var _this = this;
        this.loadingService.present();
        this.authService.profile().subscribe(function (response) {
            _this.profilePageModel = response['data'];
            _this.name = _this.profilePageModel['name'];
            _this.email = _this.profilePageModel['email'];
            _this.UrlImage = _this.profilePageModel['user_photo_url'];
            _this.displayName = _this.profilePageModel['display_name'];
            console.log(_this.profilePageModel);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    ProfilePage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 1,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are 
            // window.imagePicker.OutputType.FILE_URI (0) or 
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    // cropUpload() {
    //   this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then((results) => {
    //     for (let i = 0; i < results.length; i++) {
    //         console.log('Image URI: ' + results[i]);
    //         this.crop.crop(results[i], { quality: 100 })
    //           .then(
    //             newImage => {
    //               console.log('new image path is: ' + newImage);
    //               this.fileUrl = newImage;
    //               const fileTransfer: FileTransferObject = this.transfer.create();
    //               const uploadOpts: FileUploadOptions = {
    //                  fileKey: 'file',
    //                  fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
    //               };
    //               fileTransfer.upload(newImage, 'http://192.168.0.7:3000/api/upload', uploadOpts)
    //                .then((data) => {
    //                  console.log(data);
    //                  this.respData = JSON.parse(data.response);
    //                  console.log(this.respData);
    //                  this.fileUrl = this.respData.fileUrl;
    //                  console.log("this.file url", this.fileUrl);
    //                }, (err) => {
    //                  console.log(err);
    //                });
    //             },
    //             error => console.error('Error cropping image', error)
    //           );
    //     }
    //   }, (err) => { console.log("errorrorororo",err); });
    // }
    ProfilePage.prototype.editAction = function () {
        if (this.editButton === "Edit") {
            this.editButton = "Save";
            this.edited = true;
        }
        else {
            this.editButton = "Edit";
            this.edited = false;
            console.log("this.password", this.password);
            this.userProfileUpdate(this.displayName, this.name, this.email, this.password);
        }
    };
    ProfilePage.prototype.userProfileUpdate = function (displayName, username, emailId, password) {
        var _this = this;
        this.loadingService.present();
        console.log(password);
        this.homePageService.updateProfile(displayName, username, emailId, password).subscribe(function (resp) {
            console.log(resp);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            if (err.status == 500) {
                console.log("500 status");
                _this.alert.myAlertMethod("Error", err.message, function (data) {
                    console.log("error");
                });
            }
            _this.loadingService.dismiss();
        });
    };
    ProfilePage.prototype.openAddress = function () {
        this.router.navigate(['address']);
    };
    ProfilePage.prototype.cancelAction = function () {
        this.edited = false;
        this.editButton = "Edit";
    };
    ProfilePage.prototype.openOrders = function () {
        this.router.navigate(['your-orders']);
    };
    ProfilePage.prototype.getCartContent = function () {
        var _this = this;
        console.log("getCartContent");
        this.homePageService.getCartProducts().subscribe(function (resp) {
            console.log("getCartContent", JSON.stringify(resp));
            _this.cart = resp.data.products;
            _this.totalCount = resp.data.total_product;
        }, function (err) {
            console.log("getCartContent error", err);
        });
    };
    ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.page.html */ "./src/app/profile/profile.page.html"),
            styles: [__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_8__["Crop"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__["FileTransfer"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_10__["HomepageService"], _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["ImagePicker"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__["AlerServiceService"], _Service_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ProfilePage);
    return ProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-profile-module.js.map